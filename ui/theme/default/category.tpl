{extends file="$layouts_admin"}

{block name="content"}
    <div class="row">
        <div class="col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add Category</h5>

                </div>
                <div class="ibox-content">

                    <form role="form" name="accadd" method="post" action="{$_url}ps/add-category/">
                        <div class="form-group">
                            <label for="name">{$_L['Name']}</label>
                            <input type="text" class="form-control" id="name" {if $d}
                                value="{$d->name}"
                                {else}
                                value=""
                            {/if}
                              name="name"  required>
                        </div>
                        <div class="form-group">
                          <label  for="type">Type</label>



                              <select class="form-control" id="type" name="type">
                                  <option {if $d} {if $d->type eq '1'} selected {/if} {/if} value="1">Product</option>
                                  <option {if $d} {if $d->type eq '2'} selected {/if} {/if} value="2">Service</option>

                              </select>


                      </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" id="description" name="description" {if $d}
                                value="{$d->description}"
                                {else}
                                value=""
                            {/if} >
                        </div>

                        <div class="form-group">
                            <label for="featured">Is Feature Image</label>
                            <input type="checkbox" class="form-control" id="featured" name="featured" >
                        </div>
                        <input type="hidden" name="file_link" id="file_link" {if $d}
                            value="{$d->image}"
                            {else}
                            value="/default.jpg"
                        {/if} >

                        <input type="hidden1" name="id"
                         {if $d}
                            value="{$d->id}"
                            {else}
                            value=""
                        {/if} >
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> {$_L['Submit']}</button> | {$_L['Or']} <a href="{$_url}ps/category-list/"> {$_L['Back To The List']}</a>
                    </form>

                </div>
            </div>



        </div>

        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    {$_L['Image']}
                </div>
                <div class="ibox-content" id="ibox_form">

                    <form action="" class="dropzone" id="upload_container">

                        <div class="dz-message">
                            <h3> <i class="fa fa-cloud-upload"></i>  {$_L['Drop File Here']}</h3>
                            <br />
                            <span class="note">{$_L['Click to Upload']}</span>
                        </div>

                    </form>

                </div>
            </div>
        </div>

    </div>
{/block}


{block name="script"}
    <script>
        Dropzone.autoDiscover = false;
        $(document).ready(function () {

            $('.amount').autoNumeric('init', {

                aSign: '{$config['currency_code']} ',
                dGroup: {$config['thousand_separator_placement']},
                aPad: {$config['currency_decimal_digits']},
                pSign: '{$config['currency_symbol_position']}',
                aDec: '{$config['dec_point']}',
                aSep: '{$config['thousands_sep']}',
                vMax: '9999999999999999.00',
                vMin: '-9999999999999999.00'

            });

            $(".progress").hide();
            $("#emsg").hide();

            var _url = $("#_url").val();

          //  var ib_submit = $("#submit");

            var $file_link = $("#file_link");

            var upload_resp;

          /*  $('#description').redactor(
                {
                    minHeight: 200 // pixels
                }
            );*/

            var ib_file = new Dropzone("#upload_container",
                {
                    url: _url + "ps/upload_category/",
                    maxFiles: 1
                }
            );


            ib_file.on("sending", function() {

                //ib_submit.prop('disabled', true);

            });

            ib_file.on("success", function(file,response) {

              //  ib_submit.prop('disabled', false);

                upload_resp = response;

                if(upload_resp.success == 'Yes'){

                    toastr.success(upload_resp.msg);
                    $file_link.val(upload_resp.file);
                    //alert($file_link.val())


                }
                else{
                    toastr.error(upload_resp.msg);
                }


            });


          /*  ib_submit.click(function (e) {
                e.preventDefault();
                $('#ibox_form').block({ message: null });
                var _url = $("#_url").val();
                $.post(_url + 'ps/add-post/', $( "#rform" ).serialize())
                    .done(function (data) {

                        if ($.isNumeric(data)) {

                            location.reload();
                        }
                        else {
                            $('#ibox_form').unblock();

                            $("#emsgbody").html(data);
                            $("#emsg").show("slow");
                        }
                    });
            });*/
        });
    </script>
{/block}
