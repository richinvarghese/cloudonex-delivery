{extends file="$layouts_admin"}

{block name="style"}
    <link rel="stylesheet" type="text/css" href="{$app_url}ui/lib/footable/css/footable.core.min.css" />
    <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
{/block}

{block name="content"}
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Manage Driver</h5>

                </div>
                <div class="ibox-content">
                    <a href="{$_url}driver/driver-users-add" class="btn btn-primary"><i class="fa fa-plus"></i>Add Driver</a>
                    <a href="{$_url}driver/driver-availablity" class="btn btn-inverse "><i class="fa fa-tv"></i>Manage Driver Availablity</a>
                    <a href="{$app_url}storage/driver/driver.apk" class="btn btn-inverse "><i class="fa fa-tv" download></i>Download Driver App</a>

                    <br>
                    <br>

                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="fa fa-search"></span>
                                    </div>
                                    <input type="text" name="name" id="foo_filter" class="form-control" placeholder="{$_L['Search']}..."/>

                                </div>
                            </div>

                        </div>
                    </form>

                    <table class="table table-bordered table-hover sys_table footable" data-filter="#foo_filter" data-page-size="50">
                        <thead>
                        <tr>
                            <th style="width: 60px;">{$_L['Avatar']}</th>
                            <th>{$_L['Username']}</th>
                            <th>{$_L['Full_Name']}</th>
                            <th>{$_L['Type']}</th>
                            <th>{$_L['Manage']}</th>
                        </tr>
                        </thead>

                        {foreach $d as $ds}
                            <tr>
                                <td>{if $ds['img'] eq 'gravatar'}
                                        <img src="http://www.gravatar.com/avatar/{($ds['username'])|md5}?s=60" class="img-circle" alt="{$user['fullname']}">
                                    {elseif $ds['img'] eq ''}
                                        <img src="{$app_url}ui/lib/imgs/default-user-avatar.png" style="max-height: 60px;" alt="">
                                    {else}
                                        <img src="{$app_url}/{$ds['img']}" class="img-circle" style="max-height: 60px;" alt="{$ds['fullname']}">
                                    {/if}</td>
                                <td>{$ds['username']}</td>
                                <td>{$ds['fullname']}</td>
                                <td>{ib_lan_get_line($ds['user_type'])}</td>
                                <td>

                                    <a href="{$_url}driver/driver-users-edit/{$ds['id']}" class="btn btn-inverse"><i class="fa fa-pencil"></i> </a>
                                    {if ($_user['username']) neq ($ds['username'])}
                                        <a href="{$_url}driver/driver-users-delete/{$ds['id']}" id="{$ds['id']}" class="btn btn-danger cdelete"><i class="fa fa-trash"></i> </a>
                                    {/if}
                                    <button class="btn btn-primary mapdata" data-toggle="modal" data-latitude="{$ds['latitude']}" data-longitude="{$ds['longitude']}" data-target="#myModal"><i class="fa fa-map"></i> View On Map</button>
                                </td>
                            </tr>
                        {/foreach}

                        <tfoot>
                        <tr>
                            <td colspan="5">
                                <ul class="pagination">
                                </ul>
                            </td>
                        </tr>
                        </tfoot>

                    </table>

                </div>
            </div>



        </div>



    </div>
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-header">
    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    	<h3>Current Location</h3>
    </div>
    <div class="modal-body">
          <div id="map"></div>
    </div>
    <div class="modal-footer">

    	<button class="btn btn-primary update" data-dismiss="modal">Close</button>
    </div>
  </div>
    <style type="text/css">

    </style>


{/block}

{block name="script"}

    <script type="text/javascript" src="{$app_url}ui/lib/footable/js/footable.all.min.js"></script>

    <script>
        $(function () {
            $('.footable').footable();
        });

 {literal}

 var latitude = 1.3521;
 var longitude = 103.8198;
 $(".mapdata").click(function(){
  latitude =  parseFloat($(this).data("latitude"));
  longitude = parseFloat($(this).data("longitude"));

  initMap();
 });

        function initMap() {
          //var latitude = parseFloat($("#latitude").val());
          //var longitude = parseFloat($("#longitude").val());
          //alert(latitude);
          var uluru = {lat: latitude, lng: longitude};
          // The map, centered at Uluru
          var map = new google.maps.Map(
              document.getElementById('map'), {zoom: 15, center: uluru});
          // The marker, positioned at Uluru
          var marker = new google.maps.Marker({position: uluru, map: map});

        }

    {/literal}
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK9Wyy81NpY3wkqekhOF4EBFHt5Tc-Yyw">
    </script>

{/block}
