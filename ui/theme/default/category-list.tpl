{extends file="$layouts_admin"}

{block name="content"}
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Category List</h5>
        </div>
        <div class="ibox-content">
            <a href="{$_url}ps/category/" id="item_add" class="btn btn-primary"><i class="fa fa-plus"></i> Add Category</a>
            <hr>
            <table class="table table-bordered table-hover sys_table">
                <thead>
                <tr>
                    <th>{$_L['Name']}</th>
                    <th>Description</th>
                    <th>Type</th>

                    <th>{$_L['Manage']}</th>
                </tr>
                </thead>
                <tbody>
                {foreach $d as $ds}
                    <tr id="{$ds['id']}">
                        <td> {if $ds['is_default'] eq '1'} <label class="label label-success label-sm">Default</label> {/if} {$ds['name']} </td>
                        <td>

                            {$ds['description']}


                        </td>
                        <td>

                            {if $ds['type'] eq '1'}
                              Product
                            {else}
                              Service
                            {/if}


                        </td>
                        <td>
                            <a href= "{$_url}ps/edit-category/{$ds['id']}" class="btn btn-info btn-xs edit"><i class="fa fa-pencil"></i> {$_L['Edit']} </a>


                          {*
                            <button type="button" id="t{$ds['id']}" class="btn btn-danger btn-xs cdelete"><i class="fa fa-trash"></i> {$_L['Delete']} </button> *}
                        </td>

                    </tr>
                {/foreach}

                </tbody>
            </table>

        </div>
    </div>
    <input type="hidden" id="_lan_are_you_sure" value="{$_L['are_you_sure']}">
{/block}
