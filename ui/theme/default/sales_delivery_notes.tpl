{extends file="$layouts_admin"}
{block name="style"}
    <link rel="stylesheet" type="text/css" href="{$app_url}ui/lib/footable/css/footable.core.min.css" />
    <link href="{$app_url}ui/lib/mselect/multiple-select.css" rel="stylesheet">

{/block}
{block name="content"}

    <div class="row">
        <div class="col-md-12">
            <h3 class="ibilling-page-header">{$_L['Delivery Challans']}</h3>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">

                  <a href="{$_url}sales/delivery_challan" class="btn btn-primary"><i class="fa fa-plus"></i> {$_L['New']}</a>
                    <a href="{$_url}driver/driver-assign"  class="btn btn-inverse  btn-sm"><i class="fa fa-car"></i> Smart Assign Driver </a>
                    <a href="{$_url}driver/driver-manual-assign"  class="btn btn-inverse  btn-sm"><i class="fa fa-car"></i> Manual Assign  </a>

                  <div class="hr-line-dashed"></div>

                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="fa fa-search"></span>
                                    </div>
                                    <input type="text" name="name" id="foo_filter" class="form-control" placeholder="{$_L['Search']}..."/>

                                </div>
                            </div>

                        </div>
                    </form>

                    <table class="table table-bordered table-hover sys_table footable" data-filter="#foo_filter" data-page-size="50">
                        <thead>
                        <tr>
                            <th>Delivery Chellan</th>
                            <th>{$_L['Customer']}</th>
                            <th>Delivery Date</th>
                            <th>Assigned Driver</th>
                            <th>Status</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody>

                        {foreach $d as $ds}
                            <tr>
                              <td><a href="{$_url}invoices/deliveryview/{$ds['id']}">DO-000{$ds['id']}</a></td>
                              <td><a href="{$_url}contacts/view/{$ds['userid']}">{$ds['account']}</a> </td>
                                <td>{$ds['c2']|date_format:"%D %I:%M %p"}

 </td>
                              <!--  <td class="amount">{$ds['total']}</td>-->
                              <td>

                                {*<select  onchange="assign_driver(this.value, {$ds['id']})" class="form-control driver" name ="driver{$ds['id']}"  id="driver{$ds['id']}"  >
                                  <option value="">Select Driver...</option>
                                  {foreach $c as $cs}
                                      <option {if $ds['c5'] eq ($cs['id'])}selected="selected" {/if} value="{$cs['id']}"
                        >{$cs['fullname']}</option>
                                  {/foreach}

                              </select>*}
                              {$ds['fullname']}

                </td>
                <td>
                  {if $ds['delivery_status'] eq 'Unassigned'|lower}
                  <span class="label label-danger">{$ds['delivery_status']}</span>
                  {elseif $ds['delivery_status'] eq 'Assigned'|lower}
                      <span class="label label-info">{$ds['delivery_status']}</span>
                  {elseif $ds['delivery_status'] eq 'Acknowledged'|lower}
                      <span class="label label-primary">{$ds['delivery_status']}</span>
                  {elseif $ds['delivery_status'] eq 'Started'|lower}
                      <span class="label label-warning">{$ds['delivery_status']}</span>
                 {elseif $ds['delivery_status'] eq 'Inprogress'|lower}
                      <span class="label label-default">{$ds['delivery_status']}</span>
                 {elseif $ds['delivery_status'] eq 'Successful'|lower}
                      <span class="label label-success">{$ds['delivery_status']}</span>
                  {else}
                      <span class="label label-danger">{$ds['delivery_status']}</span>
                  {/if}
                </td>

                <td>
                  <a href="{$_url}invoices/view/{$ds['id']}/" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="{$_L['View']}"><i class="fa fa-file-text-o"></i></a>


                  <a href="{$_url}sales/edit/{$ds['id']}/" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="{$_L['Edit']}"><i class="fa fa-pencil"></i></a>



                  <a href="#" class="btn btn-danger btn-xs cdelete" id="iid{$ds['id']}" data-toggle="tooltip" data-placement="top" title="{$_L['Delete']}"><i class="fa fa-trash"></i></a>

                </td>
                            </tr>
                        {/foreach}

                        </tbody>

                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <ul class="pagination">
                                </ul>
                            </td>
                        </tr>
                        </tfoot>

                    </table>

                </div>
            </div>
        </div>
    </div>



{/block}

{block name=script}

    <script type="text/javascript" src="{$app_url}ui/lib/footable/js/footable.all.min.js"></script>
    <script type="text/javascript" src="{$app_url}ui/lib/numeric.js"></script>
    <script src="{$app_url}ui/lib/mselect/multiple-select.js"></script>

    <script>


        $(function() {

            $('.footable').footable();

            $('.amount').autoNumeric('init', {

                aSign: '{$config['currency_code']} ',
                dGroup: {$config['thousand_separator_placement']},
                aPad: {$config['currency_decimal_digits']},
                pSign: '{$config['currency_symbol_position']}',
                aDec: '{$config['dec_point']}',
                aSep: '{$config['thousands_sep']}',
                vMax: '9999999999999999.00',
                vMin: '-9999999999999999.00'

            });



        });


        function assign_driver(cid, invoiceid){
          var _url = $("#_url").val();


          // alert(invoiceid);

              $.post(_url + 'invoices/assign_driver/', {
                  cid: cid,
                  invoice: invoiceid


              })
                  .done(function (data) {
                      alert(data);

                  });
        }
        $('#driver1').select2({
            theme: "bootstrap",
            language: {
                noResults: function () {
                    return $("#_lan_no_results_found").val();
                }
            }
        })
        .on("change", function (e) {
            // mostly used event, fired to the original element when the value changes
            // log("change val=" + e.val);
            //  alert(e.val);

          //  update_address();
        });


    </script>



{/block}
