{extends file="$layouts_admin"}
{block name="style"}
    <link rel="stylesheet" type="text/css" href="{$app_url}ui/lib/footable/css/footable.core.min.css" />
    <link href="{$app_url}ui/lib/mselect/multiple-select.css" rel="stylesheet">
    <link href="{$app_url}ui/lib/css/tab.css" rel="stylesheet">
    <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>

{/block}
{block name="content"}

    <div class="row">
        <div class="col-md-12">
            <h3 class="ibilling-page-header">Tasks</h3>
        </div>
    </div>

    <form action="{$_url}sales/tasks/" method="post" accept-charset="utf-8">

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">


                  <div class="form-group">
                      <label for="date" class="col-sm-2 control-label">Choose Task Date</label>
                      <div class="col-sm-3">
                          <input type="text" class="form-control"  value="{$mdate}" name="selected_date" id="selected_date" datepicker data-date-format="yyyy-mm-dd" data-auto-close="true">
                      </div>
                      <div class="col-sm-3">
                        <button class="btn btn-primary btn-sm" type="submit">Change Date</button>
                      </div>
                  </div>

                </form>
                  <div class="hr-line-dashed"></div>

<div class="row">

  <div class="col-sm-5" style="overflow-y: scroll;height: 340px;">
                  <div class="tab">
  <button class="tablinks active" onclick="openCity(event, 'unassigned')">Unassigned</button>
  <button class="tablinks" onclick="openCity(event, 'assigned')">Assigned</button>
  <button class="tablinks" onclick="openCity(event, 'completed')">Completed</button>
</div>

<div id="unassigned" class="tabcontent active">
  <div class="ibox-content">
      <table class="table table-hover">
{foreach $un as $unassigned}

<tr>
   <td><label>Task ID </label></td>
    <td><label>{$unassigned['id']}</a></label></td>


</tr>
<tr>
    <td>Customer Name </td>
    <td>{$unassigned['account']}</td>

</tr>

<tr>
    <td>Address </td>
    <td>{$unassigned['shipping_street']}</td>

</tr>
<tr>
  <td colspan="2"> <button class="btn btn-warning btn-sm" type="button" onclick="calcRoute({$unassigned['c3']},{$unassigned['c4']},{$config['latitude']}, {$config['longitude']})">Show Route In Map</button>
</td>
</tr>

<tr>
  <td colspan="2"></td>
</tr>

{/foreach}
</table>


</div>
</div>

<div id="assigned" class="tabcontent">
  <div class="ibox-content">
      <table class="table table-hover">
{foreach $as as $unassigned}
<tr>
  <td><label>Task ID </label></td>
  <td><label>{$unassigned['id']}</label></td>

</tr>
<tr>
    <td>Customer Name </td>
    <td>{$unassigned['account']}</td>

</tr>

<tr>
    <td>Address </td>
    <td>{$unassigned['shipping_street']}</td>

</tr>
<tr>
    <td>Driver Name </td>
    <td>{$unassigned['c5']}</td>

</tr>
<tr>
  <td colspan="2"> <button class="btn btn-warning btn-sm" type="button" onclick="calcRoute({$unassigned['c3']},{$unassigned['c4']}, {$config['latitude']}, {$config['longitude']})">Show Route In Map</button>
</td>
<tr>
<td colspan="2"></td>
</tr>
{/foreach}
</table>


</div>
</div>

<div id="completed" class="tabcontent">
  <div class="ibox-content">
      <table class="table table-hover">
{foreach $co as $unassigned}
<tr>
  <td><label>Task ID </label></td>
  <td><label>{$unassigned['id']}</label></td>

</tr>
<tr>
    <td>Customer Name </td>
    <td>{$unassigned['account']}</td>

</tr>

<tr>
    <td>Address </td>
    <td>{$unassigned['shipping_street']}</td>

</tr>
<tr>
    <td>Driver Name </td>
    <td>{$unassigned['c5']}</td>

</tr>
<tr>
  <td colspan="2"> <button class="btn btn-warning btn-sm" type="button" onclick="calcRoute({$unassigned['c3']},{$unassigned['c4']}, {$config['latitude']}, {$config['longitude']})">Show Route In Map</button>
</td>
<tr>
<td colspan="2"></td>
</tr>

{/foreach}
</table>


</div>
</div>
</div>
<div class="col-sm-7">
    <div id="map"></div>
</div>
</div>





                </div>
            </div>
        </div>
    </div>



{/block}

{block name=script}

    <script type="text/javascript" src="{$app_url}ui/lib/footable/js/footable.all.min.js"></script>
    <script type="text/javascript" src="{$app_url}ui/lib/numeric.js"></script>
    <script src="{$app_url}ui/lib/mselect/multiple-select.js"></script>

    <script>


        $(function() {

          $('[data-toggle="datepicker"]').datepicker();

            $('.footable').footable();

            $('.amount').autoNumeric('init', {

                aSign: '{$config['currency_code']} ',
                dGroup: {$config['thousand_separator_placement']},
                aPad: {$config['currency_decimal_digits']},
                pSign: '{$config['currency_symbol_position']}',
                aDec: '{$config['dec_point']}',
                aSep: '{$config['thousands_sep']}',
                vMax: '9999999999999999.00',
                vMin: '-9999999999999999.00'

            });



        });


        function assign_driver(cid, invoiceid){
          var _url = $("#_url").val();


          // alert(invoiceid);

              $.post(_url + 'invoices/assign_driver/', {
                  cid: cid,
                  invoice: invoiceid


              })
                  .done(function (data) {
                      alert(data);

                  });
        }
        $('#driver1').select2({
            theme: "bootstrap",
            language: {
                noResults: function () {
                    return $("#_lan_no_results_found").val();
                }
            }
        })
        .on("change", function (e) {
            // mostly used event, fired to the original element when the value changes
            // log("change val=" + e.val);
            //  alert(e.val);

          //  update_address();
        });

        document.getElementById("unassigned").style.display = "block";
  //evt.currentTarget.className += " active"

        function openCity(evt, cityName) {
          evt.preventDefault()
          var i, tabcontent, tablinks;
          tabcontent = document.getElementsByClassName("tabcontent");
          for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
          }
          tablinks = document.getElementsByClassName("tablinks");
          for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
          }
          document.getElementById(cityName).style.display = "block";
          evt.currentTarget.className += " active";
        }

var temp = {$all|json_encode}
 {literal}
 //initMap();
                function initMap() {
                  var latitude = 1.3521;
                  var longitude = 103.8198;
                  var uluru = {lat: latitude, lng: longitude};

                  var loc = [];
                  var locations = [];
                  //var i = 1, j = 0;
                  for(i=0 , j=0 ;i < temp.length; i++, j++){

                    locations[j] = ["aaa", temp[i].c3, temp[i].c4, i];

                  }
                  /*; */


                /*  var locations = [
      ['Bondi Beach', -33.890542, 151.274856, 4],
      ['Coogee Beach', -33.923036, 151.259052, 5],
      ['Cronulla Beach', -34.028249, 151.157507, 3],
      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
      ['Maroubra Beach', -33.950198, 151.259302, 1]
    ];*/
                  //var latitude = parseFloat($("#latitude").val());
                  //var longitude = parseFloat($("#longitude").val());
              //  alert(latitude);
                  var uluru = {lat: latitude, lng: longitude};
                  // The map, centered at Uluru
                  var map = new google.maps.Map(
                      document.getElementById('map'), {zoom: 9, center: new google.maps.LatLng(1.3521, 103.8198)});
                  // The marker, positioned at Uluru
                  var marker = new google.maps.Marker({position: uluru, map: map});



                  for (i = 0; i < locations.length; i++) {
     marker = new google.maps.Marker({
       position: new google.maps.LatLng(locations[i][1], locations[i][2]),
       map: map
     });

     google.maps.event.addListener(marker, 'click', (function(marker, i) {
       return function() {
         infowindow.setContent(locations[i][0]);
         infowindow.open(map, marker);
       }
     })(marker, i));
   }

                }

                var map;
	var waypoints;

                function calcRoute(endlat, endlon, startlat, startlon) {
                  //alert(endlon);
                      var mapLayer = document.getElementById("map");
                      var centerCoordinates = new google.maps.LatLng(startlat, startlon);
                    var defaultOptions = { center: centerCoordinates, zoom: 5 }
                    map = new google.maps.Map(mapLayer, defaultOptions);

                    var directionsService = new google.maps.DirectionsService;
                    var directionsDisplay = new google.maps.DirectionsRenderer;
                    directionsDisplay.setMap(map);

                    start = -33.923036;
                    end = 151.274856;
                  drawPath(directionsService, directionsDisplay,endlat,endlon , startlat, startlon);
                  //return false;
                }
                  function drawPath(directionsService, directionsDisplay,endlat,endlon, startlat, startlon) {
                    //alert(endlon);
                    directionsService.route({
                    //  origin: new google.maps.LatLng(startlat, startlon), //mobile city
                    origin: new google.maps.LatLng(startlat, startlon), //mobile city
                    destination: new google.maps.LatLng(endlat, endlon),
                    //  destination: new google.maps.LatLng(endlat, endlon),
                    //  waypoints: waypoints,
                      optimizeWaypoints: true,
                      travelMode: 'DRIVING'
                    }, function(response, status) {
                        if (status === 'OK') {

                        directionsDisplay.setDirections(response);
                        } else {
                      //      alert("d");
                        window.alert('Problem in showing direction due to ' + status);
                        }
                    });
              }





 {/literal}

    </script>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK9Wyy81NpY3wkqekhOF4EBFHt5Tc-Yyw&callback=initMap">
    </script>



{/block}
