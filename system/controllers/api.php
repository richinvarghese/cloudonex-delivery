<?php

$version = route(1);

if (isset($_SERVER['HTTP_ORIGIN'])) {
    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
    // you want to allow, and if so:
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

switch ($version){

    case 'v2':

        if(isset($_GET['_METHOD']))
        {
            $method = $_GET['_METHOD'];
        }
        else{
            $method = $_SERVER['REQUEST_METHOD'];
        }

        header('Access-Control-Allow-Origin: *');

        apiAuth();

        $object = route(2);

        switch ($object){

            case 'customers':

                switch ($method){

                    case 'GET':

                        jsonResponse(Contact::orderBy('id','desc')->get()->toArray());


                        break;


                    case 'POST':




                        break;


                    case 'PUT':


                        break;


                    case 'DELETE':


                        break;


                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }


                break;

            case 'customer':


                switch ($method){

                    case 'GET':


                        $id = route(3);

                        $contact = Contact::find($id)->toArray();

                        if($contact)
                        {
                            jsonResponse($contact);
                        }
                        else{
                            jsonResponse([
                                'error' => true,
                                'message' => 'Contact not found!'
                            ]);
                        }


                        break;


                    case 'POST':

                        $errors = [];

                        $account = _post('account');

                        $type_customer = _post('customer');
                        $type_supplier = _post('supplier');

                        $type = $type_customer.','.$type_supplier;
                        $type = trim($type,',');

                        if($type == ''){
                            $type = 'Customer';
                        }


                        //  $company = _post('company');

                        $company_id = _post('company_id');

                        $company = '';
                        $cid = 0;

                        $email = _post('email');
                        $username = _post('username');
                        $phone = _post('phone');
                        $currency = _post('currency');

                        $address = _post('address');
                        $city = _post('city');
                        $state = _post('state');
                        $zip = _post('zip');
                        $country = _post('country');


                        $owner_id = _post('owner_id');

                        if($owner_id == '')
                        {
                            $owner_id = 0;
                        }

                        if($company_id != ''){

                            if($company_id != '0'){
                                $company_db = db_find_one('sys_companies',$company_id);

                                if($company_db){
                                    $company = $company_db->company_name;
                                    $cid = $company_id;
                                }
                            }


                        }


                        elseif (_post('company') != ''){


                            // create compnay
                            $company = _post('company');
                            $c = new Company;

                            $c->company_name = $company;
                            $c->email = $email;
                            $c->phone = $phone;


                            $c->address1 = $address;
                            $c->city = $city;
                            $c->state = $state;
                            $c->zip = $zip;
                            $c->country = $country;

                            $c->save();

                            $cid = $c->id;


                        }



                        if($currency == ''){
                            $currency = '0';
                        }

                        if(isset($_POST['tags']) AND ($_POST['tags']) != ''){
                            $tags = $_POST['tags'];
                        }
                        else{
                            $tags = '';
                        }



                        if($account == ''){
                            $errors[] = $_L['Account Name is required'];
                        }


                        if($email != ''){

                            $f = ORM::for_table('crm_accounts')->where('email',$email)->find_one();

                            if($f){
                                $errors[] =  $_L['Email already exist'];
                            }
                        }


                        if($phone != ''){

                            $f = ORM::for_table('crm_accounts')->where('phone',$phone)->find_one();

                            if($f){
                                $errors[] =  $_L['Phone number already exist'];
                            }
                        }


                        $gid = _post('group');

                        if($gid != ''){
                            $g = db_find_one('crm_groups',$gid);
                            $gname = $g['gname'];
                        }
                        else{
                            $gid = 0;
                            $gname = '';
                        }

                        $password = _post('password');

                        $u_password = '';


                        if($password != ''){


                            $u_password = $password;
                            $password = Password::_crypt($password);


                        }






                        if(empty($errors)){

                            Tags::save($tags,'Contacts');

                            $data = array();

                            $data['created_at'] = date('Y-m-d H:i:s');
                            $data['updated_at'] = date('Y-m-d H:i:s');

                            //  $type = _post('type');


                            $d = ORM::for_table('crm_accounts')->create();

                            $d->account = $account;
                            $d->email = $email;
                            $d->phone = $phone;
                            $d->address = $address;
                            $d->city = $city;
                            $d->zip = $zip;
                            $d->state = $state;
                            $d->country = $country;
                            $d->tags = Arr::arr_to_str($tags);

                            //others
                            $d->fname = '';
                            $d->lname = '';
                            $d->company = $company;
                            $d->jobtitle = '';
                            $d->cid = $cid;
                            $d->o = $owner_id;
                            $d->balance = '0.00';
                            $d->status = 'Active';
                            $d->notes = '';
                            $d->password = $password;
                            $d->token = '';
                            $d->ts = '';
                            $d->img = '';
                            $d->web = '';
                            $d->facebook = '';
                            $d->google = '';
                            $d->linkedin = '';

                            // v 4.2

                            $d->gname = $gname;
                            $d->gid = $gid;

                            // build 4550

                            $d->currency = $currency;

                            //

                            $d->created_at = $data['created_at'];

                            $d->type = $type;

                            //

                            $d->business_number = _post('business_number');

                            $d->fax = _post('fax');


                            //

                            //

                            $drive = time().Ib_Str::random_string(12);

                            $d->drive = $drive;

                            //
                            $d->save();
                            $cid = $d->id();
                            _log($_L['New Contact Added'].' '.$account.' [CID: '.$cid.']','Admin',$owner_id);

                            //now add custom fields
                            $fs = ORM::for_table('crm_customfields')->where('ctype','crm')->order_by_asc('id')->find_array();
                            foreach($fs as $f){
                                $fvalue = _post('cf'.$f['id']);
                                $fc = ORM::for_table('crm_customfieldsvalues')->create();
                                $fc->fieldid = $f['id'];
                                $fc->relid = $cid;
                                $fc->fvalue = $fvalue;
                                $fc->save();
                            }
                            //

                            Event::trigger('contacts/add-post/_on_finished');

                            // send welcome email if needed

                            $send_client_signup_email = _post('send_client_signup_email');


                            if(($email != '') && ($send_client_signup_email == 'Yes') && ($u_password != '')){

                                $email_data = array();
                                $email_data['account'] = $account;
                                $email_data['company'] = $company;
                                $email_data['password'] = $u_password;
                                $email_data['email'] = $email;

                                $send_email = Ib_Email::send_client_welcome_email($email_data);



                            }



                            // Create Drive if this feature is enabled


                            if($config['client_drive'] == '1'){

                                if (!file_exists('storage/drive/customers/'.$drive.'/storage')) {
                                    mkdir('storage/drive/customers/'.$drive.'/storage',0777,true);
                                }

                            }




                            //

                            jsonResponse([
                                'error' => false,
                                'contact_id' => $cid,
                                'message' => $_L['account_created_successfully']
                            ]);



                        }
                        else{
                            jsonResponse([
                                'error' => true,
                                'message' => $errors
                            ]);
                        }


                        break;


                    case 'PUT':


                        break;


                    case 'DELETE':


                        break;


                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }


                break;


            case 'transactions':

                switch ($method){

                    case 'GET':

                        jsonResponse(Contact::orderBy('id','desc')->get()->toArray());


                        break;


                    case 'POST':



                        break;


                    case 'PUT':


                        break;


                    case 'DELETE':


                        break;


                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }


                break;


            case 'accounts':

                switch ($method){

                    case 'GET':

                        jsonResponse(Contact::orderBy('id','desc')->get()->toArray());


                        break;


                    case 'POST':



                        break;


                    case 'PUT':


                        break;


                    case 'DELETE':


                        break;


                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }


                break;



            case 'users':


                switch ($method){

                    case 'GET':

                        jsonResponse(User::orderBy('id','desc')->get()->toArray());


                        break;




                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }

                break;



            case 'user':

                switch ($method){

                    case 'GET':

                        $id = route(3);

                        $user = User::find($id)->toArray();

                        if($user)
                        {
                            jsonResponse($user);
                        }
                        else{
                            jsonResponse([
                                'error' => true,
                                'message' => 'User not found!'
                            ]);
                        }


                        break;


                    case 'POST':

                        $username = _post('username');
                        $fullname = _post('fullname');
                        $password = _post('password');
                        $user_type = _post('user_type');


                        $r = M::factory('Models_Role')->find_one($user_type);

                        if($r){
                            $role = $r->rname;
                            $roleid = $user_type;
                            $user_type = $r->rname;
                        }
                        else{
                            $role = '';
                            $roleid = 0;
                            $user_type = 'Admin';
                        }

                        $errors = [];



//check with same name account is exist
                        $d = ORM::for_table('sys_users')->where('username',$username)->find_one();
                        if($d){
                            $errors[] = $_L['account_already_exist'];
                        }


                        // create Roles




                        if(empty($errors)){

                            $password = Password::_crypt($password);
                            // Add Account
                            $d = ORM::for_table('sys_users')->create();
                            $d->username = $username;
                            $d->password = $password;
                            $d->fullname = $fullname;
                            $d->user_type = $user_type;

                            //others
                            $d->phonenumber = '';
                            $d->last_login = date('Y-m-d H:i:s');
                            $d->email = '';
                            $d->creationdate = date('Y-m-d H:i:s');
                            $d->pin = '';
                            $d->img = '';
                            $d->otp = 'No';
                            $d->pin_enabled = 'No';
                            $d->api = 'No';
                            $d->pwresetkey = '';
                            $d->keyexpire = '';
                            $d->status = 'Active';
                            $d->role = $role;
                            $d->roleid = $roleid;


                            //

                            $d->save();

                           // r2(U . 'settings/users', 's', $_L['account_created_successfully']);

                            jsonResponse([
                                'error' => false,
                                'user_id' => $d->id(),
                                'message' => $_L['account_created_successfully']
                            ]);

                        }
                        else{
                            jsonResponse([
                                'error' => true,
                                'message' => $errors
                            ]);
                        }


                        break;


                    case 'PUT':


                        break;


                    case 'DELETE':


                        break;


                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }


                break;



            case 'invoice':

                switch ($method){

                    case 'GET':

                        $id = route(3);

                        $invoice = Invoice::find($id)->toArray();

                        if($invoice)
                        {
                            $items = InvoiceItem::where('invoiceid',$id)->get()->toArray();
                            jsonResponse([
                                'invoice' => $invoice,
                                'items' => $items
                            ]);
                        }
                        else{
                            jsonResponse([
                                'error' => true,
                                'message' => 'Invoice not found!'
                            ]);
                        }


                        break;


                    case 'POST':

                        $items = $_POST['items'];

                        $i = 0;

                        $description = [];
                        $item_number = [];
                        $qty = [];
                        $amount = [];
                        $tax_rate = [];

                        foreach ($items as $api_item)
                        {
                            $description[$i] = $api_item['description'];
                            $item_number[$i] = $api_item['item_code'];
                            $qty[$i] = $api_item['qty'];
                            $amount[$i] = $api_item['amount'];
                            $taxed[$i] = $api_item['taxed'];

                            $i++;
                        }

                        $cid = _post('cid');
                        $admin_id = _post('admin_id');

                        if($admin_id == ''){
                            $admin_id = 0;
                        }

                        // find user with cid

                        $u = ORM::for_table('crm_accounts')->find_one($cid);
                        $errors = [];
                        if ($cid == '') {
                            $errors[] = $_L['select_a_contact'];
                        }

                        $notes = _post('notes');

                        $show_quantity_as = _post('show_quantity_as');

                        // find currency

                        $currency_id = _post('currency');
                        $currency_find = Currency::where('iso_code',$currency_id)->first();
                        if ($currency_find) {
                            $currency = $currency_find->id;
                            $currency_symbol = $currency_find->symbol;
                            $currency_rate = $currency_find->rate;
                        }
                        else {
                            $currency = 0;
                            $currency_symbol = $config['currency_code'];
                            $currency_rate = 1.0000;
                        }

                        if (empty($amount)) {
                            $errors[] = $_L['at_least_one_item_required'];
                        }

                        $idate = _post('idate');
                        $its = strtotime($idate);
                        $duedate = _post('duedate');
                        $dd = '';
                        if ($duedate == 'due_on_receipt') {
                            $dd = $idate;
                        }
                        elseif ($duedate == 'days3') {
                            $dd = date('Y-m-d', strtotime('+3 days', $its));
                        }
                        elseif ($duedate == 'days5') {
                            $dd = date('Y-m-d', strtotime('+5 days', $its));
                        }
                        elseif ($duedate == 'days7') {
                            $dd = date('Y-m-d', strtotime('+7 days', $its));
                        }
                        elseif ($duedate == 'days10') {
                            $dd = date('Y-m-d', strtotime('+10 days', $its));
                        }
                        elseif ($duedate == 'days15') {
                            $dd = date('Y-m-d', strtotime('+15 days', $its));
                        }
                        elseif ($duedate == 'days30') {
                            $dd = date('Y-m-d', strtotime('+30 days', $its));
                        }
                        elseif ($duedate == 'days45') {
                            $dd = date('Y-m-d', strtotime('+45 days', $its));
                        }
                        elseif ($duedate == 'days60') {
                            $dd = date('Y-m-d', strtotime('+60 days', $its));
                        }
                        else {
                            $errors[] = 'Invalid Date';
                        }

                        if (!$dd) {
                            $errors[] = 'Date Parsing Error';
                        }

                        $repeat = _post('repeat');
                        $nd = $idate;
                        if ($repeat == '0') {
                            $r = '0';
                        }
                        elseif ($repeat == 'daily') {
                            $r = '+1 day';
                            $nd = date('Y-m-d', strtotime('+1 day', $its));
                        }
                        elseif ($repeat == 'week1') {
                            $r = '+1 week';
                            $nd = date('Y-m-d', strtotime('+1 week', $its));
                        }
                        elseif ($repeat == 'weeks2') {
                            $r = '+2 weeks';
                            $nd = date('Y-m-d', strtotime('+2 weeks', $its));
                        }
                        elseif ($repeat == 'weeks3') {
                            $r = '+3 weeks';
                            $nd = date('Y-m-d', strtotime('+3 weeks', $its));
                        }
                        elseif ($repeat == 'weeks4') {
                            $r = '+4 weeks';
                            $nd = date('Y-m-d', strtotime('+4 weeks', $its));
                        }
                        elseif ($repeat == 'month1') {
                            $r = '+1 month';
                            $nd = date('Y-m-d', strtotime('+1 month', $its));
                        }
                        elseif ($repeat == 'months2') {
                            $r = '+2 months';
                            $nd = date('Y-m-d', strtotime('+2 months', $its));
                        }
                        elseif ($repeat == 'months3') {
                            $r = '+3 months';
                            $nd = date('Y-m-d', strtotime('+3 months', $its));
                        }
                        elseif ($repeat == 'months6') {
                            $r = '+6 months';
                            $nd = date('Y-m-d', strtotime('+6 months', $its));
                        }
                        elseif ($repeat == 'year1') {
                            $r = '+1 year';
                            $nd = date('Y-m-d', strtotime('+1 year', $its));
                        }
                        elseif ($repeat == 'years2') {
                            $r = '+2 years';
                            $nd = date('Y-m-d', strtotime('+2 years', $its));
                        }
                        elseif ($repeat == 'years3') {
                            $r = '+3 years';
                            $nd = date('Y-m-d', strtotime('+3 years', $its));
                        }
                        else {
                            $errors[] = 'Date Parsing Error';
                        }

                        if(empty($errors)){

                           // $qty = $_POST['qty'];
                          //  $item_number = $_POST['item_code'];

//                            if (isset($_POST['taxed'])) {
//                                $taxed = $_POST['taxed'];
//                            }
//                            else {
//                                $taxed = false;
//                            }

                            $sTotal = '0';
                            $taxTotal = '0';
                            $i = '0';
                            $a = array();


                            $taxval = '0.00';
                            $taxname = '';
                            $taxrate = '0.00';


                            $taxed_amount = 0.00;
                            $lamount = 0.00;


                            foreach($amount as $samount) {
                                $samount = Finance::amount_fix($samount);
                                $a[$i] = $samount;

                                $sqty = $qty[$i];
                                $sqty = Finance::amount_fix($sqty);

                                $lTaxRate = $taxed[$i];
                                $lTaxRate = Finance::amount_fix($lTaxRate);


                                $sTotal+= $samount * ($sqty);
                                $lamount = $samount * ($sqty);

                                $lTaxVal = ($lamount*$lTaxRate)/100;

                                $taxed_amount += $lTaxVal;

                                $i++;
                            }




                            $invoicenum = _post('invoicenum');
                            $cn = _post('cn');
                            $fTotal = $sTotal;
                            $discount_amount = _post('discount_amount');
                            $discount_type = _post('discount_type');
                            $discount_value = '0.00';
                            if ($discount_amount == '0' OR $discount_amount == '') {
                                $actual_discount = '0.00';
                            }
                            else {
                                if ($discount_type == 'f') {
                                    $actual_discount = $discount_amount;
                                    $discount_value = $discount_amount;
                                }
                                else {
                                    $discount_type = 'p';
                                    $actual_discount = ($sTotal * $discount_amount) / 100;
                                    $discount_value = $discount_amount;
                                }
                            }

                            $actual_discount = number_format((float)$actual_discount, 2, '.', '');
                            $fTotal = $fTotal + $taxed_amount - $actual_discount;



                            $status = _post('status');
                            if ($status != 'Draft') {
                                $status = 'Unpaid';
                            }

                            $receipt_number = _post('receipt_number');

                            $datetime = date("Y-m-d H:i:s");
                            $vtoken = _raid(10);
                            $ptoken = _raid(10);
                            $d = ORM::for_table('sys_invoices')->create();
                            $d->userid = $cid;
                            $d->account = $u['account'];
                            $d->date = $idate;
                            $d->duedate = $dd;
                            $d->datepaid = $datetime;
                            $d->subtotal = $sTotal;
                            $d->discount_type = $discount_type;
                            $d->discount_value = $discount_value;
                            $d->discount = $actual_discount;
                            $d->total = $fTotal;
                            $d->tax = $taxed_amount;
                            $d->taxname = '';
                            $d->taxrate = 0.00;
                            $d->vtoken = $vtoken;
                            $d->ptoken = $ptoken;
                            $d->status = $status;
                            $d->notes = $notes;
                            $d->r = $r;
                            $d->nd = $nd;

                            $d->aid = $admin_id;

                            $d->show_quantity_as = $show_quantity_as;


                            $d->invoicenum = $invoicenum;
                            $d->cn = $cn;
                            $d->tax2 = '0.00';
                            $d->taxrate2 = '0.00';
                            $d->paymentmethod = '';



                            $d->currency = $currency;
                            $d->currency_symbol = $currency_symbol;
                            $d->currency_rate = $currency_rate;


                            $d->receipt_number = $receipt_number;


                            $d->save();
                            $invoiceid = $d->id();
                          //  $description = $_POST['desc'];

                            $i = '0';

                            foreach($description as $item) {


                                $samount = $a[$i];
                                $samount = Finance::amount_fix($samount);
                                if ($item == '' && $samount == '0.00') {
                                    $i++;
                                    continue;
                                }

                                $tax_rate = $taxed[$i];

                                $sqty = $qty[$i];
                                $sqty = Finance::amount_fix($sqty);
                                $ltotal = ($samount) * ($sqty);
                                $d = ORM::for_table('sys_invoiceitems')->create();
                                $d->invoiceid = $invoiceid;
                                $d->userid = $cid;
                                $d->description = $item;
                                $d->qty = $sqty;
                                $d->amount = $samount;
                                $d->total = $ltotal;



                                if($tax_rate == '' || $tax_rate == '0'){
                                    $tax_rate = 0.00;
                                    $d->taxed = '0';
                                }
                                else{
                                    $tax_rate = $taxed[$i];
                                    $d->taxed = '1';
                                }

                                $d->tax_rate = $tax_rate;

                                $d->type = '';
                                $d->relid = '0';
                                $d->itemcode = $item_number[$i];
                                $d->taxamount = '0.00';
                                $d->duedate = date('Y-m-d');
                                $d->paymentmethod = '';
                                $d->notes = '';



                                $d->save();

                                Inventory::decreaseByItemNumber($item_number[$i], $sqty);



                                $item_r = Item::where('name', $item)->first();
                                if ($item_r) {
                                    $item_r->sold_count = $item_r->sold_count + $sqty;
                                    $item_r->total_amount = $item_r->total_amount + $samount;
                                    $item_r->save();
                                }

                                $i++;
                            }



                            jsonResponse([
                                'error' => false,
                                'invoice_id' => $d->id(),
                                'message' => 'Invoice created successfully'
                            ]);

                        }
                        else {
                            jsonResponse([
                                'error' => true,
                                'message' => $errors
                            ]);
                        }


                        break;


                    case 'PUT':

                        $id = route(3);

                        $invoice = Invoice::find($id);

                        if($invoice)
                        {

                            parse_str(file_get_contents("php://input"),$params);

                            if(isset($params['status']))
                            {
                                $invoice->status = $params['status'];
                            }



                            $invoice->save();

                            jsonResponse([
                                'invoice_id' => $id,
                                'message' => 'Invoice updated!'
                            ]);
                        }
                        else{
                            jsonResponse([
                                'error' => true,
                                'message' => 'Invoice not found!'
                            ]);
                        }
                        break;


                    case 'DELETE':


                        break;


                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }


                break;



            case 'roles':

                switch ($method){

                    case 'GET':

                        jsonResponse(DB::table('sys_roles')->get()->toArray());


                        break;


                    case 'POST':




                        break;


                    case 'PUT':


                        break;


                    case 'DELETE':


                        break;


                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }


                break;



            case 'system':


                if(APP_STAGE == 'Demo')
                {
                    jsonResponse([
                        'error' => true,
                        'message' => 'Not available in demo!'
                    ]);
                }

                switch ($method){

                    case 'GET':

                        jsonResponse(Util::systemInfo());


                        break;


                    case 'POST':

                        $cmd = _post('cmd');

                        switch ($cmd)
                        {
                            case 'backup_files':

                                clxPerformLongProcess();

                                $taskId = _post('task_id');

                                if($taskId == '')
                                {
                                    jsonResponse([
                                        'error' => true,
                                        'message' => 'Please provide a task id.'
                                    ]);
                                }

                                $backup = Util::backupFiles($taskId);

                                jsonResponse($backup);


                                break;

                            case 'backup_database':


                                clxPerformLongProcess();
                                $taskId = _post('task_id');

                                if($taskId == '')
                                {
                                    jsonResponse([
                                        'error' => true,
                                        'message' => 'Please provide a task id.'
                                    ]);
                                }


                                $backup = Util::backupDatabase($taskId);

                                jsonResponse($backup);


                                break;

                            case 'task_log':
                                $taskId = _post('task_id');

                                if($taskId == '')
                                {
                                    jsonResponse([
                                        'error' => true,
                                        'message' => 'Please provide a task id.'
                                    ]);
                                }

                                jsonResponse(Util::getTaskLog($taskId));

                                break;

                            case 'upload_file':

                                $upload = Util::fileUpload();
                                jsonResponse($upload);

                                break;

                            case 'download_file':

                                $file_url = _post('file_url');
                                $file_name = _post('file_name');

                                $download = Util::downloadFile($file_url,$file_name);
                                jsonResponse($download);

                                break;

                            case 'unzip_file':

                                $file_name = _post('file_name');

                                $unzip = Util::extractFile($file_name);

                                jsonResponse($unzip);


                                break;


                            case 'schema_update':

                                $message = Update::singleCommand();
                                updateOption('build',$file_build);
                                jsonResponse([
                                    'success' => true,
                                    'message' => $message
                                ]);


                                break;


                        }


                        break;


                    case 'PUT':


                        break;


                    case 'DELETE':


                        break;


                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }


                break;



            default:

                jsonResponse([
                    'error' => true,
                    'message' => 'Unknown resources requested!'
                ]);

                break;

        }




        break;


    case 'create-lead':

        $validator = new Validator;
        $data = $request->all();

        if(has_data($data,'last_name'))
        {
            $lead = new Lead;
            $create = Leads::create($data);
            jsonResponse([
                'success' => true
            ]);
        }
        else{
            jsonResponse([
                'success' => false,
                'errors' => [
                    'Last name is required'
                ]
            ]);
        }

        break;


    case 'client-auth':

        $validator = new Validator;
        $data = $request->all();
        $validation = $validator->validate($data, [
            'username' => 'required|email',
            'password' => 'required',
        ]);




        if ($validation->fails()) {
            jsonResponse([
                'success' => false,
                'errors' => $validation->errors()->all()
            ]);
        } else {
            $client = Contact::where('email',$data['username'])->first();

            if($client)
            {
                if(Password::_verify($data['password'],$client->password) == true){

                    if($client->autologin == '')
                    {
                        $token = Ib_Str::random_string(20).$id.time();
                        $client->autologin = $token;
                        $client->save();

                    }
                    else{
                        $token = $client->autologin;
                    }
                    jsonResponse([
                        'success' => true,
                        'client' => [
                            'name' => $client->account,
                            'email' => $client->email,
                            'image' => $client->img,
                            'api_key' => $token
                        ]
                    ]);
                }
                else{
                    jsonResponse([
                        'success' => false,
                        'errors' => [
                            'Invalid Password'
                        ]
                    ]);
                }
            }

            else{
                jsonResponse([
                    'success' => false,
                    'errors' => [
                        'Invalid username or password'
                    ]
                ]);
            }

        }

        break;



    case 'client':

        if(isset($_GET['_METHOD']))
        {
            $method = $_GET['_METHOD'];
        }
        else{
            $method = $_SERVER['REQUEST_METHOD'];
        }

        header('Access-Control-Allow-Origin: *');
        $client = clientApiAuth();



        $account = [
            'name' => $client->account,
            'email' => $client->email,
            'phone' => $client->phone,
	        'address' => $client->address,
	        'city' => $client->city,
	        'state' => $client->state,
	        'zip' => $client->zip,
	        'country' => $client->country,
	        'image' => $client->image
        ];

        $object = route(2);

        switch ($object) {

            case 'dashboard':

                switch ($method) {

                    case 'GET':

                        $invoices = Invoice::where('userid',$client->id)
                            ->orderBy('id','desc')
                            ->limit(5)
                            ->get();

                        $recent_invoices = [];

                        foreach ($invoices as $invoice)
                        {
                            $recent_invoices[] = [
                                'client' => $invoice->account,
                                'amount' => $invoice->total,
                                'amount_formatted' => formatCurrency($invoice->total,$invoice->currency_iso_code),
                                'issued_date' => $invoice->date,
                                'issued_date_formatted' => date( $config['df'], strtotime($invoice->date)),
                                'due_date' => $invoice->due_date,
                                'due_date_formatted' => date( $config['df'], strtotime($invoice->duedate)),
                                'status' => $invoice->status,
                                'preview_url' => getInvoicePreviewUrl($invoice)

                            ];
                        }

                        $recent_transactions = [];

                        jsonResponse([
                            'account' => $account,
                            'recent_invoices' => $recent_invoices,
                            'recent_transactions' => $recent_transactions,
                        ]);

                        break;


                    case 'POST':


                        break;


                    case 'PUT':


                        break;


                    case 'DELETE':


                        break;


                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }


                break;


	        case 'invoices':

		        switch ($method) {

			        case 'GET':

				        $invoices = Invoice::where('userid',$client->id)
				                           ->orderBy('id','desc')
				                           ->limit(100)
				                           ->get();

				        $invoices_data = [];

				        foreach ($invoices as $invoice)
				        {
					        $invoices_data[] = [
						        'client' => $invoice->account,
						        'amount' => $invoice->total,
						        'amount_formatted' => formatCurrency($invoice->total,$invoice->currency_iso_code),
						        'issued_date' => $invoice->date,
						        'issued_date_formatted' => date( $config['df'], strtotime($invoice->date)),
						        'due_date' => $invoice->due_date,
						        'due_date_formatted' => date( $config['df'], strtotime($invoice->duedate)),
						        'status' => $invoice->status,
						        'preview_url' => getInvoicePreviewUrl($invoice)

					        ];
				        }

				        jsonResponse([
					        'account' => $account,
					        'invoices' => $invoices_data
				        ]);

				        break;

		        }

	        	break;


	        case 'quotes':

		        switch ($method) {

			        case 'GET':

				        $quotes = Quote::where('userid',$client->id)
				                           ->orderBy('id','desc')
				                           ->limit(1000)
				                           ->get();

				        $quotes_data = [];


				        foreach ($quotes as $quote)
				        {
					        $quotes_data[] = [
					        	'subject' => $quote->subject,
						        'total' => $quote->total,
						        'total_formatted' => formatCurrency($quote->total,$config['home_currency']),
						        'issued_date' => $quote->datecreated,
						        'valid_until_date' => $quote->validuntil,
						        'issued_date_formatted' => date( $config['df'], strtotime($quote->datecreated)),
						        'valid_until_date_formatted' => date( $config['df'], strtotime($quote->validuntil)),
						        'status' => $quote->status,
						        'preview_url' => U . 'client/q/' . $quote->id . '/token_' . $quote->vtoken

					        ];
				        }

				        jsonResponse([
					        'account' => $account,
					        'quotes' => $quotes_data
				        ]);

				        break;

		        }

		        break;

	        case 'transactions':

	        	switch ($method)
		        {
			        case 'GET':

			        	$transactions = Transaction::where('payerid',$client->id)
					        ->orWhere('payeeid',$client->id)
					        ->limit(1000)
					        ->get();

			        	$transactions_data = [];

			        	foreach ($transactions as $transaction)
				        {
				        	$transactions_data[] = [
				        		'date' => $transaction->date,
						        'date_formatted' => date( $config['df'], strtotime($transaction->date)),
						        'account' => $transaction->account,
						        'amount' => $transaction->amount,
						        'amount_formatted' => formatCurrency($transaction->amount,$config['home_currency']),
						        'description' => $transaction->description
					        ];
				        }

				        jsonResponse([
					        'account' => $account,
					        'transactions' => $transactions_data
				        ]);

			        	break;
		        }

	        	break;

	        case 'articles':
	        case 'kb':

	        switch ($method)
	        {
		        case 'GET':

			        $articles = Knowledgebase::where('status','Published')
			                                           ->where('is_public',1)

			                                           ->select('id','title','slug')
			                                           ->get();

			        $articles_data = [];

			        foreach ($articles as $article)
			        {
				        $articles_data[] = [
					        'id' => $article->id,
					        'title' => $article->title,
					        'slug' => $article->slug
				        ];
			        }

			        jsonResponse([
				        'account' => $account,
				        'articles' => $articles_data
			        ]);

			        break;
	        }

	        	break;


	        case 'tickets':

		        switch ($method)
		        {
			        case 'GET':

				        $tickets = Ticket::where('userid',$client->id)
				                                 ->select([
				                                 	'id',
					                                 'tid',
					                                 'did',
					                                 'aid',
					                                 'dname',
					                                 'account',
					                                 'email',
					                                 'created_at',
					                                 'updated_at',
					                                 'subject',
					                                 'status',
					                                 'urgency',
					                                 'admin',
					                                 'last_reply'
				                                 ])
				                                 ->get();

				        $tickets_data = [];

				        foreach ($tickets as $ticket)
				        {
					        $tickets_data[] = [
						        'id' => $ticket->id,
						        'ticket_id' => $ticket->tid,
						        'ticket_department_id' => $ticket->did,
						        'ticket_department_name' => $ticket->dname,
						        'email' => $ticket->email,
						        'created_at' => $ticket->created_at,
						        'created_at_formatted' => date( $config['df'], strtotime($ticket->created_at)),
						        'updated_at' => $ticket->updated_at,
						        'updated_at_formatted' => date( $config['df'], strtotime($ticket->updated_at)),
						        'subject' => $ticket->subject,
						        'status' => $ticket->status,
						        'urgency' => $ticket->urgency,
						        'last_reply' => $ticket->last_reply
					        ];
				        }

				        jsonResponse([
					        'account' => $account,
					        'tickets' => $tickets_data
				        ]);

				        break;
		        }

	        	break;


	        case 'orders':

		        switch ($method)
		        {
			        case 'GET':

				        $orders = Order::where('cid',$client->id)
				                         ->select([
					                         'id',
					                         'ordernum',
					                         'status',
					                         'date_added',
					                         'amount',
				                         ])
				                         ->get();

				        $orders_data = [];

				        foreach ($orders as $order)
				        {
					        $orders_data[] = [
						        'id' => $order->id,
						        'order_number' => $order->ordernum,
						        'status' => $order->status,
						        'date' => $order->date_added,
						        'date_formatted' => date( $config['df'], strtotime($order->date_added)),
						        'amount' => $order->amount,
						        'amount_formatted' => formatCurrency($order->amount,$config['home_currency']),
					        ];
				        }

				        jsonResponse([
					        'account' => $account,
					        'orders' => $orders_data
				        ]);

				        break;
		        }

		        break;


            case 'profile':
                switch ($method) {

                    case 'GET':

                        jsonResponse([
                            'account' => $account,
                        ]);

                        break;


                    case 'POST':


                        break;


                    case 'PUT':


                        break;


                    case 'DELETE':


                        break;


                    default:

                        jsonResponse([
                            'error' => true,
                            'message' => 'Unknown Method!'
                        ]);

                        break;
                }

                break;

	        case 'items':

		        switch ($method) {

			        case 'GET':

			        	$items = Item::select([
			        		'id',
					        'name',
					        'sales_price',
					        'item_number',
					        'type',
					        'image'
				        ])->orderBy('id','desc')->get();


			        	$items_data = [];

			        	foreach ($items as $item)
				        {
				        	$image = '';
				        	$image_thumbnail = '';

				        	if($item->image != '')
					        {
					        	$image = APP_URL.'/storage/items/'.$item->image;
					        }

					        if($item->image_thumbnail != '')
					        {
						        $image_thumbnail = APP_URL.'/storage/items/thumb_'.$item->image;
					        }

				        	$items_data[] = [
				        		'id' => $item->id,
						        'name' => $item->name,
						        'price' => $item->sales_price,
						        'type' => $item->type,
						        'image' => $image,
						        'image_thumbnail' => $image_thumbnail
					        ];
				        }

				        jsonResponse([
					        'account' => $account,
					        'items' => $items_data
				        ]);

				        break;


			        case 'POST':


				        break;


			        case 'PUT':


				        break;


			        case 'DELETE':


				        break;


			        default:

				        jsonResponse([
					        'error' => true,
					        'message' => 'Unknown Method!'
				        ]);

				        break;
		        }

	        	break;

            default:

                jsonResponse([
                    'error' => true,
                    'message' => 'Unknown request!'
                ]);

                break;
        }

        break;





        case 'v3':
          if(isset($_GET['_METHOD']))
          {
              $method = $_GET['_METHOD'];

          }
          else{
              $method = $_SERVER['REQUEST_METHOD'];

          }

          header('Access-Control-Allow-Origin: *');

          apiAuth();

          $object = route(2);

          switch($object){

            case 'getappsettings': //{{url}}getappsettings&api_key=oufgjolled9ctmkb0fuhlj9wsst7t3u8lcyvapv8

            $request = new stdClass();
            $request->lang = $_GET["lang"];
            $request->api_key = $_GET["api_key"];

            $response = new stdClass();
            $response->notification_sound_url = APP_URL.'/ui/assets/driver/sound/food_song.mp3';
            $response->enabled_signup = "";
            $response->vibrate_interval = "3000";
            $response->admin_country_set  = "SG";
            $response->record_track_Location = "";
            $response->disabled_tracking_bg = "";
            $response->track_interval = "8000";
            $response->app_language = "en";
            $response->app_name = "Mobilecity Delivery";
            $response->translation = "";
            $response->hide_total = "";

            //  $req["api_key"] = $_GET['api_key'];
              jsonResponse([
                  'code' => 1,
                  'msg' => 'OK',
                  'details' =>  $response,
                  'request' =>$request
              ]);
            break;

            case 'signup':
            $request = new stdClass();
            $request->lang = $_GET["lang"];
            $request->api_key = $_GET["api_key"];
            $request->transport_type_id = $_GET['transport_type_id'];
            $request->first_name = $_GET['first_name'];
            $request->last_name = $_GET['last_name'];
            $request->email = $_GET['email'];
            $request->phone = $_GET['phone'];
            $request->username = $_GET['username'];
            $request->password = $_GET['password'];
            $request->transport_description = $_GET['transport_description'];
            $request->licence_plate = $_GET['licence_plate'];
            $request->color = $_GET['color'];
            $request->device_platform = $_GET['device_platform'];

            jsonResponse([
                'code' => 1,
                'msg' => 'Signup successful',
                'details' =>  "",
                'request' =>$request
            ]);
            break;
            case 'login':
            $request = new stdClass();
            $request->lang = $_GET["lang"];
            $request->username = $_GET["username"];
            $request->password = $_GET["password"];
            $request->remember = $_GET["remember"];
            $request->device_id = $_GET["device_id"];
            $request->device_platform = $_GET["device_platform"];


            $request->username = filter_var($request->username, FILTER_SANITIZE_STRING);
            $request->username = addslashes($request->username);
            $request->password = addslashes($request->password);
              $auth = Admin::login($request->username,$request->password);
              $today_date = Date('M, d');
              $todays_date_raw  = Date('Y-m-d');
              if($auth){
              //  printf("uniqid('', true): %s\r\n", uniqid('', true)); die();
              $c = ORM::for_table('sys_users')->where('username',  $request->username)->where_like('user_type','%Driver%')->find_one();

              if($c){
                  if($c->token==""){
                    $uuid =   uniqid($c->id, false);

                    $c->token = $uuid;
                    $c->device_id = $request->device_id;
                    $c->device_platform = $request->device_platform;
                  //  print_r($c->token); die();
                    $c->save();

                  }else{
                    $uuid = $c->token;
                    $c->device_id = $request->device_id;
                    $c->device_platform = $request->device_platform;
                    //$c->save();
                  }

            $driver_available =  ORM::for_table('driver_availablity')->where('driver_id',  $c->id)->where('date',date('Y-m-d'))->find_one();
           if($driver_available) {
               $on_duty = $driver_available->onduty;
           }
           else{
               $on_duty = "1";
           }

                    jsonResponse([
                        'code' => 1,
                        'msg' => "Login Successful",
                        "username" =>  $request->username,
                        "password" => $request->password,
                        "user_id" => $c->id,

                        "remember" => "on",
                        "todays_date" => $today_date,
                        "todays_date_raw" => $todays_date_raw,

                        "on_duty" => $on_duty,
                        "token" => $uuid,
                        "duty_status" => $on_duty,
                        "location_accuracy" => 2,
                        "request" => $request

                    ]);
                  }
                  else{
                    jsonResponse([
                        'code' => 2,
                        'msg' => "Only Driver can login to the app",
                        "details" => "",
                        "request" => $request

                    ]);
                  }
              }
              else{
                jsonResponse([
                    'code' => 2,
                    'msg' => "Login failed. either username or password is incorrect",
                    "details" => "",
                    "request" => $request

                ]);

              }

            case 'changedutystatus':
            $request = new stdClass();
            $request->lang = $_GET["lang"];
            $request->api_key = $_GET["api_key"];
            if(isset($_GET["date"])){
                $request->date = $_GET["date"];
            }
            else {
                $request->date = date('Y-m-d');
            }
            $request->onduty = $_GET["onduty"];
            $request->token = $_GET["token"];




              $c = ORM::for_table('sys_users')->select('sys_users.id')->where('token', $request->token)->find_one();
              $driver_id = $c['id'];
              $x = ORM::for_table('driver_availablity')->where('date', $request->date)->where('driver_id', $driver_id)->find_one();

               if($x){
                $x->onduty = $request->onduty;
              //  print_r($c->token); die();
                $x->save();

              }
              else{
                $d = ORM::for_table('driver_availablity')->create();
              $d->onduty = $request->onduty;

              $d->date = $request->date;
              $d->driver_id = $driver_id;
              $d->save();

              }    //
              jsonResponse([
                  'code' => 1,
                  'msg' => "OK",
                  "details" => $request->onduty,
                  "request" => $request

              ]);


            break;
            default:
            jsonResponse([
                'error' => true,
                'message' => 'Unknown Request!'
            ]);

            break;

            case 'getnotifications':
              $request = new stdClass();
              $request->lang = $_GET["lang"];
              $request->api_key = $_GET["api_key"];
              $request->token = $_GET["token"];
              $c = ORM::for_table('sys_users')->select('sys_users.id')->where('token', $request->token)->find_one();
              $driver_id = $c['id'];
              $x = ORM::for_table('driver_pushlog')->where('is_read', '1')->where('driver_id', $driver_id)->find_many();
              //print_r($x);
              $count =0;
              //$push  = array();
              foreach ($x as $key => $value) {
                $response = new stdClass();
                $response->push_id = $value->id;
                $response->device_platform = $value->device_platform;
                $response->device_id = $value->device_id;
                $response->push_title = $value->push_title;
                $response->push_message = $value->push_message;
                $response->push_type = $value->push_type;
                $response->actions = $value->actions;
                $response->json_response = $value->json_response;
                $response->order_id = $value->order_id;
                $response->driver_id = $value->driver_id;
                $response->task_id = $value->task_id;
                $response->date_created = $value->date_created;
                $response->date_process = $value->date_process;
                $response->ip_address = $value->ip_address;
                $response->is_read = $value->is_read;
                $response->bulk_id = $value->bulk_id;
                $response->user_type = $value->user_type;
                $response->user_id = $value->user_id;

                //  print_r($value->push_id);
                $push[] = $response;
                $count++;
              }
        if($count){
          //echo "list";
          jsonResponse([
              'code' => 1,
              'msg' => "OK",
              "details" => $push,
              "request" => $request

          ]);
        }
        else{
          jsonResponse([
              'code' => 2,
              'msg' => "No notifications",
              "details" => "",
              "request" => $request

          ]);
        }

            break;

        case 'clearnofications':
        $request = new stdClass();
        $request->lang = $_GET["lang"];
        $request->api_key = $_GET["api_key"];
        $request->token = $_GET["token"];
        $c = ORM::for_table('sys_users')->select('sys_users.id')->where('token', $request->token)->find_one();
        $driver_id = $c['id'];

        $x = ORM::for_table('driver_pushlog')->where('is_read', '1')->where('driver_id', $driver_id)->find_many();
        foreach ($x as $key => $value) {
          $d = ORM::for_table('driver_pushlog')->where('id', $value->id)->find_one();
          //print_r($d->is_read);
          if($d){
          $d->is_read = 2;
          //$d->push_id = $value->push_id;
            $d->save();
        }

        }
        jsonResponse([
            'code' => 1,
            'msg' => "OK",
            "details" => "",
            "request" => $request

        ]);

        break;

        case 'calendartask':
          $request = new stdClass();
          $request->lang = $_GET["lang"];
          $request->api_key = $_GET["api_key"];
          $request->token = $_GET["token"];
          $request->start = trim($_GET["start"]);
          $request->end = trim($_GET["end"]);
        //  $from_date = trim($reportrange[0]);
          //$to_date = trim($reportrange[1]);

          $c = ORM::for_table('sys_users')->select('sys_users.id')->where('token', $request->token)->find_one();
          $driver_id = $c['id'];

            //$sql = "SELECT count(c2) as title, DATE_FORMAT(c2,'%Y-%m-%d') as c2 from sys_invoices where c5=13 GROUP BY DATE_FORMAT(c2,'%Y-%m-%d')";
          //$task =  ORM::for_table('sys_invoices')->select_expr("DATE_FORMAT(c2,'%Y-%m-%d')  as c2")->slec->where_gte('c2', $request->start)->where_lte('c2', $request->end)->where('c5', $driver_id)->group_by_expr("DATE_FORMAT('c2','%Y-%m-%d')")->find_many();
          $task = ORM::for_table('sys_invoices')->select_expr("count(c2) as title,DATE_FORMAT(c2,'%Y-%m-%d') as c2")->where_gte('c2', $request->start)->where_lte('c2', $request->end)->where('c5', $driver_id)->group_by_expr("DATE_FORMAT(c2,'%Y-%m-%d')")->find_many();

          $tasklist = array();
          foreach ($task as $key => $value) {
            //print_r($value->title." -- ".$value->c2. "<br/>");
            $daydetail = array();
            $daydetail["title"] = $value->title;
            $daydetail["id"] = $value->c2;
            $date = DateTime::createFromFormat("Y-m-d", $value->c2);
            //echo $date->format("Y");
            $daydetail["year"] = $date->format("Y");
            $daydetail["month"] = $date->format("m");
            $daydetail["day"] = $date->format("d");

            array_push($tasklist, $daydetail);
          }
          if(count($task)!=0){
          jsonResponse([
              'code' => 1,
              'msg' => "OK",
              "details" => $tasklist,
              "request" => $request

          ]);
        }
        else {
          jsonResponse([
              'code' => 2,
              'msg' => "",
              "details" => "",
              "request" => $request

          ]);
        }

        break;

        case 'gettaskbydate':
        $request = new stdClass();
        $request->lang = $_GET["lang"];
        $request->api_key = $_GET["api_key"];
        $request->token = $_GET["token"];
        $request->date = trim($_GET["date"]);
        $c = ORM::for_table('sys_users')->select('sys_users.id')->select('sys_users.fullname')->select('sys_users.device_id')->select('sys_users.phonenumber')->where('token', $request->token)->find_one();
        $driver_id = $c['id'];
        $task = ORM::for_table('sys_invoices')->where('c5',  $driver_id)->where_like('c2', $request->date.'%')->find_many();
        $tasklist = array();
        foreach ($task as $key => $value) {
          $daydetail = array();
          $daydetail["task_id"] = $value->id;//$value->cn;
          $daydetail["id"] = $value->id;
          $daydetail["order_id"] = $value->id;
          $daydetail['user_type'] = "admin";
          $daydetail['user_id'] = $value->userid;
          //$daydetail['task_description'] = "1";
          $invoice_detail = ORM::for_table('sys_invoiceitems')->where('invoiceid',  $value->id)->find_many();
          $task_description = "Deliver ";
          foreach ($invoice_detail as $inv) {
            $task_description .= strip_tags($inv->description);
          }

          $daydetail['task_description']  = $task_description;
          $daydetail['trans_type'] = "delivery";
          $customer_detail = ORM::for_table('crm_accounts')->where('id', $value->userid)->find_one();
          $daydetail['contact_number'] = $customer_detail["phone"];
          $daydetail['email_address'] = $customer_detail["email"];
          $daydetail['customer_name'] = $value->account;
          $daydetail['delivery_date'] = date('Y-m-d H:m:s',strtotime($value->c2));
          $daydetail['delivery_address'] = $value->shipping_street;
          $daydetail['team_id'] = "1";
          $daydetail['driver_id'] = $driver_id;
          $daydetail['task_lat'] = $value->c3;
          $daydetail['task_lng'] = $value->c4;
          $daydetail['customer_signature'] = $value->signature_data_source;
          $daydetail['status'] = strtolower($value->delivery_status);
          $daydetail['date_created'] = $value->date;
          $daydetail['date_modified'] = $value->date;
          $daydetail['ip_address'] = "183.90.37.208";
          $daydetail['auto_assign_type'] = "";
          $daydetail['assign_started'] = "0000-00-00 00:00:00";
          $daydetail['assignment_status'] = "";
          $daydetail['dropoff_merchant'] = "0";
          $daydetail['dropoff_contact_name'] = "";
          $daydetail['dropoff_contact_number']= "";
          $daydetail['drop_address'] = "";
          $daydetail["dropoff_lat"] = "";
          $daydetail["dropoff_lng"] = "";
          $daydetail["recipient_name"] = "";
          $daydetail["critical"] = "1";
          $daydetail["driver_name"] = $c["fullname"];
          $daydetail["device_id"] = $c["device_id"];
          $daydetail["driver_phone"] = $c["phonenumber"];
          $daydetail["driver_email"] = $c["username"];
          $daydetail["device_platform"] = $c["device_platform"];
          $daydetail["enabled_push"] = "1";
          $daydetail["driver_lat"] = "";
          $daydetail["driver_lng"] = "";
          $daydetail["merchant_id"] = "";
          $daydetail["merchant_name"] = "";
          $daydetail["merchant_address"] = "";
          $daydetail["team_name"] = "Local Delivery";
          $daydetail["total_w_tax"] = "";
          $daydetail["delivery_time"] = date('H:m',strtotime($value->c2));;
          $daydetail["status_raw"] = strtolower($value->delivery_status);
          $daydetail["trans_type_raw"] = "delivery";
          $daydetail["order_total_amount"] = 'SGD '.$value->total;
          //$daydetail["history"] : [];

          array_push($tasklist, $daydetail);
        }
        if(count($tasklist)!=0){
        jsonResponse([
            'code' => 1,
            'msg' => "OK",
            "details" => $tasklist,
            "request" => $request

        ]);
        }else {
          jsonResponse([
              'code' => 2,
              'msg' => "No task for the day",
              "details" => "",
              "request" => $request

          ]);
        }
        break;

        case 'taskdetails':
        $request = new stdClass();
        $request->api_key = $_GET["api_key"];
        $request->token = $_GET["token"];
        $request->task_id = $_GET["task_id"];
        $request->lang = $_GET["lang"];

        $detail = new stdClass();
        $detail->task_id = $request->task_id;
        $c = ORM::for_table('sys_users')->select('sys_users.id')->select('sys_users.fullname')->select('sys_users.device_id')->select('sys_users.phonenumber')->where('token', $request->token)->find_one();
        $driver_id = $c['id'];

        $taskdetail = ORM::for_table('sys_invoices')->where('id', $detail->task_id)->find_one();
        $detail->order_id = $taskdetail['id'];

        $detail->user_type = 'admin';//$taskdetail['id'];
        $detail->user_id = $taskdetail['userid'];
        $invoice_detail = ORM::for_table('sys_invoiceitems')->where('invoiceid',  $taskdetail['id'])->find_many();
        $task_description = "Deliver ";
        foreach ($invoice_detail as $inv) {
          $task_description .= strip_tags($inv->description);
        }
        $detail->task_description = $task_description;
        $detail->trans_type = "delivery";

          $customer_detail = ORM::for_table('crm_accounts')->where('id', $taskdetail['userid'])->find_one();
          //print_r($taskdetail['user_id']); die();
          $detail->contact_number = $customer_detail["phone"];
          $detail->email_address = $customer_detail["email"];
          $detail->customer_name = $taskdetail['account'];
          $detail->delivery_date = date('Y-m-d H:m:s',strtotime($taskdetail['c2']));
          $detail->delivery_address = $taskdetail['shipping_street'];
          $detail->team_id = "1";
          $detail->driver_id = $driver_id;
          $detail->task_lat = $taskdetail['c3'];
          $detail->task_lng = $taskdetail['c4'];
          $detail->customer_signature = $taskdetail['signature_data_source'];
          $detail->status = strtolower($taskdetail['delivery_status']);
          $detail->date_created = $taskdetail['date'];
          $detail->date_modified = $taskdetail['date'];
          $detail->ip_address = "183.90.37.208";
          $detail->auto_assign_type = "";
          $detail->assign_started = "0000-00-00 00:00:00";
          $detail->assignment_status = "";
          $detail->dropoff_merchant = "0";
          $detail->dropoff_contact_name = "";
          $detail->dropoff_contact_number= "";
          $detail->drop_address = "";
          $detail->dropoff_lat = "";
          $detail->dropoff_lng = "";
          $detail->recipient_name = "";
          $detail->critical = "1";
          $detail->driver_name = $c["fullname"];
          $detail->device_id = $c["device_id"];
          $detail->driver_phone = $c["phonenumber"];
          $detail->driver_email = $c["username"];
          $detail->device_platform = $c["device_platform"];
          $detail->enabled_push = "1";
          $detail->driver_lat = "";
          $detail->driver_lng = "";
          $detail->merchant_id = "";
          $detail->merchant_name = "";
          $detail->merchant_address = "";
          $detail->team_name = "Local Delivery";
          $detail->total_w_tax = "";
          $detail->delivery_time = date('H:m',strtotime($taskdetail['c2']));;
          $detail->status_raw = strtolower($taskdetail['delivery_status']);
          $detail->trans_type_raw = "delivery";
          $history = ORM::for_table('sys_driverhistory')->where('task_id',  $taskdetail['id'])->find_many();
          $his = array();
          foreach ($history as $key => $value) {

            $history_detail = new stdClass();

            $history_detail->id = $value->id;
            $history_detail->order_id = $value->task_id;
            $history_detail->status = strtolower($value->status);
            $history_detail->remarks = $value->remark;
            $history_detail->date_created = $value->created_at;
              $history_detail->ip_address = "183.90.37.208";
            $history_detail->task_id = $value->task_id;
              $history_detail->reason = "";
              $history_detail->customer_signature = "";
              $history_detail->notification_viewed = "";
              $history_detail->driver_id = $driver_id;
              $history_detail->driver_location_lat = $value->latitude;
              $history_detail->driver_location_lng = $value->longitude;;
              $history_detail->remarks2 = "";
              $history_detail->remarks_args = "";

              $history_detail->notes = $value->notes;
              $history_detail->photo_task_id = "0";
              $history_detail->received_by = "";
              $history_detail->signature_base30 = "";
              $history_detail->status_raw = strtolower($value->status);
              $history_detail->time = $value->time;
              $history_detail->date = $value->date; //Jul 20,2018*/

            array_push($his, $history_detail);
          }










          $detail->history = $his;

        $detail->customer_signature_url = $taskdetail['signature_data_source'];
        $map_icons = new stdClass();
        $map_icons->driver = APP_URL.'/ui/assets/driver/images/car.png';
        $map_icons->customer = APP_URL.'/ui/assets/driver/images/racing-flag.png';
        $map_icons->merchant = APP_URL.'/ui/assets/driver/images/restaurant-pin-32.png';
        $detail->map_icons = $map_icons;

        $detail->driver_enabled_notes = "";
        $detail->driver_enabled_signature = "";
        $detail->driver_enabled_addphoto = "1";
        $detail->task_photo = "2";
        $detail->history_notes = "2";
        $detail->enabled_resize_photo = "";
        $detail->photo_resize_width  = "";
        $detail->photo_resize_height = "";
        jsonResponse([
            'code' => 1,
            'msg' => "Task:".$request->task_id,
            "details" => $detail,
            "request" => $request

        ]);


        break;

        case 'updatedriverlocation':
        $request = new stdClass();
        $request->lat = $_GET["lat"];
        $request->lng = $_GET["lng"];
        $request->app_version = $_GET["app_version"];
        $request->altitude = $_GET["altitude"];
        $request->accuracy = $_GET["accuracy"];
        $request->altitudeaccuracy = $_GET["altitudeaccuracy"];
        $request->heading = $_GET["heading"];
        $request->speed = $_GET["speed"];
        $request->track_type = $_GET["track_type"];
        $request->lang = $_GET["lang"];
        $request->api_key = $_GET["api_key"];
        $request->token = $_GET["token"];
        $c = ORM::for_table('sys_users')->select('sys_users.id')->select('sys_users.fullname')->select('sys_users.device_id')->select('sys_users.phonenumber')->where('token', $request->token)->find_one();
        $driver_id = $c['id'];

        $x = ORM::for_table('driver_location')->where('driver_id', $driver_id)->find_one();
        if($x){
        $x->latitude =$_GET["lat"];
        $x->longitude =$_GET["lng"];
        $x->app_version =$_GET["app_version"];
        $x->altitude =$_GET["altitude"];
        $x->accuracy =$_GET["accuracy"];
        $x->altitudeaccuracy =$_GET["altitudeaccuracy"];
        $x->heading =$_GET["heading"];
        $x->speed =$_GET["speed"];
        $x->track_type =$_GET["track_type"];
        $x->date_time=date('y-m-d H:m:s');
        $x->save();


        }
        else{
          $d = ORM::for_table('driver_location')->create();
          $d->latitude =$_GET["lat"];
          $d->longitude =$_GET["lng"];
          $d->app_version =$_GET["app_version"];
          $d->altitude =$_GET["altitude"];
          $d->accuracy =$_GET["accuracy"];
          $d->altitudeaccuracy =$_GET["altitudeaccuracy"];
          $d->heading =$_GET["heading"];
          $d->speed =$_GET["speed"];
          $d->track_type =$_GET["track_type"];
          $d->date_time=date('y-m-d H:m:');
          $d->driver_id = $driver_id;
          $d->save();
        }

        jsonResponse([
            'code' => 1,
            'msg' => "Location Set",
            "details" => "",
            "request" => $request

        ]);


          break;

          case 'viewtaskdescription':
          $request = new stdClass();
          $request->api_key = $_GET["api_key"];
          $request->token = $_GET["token"];
          $request->task_id = $_GET["task_id"];
          $request->lang = $_GET["lang"];

          $detail = new stdClass();
          $detail->task_id = $request->task_id;
          $c = ORM::for_table('sys_users')->select('sys_users.id')->select('sys_users.fullname')->select('sys_users.device_id')->select('sys_users.phonenumber')->where('token', $request->token)->find_one();
          $driver_id = $c['id'];

          $taskdetail = ORM::for_table('sys_invoices')->where('id', $detail->task_id)->find_one();
          $detail->order_id = $taskdetail['id'];

          $detail->user_type = 'admin';//$taskdetail['id'];
          $detail->user_id = $taskdetail['userid'];
          $invoice_detail = ORM::for_table('sys_invoiceitems')->where('invoiceid',  $taskdetail['id'])->find_many();
          $task_description = "Deliver ";
          foreach ($invoice_detail as $inv) {
            $task_description .= strip_tags($inv->description);
          }
          $detail->task_description = $task_description;
          $detail->trans_type = "delivery";

            $customer_detail = ORM::for_table('crm_accounts')->where('id', $taskdetail['userid'])->find_one();
            //print_r($taskdetail['user_id']); die();
            $detail->contact_number = $customer_detail["phone"];
            $detail->email_address = $customer_detail["email"];
            $detail->customer_name = $taskdetail['account'];
            $detail->delivery_date = date('Y-m-d H:m:s',strtotime($taskdetail['c2']));
            $detail->delivery_address = $taskdetail['shipping_street'];
            $detail->team_id = "1";
            $detail->driver_id = $driver_id;
            $detail->task_lat = $taskdetail['c3'];
            $detail->task_lng = $taskdetail['c4'];
            $detail->customer_signature = $taskdetail['signature_data_source'];
            $detail->status = $taskdetail['delivery_status'];
            $detail->date_created = $taskdetail['date'];
            $detail->date_modified = $taskdetail['date'];
            $detail->ip_address = "183.90.37.208";
            $detail->auto_assign_type = "";
            $detail->assign_started = "0000-00-00 00:00:00";
            $detail->assignment_status = "";
            $detail->dropoff_merchant = "0";
            $detail->dropoff_contact_name = "";
            $detail->dropoff_contact_number= "";
            $detail->drop_address = "";
            $detail->dropoff_lat = "";
            $detail->dropoff_lng = "";
            $detail->recipient_name = "";
            $detail->critical = "1";
            $detail->driver_name = $c["fullname"];
            $detail->device_id = $c["device_id"];
            $detail->driver_phone = $c["phonenumber"];
            $detail->driver_email = $c["username"];
            $detail->device_platform = $c["device_platform"];
            $detail->enabled_push = "1";
            $detail->driver_lat = "";
            $detail->driver_lng = "";
            $detail->merchant_id = "";
            $detail->merchant_name = "";
            $detail->merchant_address = "";
            $detail->team_name = "Local Delivery";
            $detail->total_w_tax = "";
            $detail->delivery_time = date('H:m',strtotime($taskdetail['c2']));;
            $detail->status_raw = $taskdetail['delivery_status'];
            $detail->trans_type_raw = "delivery";
            $history_array = array();
            $history_detail["id"] = "0";
            $history_detail["order_id"] = "0";
            $history_detail["status"] = "acknowledged";
            $history_detail["remarks"] = "";
            $history_detail["date_created"] = "2018-07-20 18:01:17";
            $history_detail["ip_address"] = "183.90.37.208";
            $history_detail["task_id"] = "0";
            $history_detail["reason"] = "";
            $history_detail["customer_signature"] = "";
            $history_detail["notification_viewed"] = "";
            $history_detail["driver_id"] = $driver_id;
            $history_detail["driver_location_lat"] = "";
            $history_detail["driver_location_lng"] = "";
            $history_detail["remarks2"] = "";
            $history_detail["remarks_args"] = "";
            $history_detail["notes"] = "";
            $history_detail["photo_task_id"] = "0";
            $history_detail["received_by"] = "";
            $history_detail["signature_base30"] = "";
            $history_detail["status_raw"] = "acknowledged";
            $history_detail["time"] = "";
            $history_detail["date"] = ""; //Jul 20,2018
          $detail->history = array($history_detail);

          $detail->customer_signature_url = $taskdetail['signature_data_source'];
          $map_icons = new stdClass();
          $map_icons->driver = APP_URL.'/ui/assets/driver/images/car.png';
          $map_icons->customer = APP_URL.'/ui/assets/driver/images/racing-flag.png';
          $map_icons->merchant = APP_URL.'/ui/assets/driver/images/restaurant-pin-32.png';
          $detail->map_icons = $map_icons;

          $detail->driver_enabled_notes = "";
          $detail->driver_enabled_signature = "";
          $detail->driver_enabled_addphoto = "1";
          $detail->task_photo = "2";
          $detail->history_notes = "2";
          $detail->enabled_resize_photo = "";
          $detail->photo_resize_width  = "";
          $detail->photo_resize_height = "";
          jsonResponse([
              'code' => 1,
              'msg' => "Task:".$request->task_id,
              "details" => $detail,
              "request" => $request

          ]);

          break;

          case 'vieworderdetails':
          $request = new stdClass();
          $request->api_key = $_GET["api_key"];
          $request->token = $_GET["token"];
          $request->order_id = $_GET["order_id"];
          $request->lang = $_GET["lang"];


              jsonResponse([
                  'code' => 2,
                  "msg" => "Record not found",
                  "details" => "",
                  "request" => $request

              ]);

          break;

          case 'addsignaturetotask':
          $request = new stdClass();
          //$request->image = $_POST["file"];
          $request->task_id = $_GET["task_id"];
          $request->recipient_name = $_GET["recipient_name"];
          $request->signature_id = $_GET["signature_id"];
          $request->lang = $_GET["lang"];
          $request->api_key = $_GET["api_key"];
          $request->token = $_GET["token"];


          $uploader   =   new Uploader();
          $uploader->setDir('storage/driver/sign');
          $uploader->sameName(false);
          $uploader->setExtensions(array('jpg','jpeg','png','gif'));  //allowed extensions list//
          if($uploader->uploadFile('file')){
              $uploaded  =   $uploader->getUploadName();

              $file = $uploaded;
              $msg = $_L['Uploaded Successfully'];
              $success = 'Yes';

              // create thumb


          }else{//upload failed
              $file = '';
              $msg = $uploader->getMessage();
              $success = 'No';
          }

          $a = array(
              'success' => $success,
              'msg' =>$msg,
              'file' =>$file
          );
          $d = ORM::for_table('sys_invoices')->where('id', $_GET["task_id"])->find_one();
          $d->signature_data_source = $file;
          $d->save();

          jsonResponse([
              'code' => 1,
              "msg" => "Successfully Updated",
              "details" => "",
              "request" => $request

          ]);



          break;

          case 'loadsignature':
          $request = new stdClass();

          $request->task_id = $_GET["task_id"];
          $request->lang = $_GET["lang"];
          $request->api_key = $_GET["api_key"];
          $request->token = $_GET["token"];
          $d = ORM::for_table('sys_invoices')->where('id', $_GET["task_id"])->find_one();
        if($d){
          $detail = new stdClass();
          $detail->task_id = $d['id'];
          $detail->image = APP_URL.'/storage/driver/sign'.$d['signature_data_source'];
        if($d['signature_data_source']==""){
          jsonResponse([
              'code' => 2,
              "msg" => "No signature found",
              "details" => "",
              "request" => $request

          ]);
        }
        else{
          jsonResponse([
              'code' => 1,
              "msg" => "OK",
              "details" => $detail,
              "request" => $request

          ]);
        }
        }
        else{
          jsonResponse([
              'code' => 2,
              'msg' => "No signature found",
              "details" => "",
              "request" => $request

          ]);
        }
          break;

          case 'changetaskstatus':

          $request = new stdClass();

          $request->task_id = $_GET["task_id"];
          $request->status_raw = $_GET["status_raw"];

          $request->lang = $_GET["lang"];
          $request->api_key = $_GET["api_key"];
          $request->token = $_GET["token"];

          $driver_detail = ORM::for_table('sys_users')->select('sys_users.id')->select('sys_users.fullname')->where('token', $request->token)->find_one();
        //print_r($driver_detail['id']); die();
          $driver_location = ORM::for_table('driver_location')->select('driver_location.latitude')->select('driver_location.longitude')->where('driver_id', $driver_detail['id'])->find_one();
          ///print_r($driver_location['latitude']); die();

          $detail = new stdClass();
          $d = ORM::for_table('sys_invoices')->where('id', $_GET["task_id"])->find_one();

          $d->delivery_status = $request->status_raw;
          $d->save();
          $history =  ORM::for_table('sys_driverhistory')->create();
          $history->status = $_GET["status_raw"];

          $date=date('Y-m-d H:i:s');
          $cdate=date_create($date);
          $history->date = date_format($cdate, 'M d,Y');
          $history->time = date_format($cdate, "G:i");
          $history->task_id = $_GET["task_id"];
          $history->remark =  $driver_detail['fullname']." ".$_GET["status_raw"]." the task";
          $history->latitude = $driver_location["latitude"];
          $history->longitude = $driver_location["longitude"];
          $history->notes = "";
          //cprint_r($history->date); die();
          $history->save();

          $detail->task_id = $request->task_id;
          $detail->reload_functions = "TaskDetails";
          $detail->status_raw = $request->status_raw;
          jsonResponse([
              'code' => 1,
              'msg' => "OK",
              "details" => $detail,
              "request" => $request

          ]);


          break;



        case 'getprofile':
        $request = new stdClass();
        $request->lang = $_GET["lang"];
        $request->api_key = $_GET["api_key"];
        $request->token = $_GET["token"];
        $c = ORM::for_table('sys_users')->select('sys_users.id')->select('sys_users.fullname')->select('sys_users.device_id')->select('sys_users.phonenumber')->select('sys_users.username')->select('sys_users.transport_type')->select('sys_users.vehicle_number')->where('token', $request->token)->find_one();
        $driver_id = $c['id'];
        $driver_detail = new stdClass();
        $driver_detail->full_name = $c['fullname'];
        $driver_detail->team_name = "Local Deliver";
        $driver_detail->email = $c['username'];
        $driver_detail->phone = $c['phonenumber'];
        $driver_detail->transport_type_id = strtolower($c['transport_type']);
        $driver_detail->transport_type_id2 = $c['transport_type'];
        $driver_detail->transport_description = "";
        $driver_detail->licence_plate = $c['vehicle_number'];
        $driver_detail->color = "";
        $driver_detail->profile_photo = "";
        jsonResponse([
            'code' => 1,
            'msg' => "OK",
            "details" => $driver_detail,
            "request" => $request

        ]);
        break;

        case 'profilechangepassword':
        $request = new stdClass();
        $request->current_pass = $_GET["current_pass"];
        $request->new_pass = $_GET["new_pass"];
        $request->confirm_pass = $_GET["confirm_pass"];
        $request->lang = $_GET["lang"];
        $request->api_key = $_GET["api_key"];
        $request->token = $_GET["token"];
        $c = ORM::for_table('sys_users')->select('sys_users.password')->select('sys_users.id')->where('token', $request->token)->find_one();
        //print_r($c['password']);
        $u = User::find($c['id']);
        $u->password = Password::_crypt(trim($request->new_pass));


        $u->save();

        jsonResponse([
            'code' => 1,
            "msg" => "Password Successfully Changed",
            "details" => $request->new_pass,
            "request" => $request

        ]);

        break;

        case 'logout':
        $request = new stdClass();
        $request->lang = $_GET["lang"];
        $request->api_key = $_GET["api_key"];
        $request->token = $_GET["token"];
        jsonResponse([
            'code' => 1,
            "msg" => "OK",
            "details" => "",
            "request" => $request

        ]);

        break;

        case 'forgotpassword':
          $request = new stdClass();
          $request->email = $_GET["email"];
          $request->api_key = $_GET["lang"];
          $request->token = $_GET["api_key"];
          jsonResponse([
              'code' => 1,
              'msg' => "We have send the a default password to your email",
              "details" => "send email ok",
              "request" => $request

          ]);

        break;

        case 'trackDistance':

        $request = new stdClass();
        $request->api_key = $_GET["api_key"];
        $request->driver_lat = $_GET["driver_lat"];
        $request->driver_lng = $_GET["driver_lng"];
        $request->task_lat = $_GET["task_lat"];
        $request->task_lng = $_GET["task_lng"];
        $request->map_action = $_GET["map_action"];
        $request->lang = $_GET["lang"];
        $request->token = $_GET["token"];
        $request->dropoff_lat = $_GET["dropoff_lat"];
        $request->driver_lng = $_GET["driver_lng"];

        if(isset($_GET["dropoff_lat"])){
          $driver_detail = new stdClass();
          $merchant_distance = new stdClass();
          $delivery_distance = new stdClass();
          $delivery_distance->duration = "29 mins";
          $delivery_distance->distance = "23.1 km";

          $merchant_distance->duration = "15 mins";
          $merchant_distance->distance = "5.6 km";

          $driver_detail->merchant_distance = $merchant_distance;
          $driver_detail->delivery_distance = $delivery_distance;
          $driver_detail->map_action = "map1";

        }else{
        $driver_detail = new stdClass();
        $merchant_distance = new stdClass();
        $merchant_distance->duration = "15 mins";
        $merchant_distance->distance = "5.6 km";
        $driver_detail->merchant_distance = $merchant_distance;
        $driver_detail->map_action = "map1";

        }
        jsonResponse([
            'code' => 1,
            "msg" => "OK",
            "details" => $driver_detail,
            "request" => $request

        ]);


        case 'SettingPush':

        $request = new stdClass();
        $request->enabled_push = $_GET["enabled_push"];
        $request->lang = $_GET["lang"];
        $request->api_key = $_GET["api_key"];
        $request->token = $_GET["token"];
        jsonResponse([
            'code' => 1,
            "msg" => "Setting Saved",
            "details" => "",
            "request" => $request

        ]);
        break;

        case 'ChangePassword':

        $request = new stdClass();
        $request->email_address = $_GET["email_address"];
        $request->code = $_GET["code"];
        $request->newpass = $_GET["newpass"];
        $request->lang = $_GET["lang"];
        $request->api_key = $_GET["api_key"];
        //$request->token = $_GET["token"];
        jsonResponse([
            'code' => 1,
            "msg" => "Successfully Changed",
            "details" => "",
            "request" => $request

        ]);
        break;


        /* Shopping Delivery API */

        case 'productlist':
        case 'post':
        $request = new stdClass();
        //$request->api_key = $_GET["api_key"];

        $product = ORM::for_table('sys_items')->where('status', 'Active')->find_many();

        $productslist = array();
        foreach ($product as $key => $value) {
          //print_r($value->title." -- ".$value->c2. "<br/>");
          $productDetail = array();
          $productDetail["id"] = $value->id;
          $productDetail["product_name"] = $value->name;
          //$date = DateTime::createFromFormat("Y-m-d", $value->c2);
          //echo $date->format("Y");
          $productDetail["regular_price"] = $value->sales_price;
          $productDetail["price"] = $value->sales_price;
          $productDetail["sale_price"] = $value->sales_price;
          $productDetail["in_stock"] = true;//$value->inventory;
          $productDetail["managing_stock"] = true;//$value->inventory;
          $productDetail["stock_quantity"] = $value->inventory;
          $productDetail["sku"] = $value->id;
          $productDetail["title"] = $value->name;
          $productDetail["image"] = APP_URL.'/storage/items/'.$value->image;

          array_push($productslist, $productDetail);
        }
        if(count($product)!=0){
        jsonResponse([
            'success' => "1",
            'error'=> "0",
            'message' => "data success",
            "data" => $productslist
            //"request" => $request

        ]);
        }
        else {
        jsonResponse([
          'success' => "0",
          'error'=> "1",
          'message' => "Product not found"

        ]);
        }

        break;


        case 'product_detail':
            case 'post':
            $request = new stdClass();
            //$request->api_key = $_GET["api_key"];
            $product_id = _post('product_id');
            $product = ORM::for_table('sys_items')->where('status', 'Active')->where('id', $product_id)->find_many();

            $productslist = array();
            foreach ($product as $key => $value) {
              //print_r($value->title." -- ".$value->c2. "<br/>");
              $productDetail = array();
              $productDetail["id"] = $value->id;
              $productDetail["product_name"] = $value->name;
              //$date = DateTime::createFromFormat("Y-m-d", $value->c2);
              //echo $date->format("Y");
              $productDetail["regular_price"] = $value->sales_price;
              $productDetail["price"] = $value->sales_price;
              $productDetail["sale_price"] = $value->sales_price;
              $productDetail["in_stock"] = true;//$value->inventory;
              $productDetail["managing_stock"] = true;//$value->inventory;
              $productDetail["stock_quantity"] = $value->inventory;
              $productDetail["sku"] = $value->id;
              $productDetail["title"] = $value->name;
              $productDetail["image"] = APP_URL.'/storage/items/'.$value->image;

              array_push($productslist, $productDetail);
            }
            if(count($product)!=0){
            jsonResponse([
                'success' => "1",
                'error'=> "0",
                'message' => "data success",
                "data" => $productDetail
                //"request" => $request

            ]);
            }
            else {
            jsonResponse([
              'success' => "0",
              'error'=> "1",
              'message' => "Product not found"

            ]);
            }

            break;


        case 'category_all':
        case 'post':
        $request = new stdClass();
        //$request->api_key = $_POST["api_key"];

        $category = ORM::for_table('sys_category')->where('type', 1)->where('status', 1)->find_many();


        $categorylist = array();
        foreach ($category as $key => $value) {
          //print_r($value->title." -- ".$value->c2. "<br/>");
          $categoryDetail = array();
          $categoryDetail["id"] = $value->id;
          $categoryDetail["category_name"] = $value->name;
          $categoryDetail["description"] = $value->description;
          if($value->image != ""){
             $categoryDetail["image"] = APP_URL.'/ui/assets/shopping'.$value->image;

          }
          else{
          $categoryDetail["image"] = $value->image;
          }
            // $categoryDetail["description"] = $value->description;


          array_push($categorylist, $categoryDetail);
        }
        if(count($category)!=0){
        jsonResponse([
            'success' => "1",
            'error'=> "0",
            'message' => "data success",
            "data" => $categorylist
            //"request" => $request

        ]);
        }
        else {
        jsonResponse([
          'success' => "0",
          'error'=> "1",
          'message' => "Category not found"
          //  "request" => $request

        ]);
        }

        break;




        case 'registration':

                            case 'POST':

                                $errors = [];
                                $fname = _post('first_name');
                                $lname = _post('last_name');
                                $account = _post('first_name');

                                $type_customer = _post('customer');
                                $type_supplier = _post('supplier');

                                $type = $type_customer.','.$type_supplier;
                                $type = trim($type,',');

                                if($type == ''){
                                    $type = 'Customer';
                                }


                                //  $company = _post('company');

                                $company_id = _post('company_id');

                                $company = '';
                                $cid = 0;

                                $email = _post('email');
                                $username = $email;//_post('username');
                                $phone = _post('contact');
                                $currency = _post('currency');

                                $address = _post('address');
                                $city = _post('city');
                                $state = _post('state');
                                $zip = _post('zip');
                                $country = _post('country');


                                $owner_id = _post('owner_id');

                                if($owner_id == '')
                                {
                                    $owner_id = 0;
                                }

                                if($company_id != ''){

                                    if($company_id != '0'){
                                        $company_db = db_find_one('sys_companies',$company_id);

                                        if($company_db){
                                            $company = $company_db->company_name;
                                            $cid = $company_id;
                                        }
                                    }


                                }


                                elseif (_post('company') != ''){


                                    // create compnay
                                    $company = _post('company');
                                    $c = new Company;

                                    $c->company_name = $company;
                                    $c->email = $email;
                                    $c->phone = $phone;


                                    $c->address1 = $address;
                                    $c->city = $city;
                                    $c->state = $state;
                                    $c->zip = $zip;
                                    $c->country = $country;

                                    $c->save();

                                    $cid = $c->id;


                                }



                                if($currency == ''){
                                    $currency = '0';
                                }

                                if(isset($_POST['tags']) AND ($_POST['tags']) != ''){
                                    $tags = $_POST['tags'];
                                }
                                else{
                                    $tags = '';
                                }


                                if($fname == ''){
                                    $errors[] = "First Name is required";//$_L['Account Name is required'];
                                }
                                if($lname == ''){
                                    $errors[] = "Last Name is required";//$_L['Account Name is required'];
                                }
                                if($account == ''){
                                //    $errors[] = "First Name is required";//$_L['Account Name is required'];
                                }
                                if($email == ''){
                                    $errors[] = "Please enter your email address ";//$_L['Account Name is required'];
                                }
                                if($phone == ''){
                                    $errors[] = "Please enter your contact number ";//$_L['Account Name is required'];
                                }


                                if($email != ''){

                                    $f = ORM::for_table('crm_accounts')->where('email',$email)->find_one();

                                    if($f){
                                        $errors[] =  $_L['Email already exist'];
                                    }
                                }


                                if($phone != ''){

                                    $f = ORM::for_table('crm_accounts')->where('phone',$phone)->find_one();

                                    if($f){
                                        $errors[] =  $_L['Phone number already exist'];
                                    }
                                }


                                $gid = _post('group');

                                if($gid != ''){
                                    $g = db_find_one('crm_groups',$gid);
                                    $gname = $g['gname'];
                                }
                                else{
                                    $gid = 0;
                                    $gname = '';
                                }

                                $password = _post('password');
                                if($password == ''){
                                    $errors[] = "Please enter your password ";//$_L['Account Name is required'];
                                }



                                $u_password = '';


                                if($password != ''){


                                    $u_password = $password;
                                    $password = Password::_crypt($password);


                                }






                                if(empty($errors)){

                                    Tags::save($tags,'Contacts');

                                    $data = array();

                                    $data['created_at'] = date('Y-m-d H:i:s');
                                    $data['updated_at'] = date('Y-m-d H:i:s');

                                    //  $type = _post('type');


                                    $d = ORM::for_table('crm_accounts')->create();

                                    $d->account = $account;
                                    $d->email = $email;
                                    $d->phone = $phone;
                                    $d->address = $address;
                                    $d->city = $city;
                                    $d->zip = $zip;
                                    $d->state = $state;
                                    $d->country = $country;
                                    $d->tags = Arr::arr_to_str($tags);

                                    //others
                                    $d->fname = $fname;
                                    $d->lname = $lname;
                                    $d->company = $company;
                                    $d->jobtitle = '';
                                    $d->cid = $cid;
                                    $d->o = $owner_id;
                                    $d->balance = '0.00';
                                    $d->status = 'Active';
                                    $d->notes = '';
                                    $d->password = $password;
                                    $d->token = '';
                                    $d->ts = '';
                                    $d->img = '';
                                    $d->web = '';
                                    $d->facebook = '';
                                    $d->google = '';
                                    $d->linkedin = '';

                                    // v 4.2

                                    $d->gname = $gname;
                                    $d->gid = $gid;

                                    // build 4550

                                    $d->currency = $currency;

                                    //

                                    $d->created_at = $data['created_at'];

                                    $d->type = $type;

                                    //

                                    $d->business_number = _post('business_number');

                                    $d->fax = _post('fax');


                                    //

                                    //

                                    $drive = time().Ib_Str::random_string(12);

                                    $d->drive = $drive;

                                    //
                                    $d->save();
                                    $cid = $d->id();
                                    _log($_L['New Contact Added'].' '.$account.' [CID: '.$cid.']','Admin',$owner_id);

                                    //now add custom fields
                                    $fs = ORM::for_table('crm_customfields')->where('ctype','crm')->order_by_asc('id')->find_array();
                                    foreach($fs as $f){
                                        $fvalue = _post('cf'.$f['id']);
                                        $fc = ORM::for_table('crm_customfieldsvalues')->create();
                                        $fc->fieldid = $f['id'];
                                        $fc->relid = $cid;
                                        $fc->fvalue = $fvalue;
                                        $fc->save();
                                    }
                                    //

                                    Event::trigger('contacts/add-post/_on_finished');

                                    // send welcome email if needed

                                    $send_client_signup_email = _post('send_client_signup_email');


                                    if(($email != '') && ($send_client_signup_email == 'Yes') && ($u_password != '')){

                                        $email_data = array();
                                        $email_data['account'] = $account;
                                        $email_data['company'] = $company;
                                        $email_data['password'] = $u_password;
                                        $email_data['email'] = $email;

                                        $send_email = Ib_Email::send_client_welcome_email($email_data);



                                    }



                                    // Create Drive if this feature is enabled


                                    if($config['client_drive'] == '1'){

                                        if (!file_exists('storage/drive/customers/'.$drive.'/storage')) {
                                            mkdir('storage/drive/customers/'.$drive.'/storage',0777,true);
                                        }

                                    }




                                    //
                                    $ress_data=array(
        		'user_id'=>$cid,
        		'first_name'=>$fname,
        		'last_name'=>$lname,
        		'email'=>$email,
        		'contact'=>$phone,
        		'image'=> ""//base_url('/userimage/'.$image->image)
        		);

                                    jsonResponse([
                                        'success' => '1',
                                        'error' => '0',
                                        'message' => "Registration successfull",
                                        'data' => $ress_data
                                    ]);



                                }
                                else{
                                    jsonResponse([
                                        'success' => '0',
                                        'error' => '1',
                                        'message' => $errors
                                    ]);
                                }


                                break;



                                    case 'signin':

                                        $validator = new Validator;
                                        $data = $request->all();
                                        $validation = $validator->validate($data, [
                                            'email' => 'required|email',
                                            'password' => 'required',
                                        ]);




                                        if ($validation->fails()) {
                                            jsonResponse([
                                                'success' => "0",
                                                'error' =>"1",
                                                'message' => $validation->errors()->all()
                                            ]);
                                        } else {
                                            $client = Contact::where('email',$data['email'])->first();

                                            if($client)
                                            {
                                                if(Password::_verify($data['password'],$client->password) == true){

                                                    if($client->autologin == '')
                                                    {
                                                        $token = Ib_Str::random_string(20).$client->id.time();
                                                        $client->autologin = $token;
                                                        $client->save();

                                                    }
                                                    else{
                                                        $token = $client->autologin;
                                                    }
                                                    jsonResponse([
                                                        'success' => "1",
                                                        'error' => "0",
                                                        "message" => "Login successfull",
                                                        'data' => [
                                                            "user_id" =>$client->id,
                                                            'name' => $client->account,
                                                            'email' => $client->email,
                                                            'image' => $client->img,
                                                            'contact' => $client->phone
                                                        ]
                                                    ]);
                                                }
                                                else{
                                                    jsonResponse([
                                                      'success' => "0",
                                                      'error' =>"1",
                                                      'message' => 'Invalid Password'

                                                    ]);
                                                }
                                            }

                                            else{
                                                jsonResponse([
                                                  'success' => "0",
                                                  'error' =>"1",
                                                    'message' =>   'Invalid username or password'
                                                  ]);
                                            }

                                        }

                                        break;
                                        case 'forgot_password':
                                          $email  = $_POST['email'];

                                          jsonResponse([
                                            'success' => "1",
                                            'error' =>"0",
                                              'message' =>   'Please check your inbox'
                                            ]);
                                        break;

                            case 'change_password':
                              case 'POST':

                              $old_password = $_POST['old_password'];
                              $new_password = $_POST['new_password'];
                              $user_id = $_POST['user_id'];

                              $client = Contact::where('id',$user_id)->first();
                              if($client)
                              {
                                //$password = Password::_crypt($new_password);
                                $client->password = Password::_crypt(trim($new_password));


                                $client->save();
                                jsonResponse([
                                    'success' => "1",
                                    'error' => "0",
                                    "message" => "Change password successfull!"

                                ]);

                              }
                              else{
                                jsonResponse([
                                    'success' => "0",
                                    'error' => "1",
                                    "message" => "User Not found"

                                ]);
                              }



                            break;

              case 'signout':
              case 'post':
                $user_id = $_POST['user_id'];
                jsonResponse([
                    'success' => "1",
                    'error' => "0",
                    "message" => "Logout successfull"

                ]);
              break;

              case 'search':
              case 'post':
              $pname = $_POST['keyword'];
              $product = ORM::for_table('sys_items')->where('status', 'Active')->where_like('name', "%".$pname."%")->find_many();

              $productslist = array();
              foreach ($product as $key => $value) {
                //print_r($value->title." -- ".$value->c2. "<br/>");
                $productDetail = array();
                $productDetail["id"] = $value->id;
                $productDetail["product_name"] = $value->name;
                //$date = DateTime::createFromFormat("Y-m-d", $value->c2);
                //echo $date->format("Y");
                $productDetail["regular_price"] = $value->sales_price;
                $productDetail["price"] = $value->sales_price;
                $productDetail["sale_price"] = $value->sales_price;
                $productDetail["in_stock"] = $value->inventory;
                $productDetail["managing_stock"] = true;//$value->inventory;
                $productDetail["stock_quantity"] = true;//$value->inventory;
                $productDetail["sku"] = $value->id;
                $productDetail["title"] = $value->name;
                $productDetail["image"] = $value->image;

                array_push($productslist, $productDetail);
              }
              if(count($product)!=0){
              jsonResponse([
                  'success' => "1",
                  'error'=> "0",
                  'message' => "success data",
                  "data" => $productslist
                  //"request" => $request

              ]);
              }
              else {
              jsonResponse([
                'success' => "0",
                'error'=> "1",
                'message' => "product not found"

              ]);
              }
              break;

              case 'get_profile':
              case 'post':

              if (isset($_POST['user_id'])){
                  $user_id = $_POST['user_id'];
                  if($user_id==0){
                      jsonResponse([
                  'success' => "0",
                  'error' => "1",
                  "message" => "user not found"


              ]);
              die();
                  }
              $client = Contact::where('id',$user_id)->first();
              if($client){
              $account_details=array(
        		      'user_id'=>$user_id,
                  'name' => $client->account,
                  'email' => $client->email,
                //  'image' => APP_URL.'/storage/pics/'.$client->img ,
                    'image' => APP_URL.'/storage/pics/'.strstr($client->img, '_') ,
                  'mobile' => $client->phone,
                  'billing_address' => $client->address,
                  'shipping_address' => $client->company,
                  'city' => $client->city,
                  'country'=> $client->country,
                  'postcode' =>$client->zip

                );

                jsonResponse([
                    'success' => "1",
                    'error' => "0",
                    "message" => "success data",
                    "data" => $account_details

                ]);
              }
              else{
                jsonResponse([
                    'success' => "1",
                    'error' => "0",
                    "message" => "data not found"


                ]);
              }
            }
            else{
              jsonResponse([
                  'success' => "0",
                  'error' => "1",
                  "message" => "user not found"


              ]);
            }


              break;

                case 'update_profile':
              case 'post':

              $user_id = $_POST['user_id'];

              if(count($_FILES)){

              $uploader   =   new Uploader();
              $uploader->setDir('storage/pics/');
              $uploader->sameName(false);
              $uploader->setExtensions(array('jpg','jpeg','png','gif'));  //allowed extensions list//
              //  print_r($uploader->uploadFile('image')); die();
            //  print_r($uploader); die();

              if($uploader->uploadFile('image')){
                  $uploaded  =   $uploader->getUploadName();

                  $file = $uploaded;
                  $msg = $_L['Uploaded Successfully'];
                  $success = 'Yes';

                  // create thumb


              }else{//upload failed
                  $file = '';
                  $msg = $uploader->getMessage();
                  $success = 'No';
              }

              $a = array(
                  'success' => $success,
                  'msg' =>$msg,
                  'file' =>$file
              );
            $filename = $user_id.$file;
            }
            else {
            //  print_r($_FILES);
//echo "sdfsd"; die();
              $filename = "avathar.png";
            }

              $client = Contact::where('id',$user_id)->first();
              if($client)
              {
                //$password = Password::_crypt($new_password);
                $client->img = $filename;
                $client->fname = $_POST['name'];
                $client->account = $_POST['name'];
                $client->address = $_POST['billing_address'];
                $client->company = $_POST['shipping_address'];

                $client->city = $_POST['city'];
                $client->zip = $_POST['postcode'];
                  $client->state = "";
                  $client->country  = $_POST['country'];
                  $client->phone = $_POST['mobile'];
                $client->save();
                $account_details=array(
        				'user_id'=>$user_id,
        				'name'=>$client->fname,
        				'email'=>$client->email,
        				'image'=>$client->image,
        				'mobile'=>$client->phone,
        				'billing_address'=>$client->address,
        				'shipping_address'=>$client->address,
        				'city'=>$client->city,
        				'country'=>$client->country,
        				'postcode'=>$client->zip,
        				);
                jsonResponse([
                    'success' => "1",
                    'error' => "0",
                    "message" => "Profile has been successfully updated",
                    "data" => $account_details
                ]);

              }

              break;

        case 'get_subcategory':
        case 'post':
          $category_name = $_POST['category_name'];
          $cat = ORM::for_table('sys_category')->where('name', $category_name)->find_one();
          //print_r($cat->id);
          if($cat){
          $product = ORM::for_table('sys_items')->where('status', 'Active')->where('category_id', $cat->id)->find_many();

          $productslist = array();
          foreach ($product as $key => $value) {
            //print_r($value->title." -- ".$value->c2. "<br/>");
            $productDetail = array();
            $productDetail["id"] = $value->id;
            $productDetail["product_name"] = $value->name;
            //$date = DateTime::createFromFormat("Y-m-d", $value->c2);
            //echo $date->format("Y");
            $productDetail["regular_price"] = $value->sales_price;
            $productDetail["price"] = $value->sales_price;
            $productDetail["sale_price"] = $value->sales_price;
            $productDetail["in_stock"] = true;
            $productDetail["managing_stock"] = true;//$value->inventory;
            $productDetail["stock_quantity"] = $value->inventory;//$value->inventory;
            $productDetail["sku"] = $value->id;
            $productDetail["title"] = $value->name;
            $productDetail["image"] = APP_URL.'/storage/items/'.$value->image;

            array_push($productslist, $productDetail);
          }
          if(count($product)!=0){
          jsonResponse([
              'success' => "1",
              'error'=> "0",
              'message' => "success data",
              "data" => $productslist
              //"request" => $request

          ]);
          }
          else {
          jsonResponse([
            'success' => "0",
            'error'=> "1",
            'message' => "Product not found"

          ]);
          }
        }else{
          jsonResponse([
            'success' => "0",
            'error'=> "1",
            'message' => "Catrgory not found"

          ]);
        }
        break;

        case 'check_quantity':
        case 'POST':
        $rawPostData  = file_get_contents('php://input');

                $jsonData   = json_decode($rawPostData);
                foreach($jsonData->product as $value)
                {
                  $product_details[]=array(
				          'product_id'=>$value->product_id,
          				'message'=>'available',
          				'available_stock_quantity'=>100,
          				'product_name'=>$value->product_id
          				);
                }

                jsonResponse([
                  'success' => "1",
                  'error'=> "0",
                  'message' => "success data",
                  'data'=>$product_details

                ]);


        break;

        case 'create_order':
        case 'POST':

        $rawPostData  = file_get_contents('php://input');

                $jsonData   = json_decode($rawPostData);

        		if(isset($jsonData->user_id))
        		{



        		//$res=$this->Check_quantity($product);
        		$cid=$jsonData->user_id;
        		$tips=$jsonData->tips;
        		$station=$jsonData->station;


        //    $items = $_POST['items'];

            $i = 0;

            $description = [];
            $item_number = [];
            $qty = [];
            $amount = [];
            $tax_rate = [];

            foreach($jsonData->product as $value)
            {
              $product_id = $value->product_id;
              $product_detail = ORM::for_table('sys_items')->where('id', $product_id)->find_one();

                $description[$i] = $product_detail->name ;//$api_item['description'];
                $item_number[$i] = $product_detail->id;//$api_item['item_code'];
                $qty[$i] = $value->quantity;//$api_item['qty'];
                $amount[$i] = $product_detail->sales_price;// $api_item['amount'];
                //$taxed[$i] = $api_item['taxed'];

                $i++;
            }

          //  $cid = $user_id;//_post('cid');
            $admin_id = 0;//_post('admin_id');

          /*  if($admin_id == ''){
                $admin_id = 0;
            }
        */
            // find user with cid

            $u = ORM::for_table('crm_accounts')->find_one($cid);
            $errors = [];
            if ($cid == '') {
                $errors[] = $_L['select_a_contact'];
            }

            $notes = $tips;//_post('notes');

            //$show_quantity_as = _post('show_quantity_as');

            // find currency

          /*  $currency_id = _post('currency');
            $currency_find = Currency::where('iso_code',$currency_id)->first();
            if ($currency_find) {
                $currency = $currency_find->id;
                $currency_symbol = $currency_find->symbol;
                $currency_rate = $currency_find->rate;
            }
            else {
                $currency = 0;
                $currency_symbol = $config['currency_code'];
                $currency_rate = 1.0000;
            }

            if (empty($amount)) {
                $errors[] = $_L['at_least_one_item_required'];
            }

            $idate = _post('idate');
            $its = strtotime($idate);
            $duedate = _post('duedate');
            $dd = '';
            if ($duedate == 'due_on_receipt') {
                $dd = $idate;
            }
            elseif ($duedate == 'days3') {
                $dd = date('Y-m-d', strtotime('+3 days', $its));
            }
            elseif ($duedate == 'days5') {
                $dd = date('Y-m-d', strtotime('+5 days', $its));
            }
            elseif ($duedate == 'days7') {
                $dd = date('Y-m-d', strtotime('+7 days', $its));
            }
            elseif ($duedate == 'days10') {
                $dd = date('Y-m-d', strtotime('+10 days', $its));
            }
            elseif ($duedate == 'days15') {
                $dd = date('Y-m-d', strtotime('+15 days', $its));
            }
            elseif ($duedate == 'days30') {
                $dd = date('Y-m-d', strtotime('+30 days', $its));
            }
            elseif ($duedate == 'days45') {
                $dd = date('Y-m-d', strtotime('+45 days', $its));
            }
            elseif ($duedate == 'days60') {
                $dd = date('Y-m-d', strtotime('+60 days', $its));
            }
            else {
                $errors[] = 'Invalid Date';
            }

            if (!$dd) {
                $errors[] = 'Date Parsing Error';
            }

            $repeat = _post('repeat');
            $nd = $idate;
            if ($repeat == '0') {
                $r = '0';
            }
            elseif ($repeat == 'daily') {
                $r = '+1 day';
                $nd = date('Y-m-d', strtotime('+1 day', $its));
            }
            elseif ($repeat == 'week1') {
                $r = '+1 week';
                $nd = date('Y-m-d', strtotime('+1 week', $its));
            }
            elseif ($repeat == 'weeks2') {
                $r = '+2 weeks';
                $nd = date('Y-m-d', strtotime('+2 weeks', $its));
            }
            elseif ($repeat == 'weeks3') {
                $r = '+3 weeks';
                $nd = date('Y-m-d', strtotime('+3 weeks', $its));
            }
            elseif ($repeat == 'weeks4') {
                $r = '+4 weeks';
                $nd = date('Y-m-d', strtotime('+4 weeks', $its));
            }
            elseif ($repeat == 'month1') {
                $r = '+1 month';
                $nd = date('Y-m-d', strtotime('+1 month', $its));
            }
            elseif ($repeat == 'months2') {
                $r = '+2 months';
                $nd = date('Y-m-d', strtotime('+2 months', $its));
            }
            elseif ($repeat == 'months3') {
                $r = '+3 months';
                $nd = date('Y-m-d', strtotime('+3 months', $its));
            }
            elseif ($repeat == 'months6') {
                $r = '+6 months';
                $nd = date('Y-m-d', strtotime('+6 months', $its));
            }
            elseif ($repeat == 'year1') {
                $r = '+1 year';
                $nd = date('Y-m-d', strtotime('+1 year', $its));
            }
            elseif ($repeat == 'years2') {
                $r = '+2 years';
                $nd = date('Y-m-d', strtotime('+2 years', $its));
            }
            elseif ($repeat == 'years3') {
                $r = '+3 years';
                $nd = date('Y-m-d', strtotime('+3 years', $its));
            }
            else {
                $errors[] = 'Date Parsing Error';
            }*/

            if(empty($errors)){

               // $qty = $_POST['qty'];
              //  $item_number = $_POST['item_code'];

          //                            if (isset($_POST['taxed'])) {
          //                                $taxed = $_POST['taxed'];
          //                            }
          //                            else {
          //                                $taxed = false;
          //                            }

                $sTotal = '0';
                $taxTotal = '0';
                $i = '0';
                $a = array();


                $taxval = '0.00';
                $taxname = '';
                $taxrate = '0.00';


                $taxed_amount = 0.00;
                $lamount = 0.00;


                foreach($amount as $samount) {
                    $samount = Finance::amount_fix($samount);
                    $a[$i] = $samount;

                    $sqty = $qty[$i];
                    $sqty = Finance::amount_fix($sqty);

                    $lTaxRate = 0;//$taxed[$i];
                    $lTaxRate = Finance::amount_fix($lTaxRate);


                    $sTotal+= $samount * ($sqty);
                    $lamount = $samount * ($sqty);

                    $lTaxVal = ($lamount*$lTaxRate)/100;

                    $taxed_amount += $lTaxVal;

                    $i++;
                }




              //  $invoicenum = _post('invoicenum');
              //  $cn = _post('cn');
                $fTotal = $sTotal;
              /*  $discount_amount = _post('discount_amount');
                $discount_type = _post('discount_type');
                $discount_value = '0.00';
                if ($discount_amount == '0' OR $discount_amount == '') {
                    $actual_discount = '0.00';
                }
                else {
                    if ($discount_type == 'f') {
                        $actual_discount = $discount_amount;
                        $discount_value = $discount_amount;
                    }
                    else {
                        $discount_type = 'p';
                        $actual_discount = ($sTotal * $discount_amount) / 100;
                        $discount_value = $discount_amount;
                    }
                }

                $actual_discount = number_format((float)$actual_discount, 2, '.', '');
                $fTotal = $fTotal + $taxed_amount - $actual_discount;



                $status = _post('status');
                if ($status != 'Draft') {
                    $status = 'Unpaid';
                }

                $receipt_number = _post('receipt_number');
        */
                $datetime = date("Y-m-d H:i:s");
                $vtoken = _raid(10);
                $ptoken = _raid(10);
                $d = ORM::for_table('sys_invoices')->create();
                $d->userid = $cid;
                $d->account = $u['account'];
                $d->date = date("Y-m-d");
                $d->duedate = date("Y-m-d");
                $d->datepaid = $datetime;

                $d->subtotal = $sTotal;
                $d->discount_type = "p";//$discount_type;
                $d->discount_value = 0.0;//$discount_value;
                $d->discount = 0.0;//$actual_discount;
                $d->total = $fTotal;
                $d->tax = 0.0;//$taxed_amount;
                $d->taxname = '';
                $d->taxrate = 0.00;
                $d->vtoken = $vtoken;
                $d->ptoken = $ptoken;
                $d->status = "Unpaid";
                $d->notes = $notes;
                $d->r = 0;
                $d->nd = date('Y-m-d');

                $d->aid = $admin_id;

                $d->show_quantity_as = "Qty";


                $d->invoicenum = "INV-";
                $d->cn = "";//str_pad($config['invoice_code_current_number'], $config['number_pad'], '0', STR_PAD_LEFT);
                $d->tax2 = '0.00';
                $d->taxrate2 = '0.00';
                $d->paymentmethod = '';
                $d->shipping_street = $u['address']."\n".$u['city']."\n".$u['state']." ".$u['zip']."\n".$u['country'];

                $d->currency = 1;
                $d->currency_iso_code = "SGD";
                $d->currency_symbol = "$";
                $d->currency_rate = 1.0;


                $d->receipt_number = "";

                //for delivery Settings
                 $d->c1 = 1;
                 $delivery_date = new DateTime('tomorrow');
                 $d->c2 = $delivery_date->format('Y-m-d H:i:s');
                 $d->delivery_status = "Unassigned";

                 $string = str_replace(' ', '-', $d->shipping_street); // Replaces all spaces with hyphens.

                 $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                 $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$string.'&key=AIzaSyDK9Wyy81NpY3wkqekhOF4EBFHt5Tc-Yyw'; // path to your JSON file
$data = file_get_contents($url); // put the contents of the file into a variable
$location_detail = json_decode($data); // decode the JSON feed
if(count($location_detail->results)){
$d->c3 = $location_detail->results[0]->geometry->location->lat;
$d->c4 = $location_detail->results[0]->geometry->location->lng;
}
else{


}

                 $d->save();
                $invoiceid = $d->id();
              //  $description = $_POST['desc'];

                $i = '0';

                foreach($description as $item) {


                    $samount = $a[$i];
                    $samount = Finance::amount_fix($samount);
                    if ($item == '' && $samount == '0.00') {
                        $i++;
                        continue;
                    }

                    $tax_rate = 0;//$taxed[$i];

                    $sqty = $qty[$i];
                    $sqty = Finance::amount_fix($sqty);
                    $ltotal = ($samount) * ($sqty);
                    $d = ORM::for_table('sys_invoiceitems')->create();
                    $d->invoiceid = $invoiceid;
                    $d->userid = $cid;
                    $d->description = $item;
                    $d->qty = $sqty;
                    $d->amount = $samount;
                    $d->total = $ltotal;
                    $d->taxed = '0';





                    $d->tax_rate = 0.00;

                    $d->type = '';
                    $d->relid = '0';
                    $d->itemcode = $item_number[$i];
                    $d->taxamount = '0.00';
                    $d->duedate = date('Y-m-d');
                    $d->paymentmethod = '';
                    $d->notes = '';



                    $d->save();

                    Inventory::decreaseByItemNumber($item_number[$i], $sqty);



                    $item_r = Item::where('name', $item)->first();
                    if ($item_r) {
                        $item_r->sold_count = $item_r->sold_count + $sqty;
                        $item_r->total_amount = $item_r->total_amount + $samount;
                        $item_r->save();
                    }

                    $i++;
                }
                $total_data=array(
                		'amount'=>$sTotal,
                		'order_id'=>$invoiceid,
                		);
                jsonResponse([
                    'success' => "1",
                    'error'=> "0",
                    'message' => 'Successfull created!',
                    'data'=>$total_data

                ]);


            }
            else {
              jsonResponse([
                  'success' => "0",
                  'error'=> "1",
                  'message' => 'error Occured'

              ]);
            }



          }
        break;
        case 'nonce':
        case 'post':

        if(isset($_POST['nonce']) && $_POST['nonce']!=''){
          $transaction_id = $_POST['nonce'];
        			$id=$_POST['order_id'];
              $d = ORM::for_table('sys_invoices')->where('id', $id)->find_one();
              $d->status = "Paid";
              $d->save();
              $ord  = array('id' => $id);
              jsonResponse([
                  'success' => "1",
                  'error'=> "0",
                  'message' => 'Order Created Successfull!',
                  'data' => $ord

              ]);

        }
        else {
          jsonResponse([
              'success' => "0",
              'error'=> "1",
              'message' => 'access denied'

          ]);
        }

        break;
        case 'token':
        case 'post':

        $data=array(
        		// 'token'=>$clientToken,
        		'service_charge'=>"0",
        		'tax'=>"0"
        		);
            jsonResponse([
                'success' => "0",
                'error'=> "1",
                'message' => 'success data',
                'data' => $data

            ]);


        break;


        case 'order_details':
            case 'post':
            /*
            $data = array();
            $product = new stdClass();
            $product->sr = 1;
            $product->amount = "107.00";
            $product->quantity = 1;
            $product->date = "2019-10-01T06:29:31Z";
            $product->image = "http://freebizoffer.com/apptech/eCommerceApptech/mart/wordpress/wp-content/uploads/2018/08/image_place.png";
            $product_data = new stdClass();
            $product_data->user_id="36";
            $item_detail = array();
            $item = new stdClass();
            $item->product_id= 94;
            $item->quantity = 1;

            array_push($item_detail, $item);
            $product_data->product = $item_detail;
            $product->product_data = $product_data;
            array_push($data, $product);
              jsonResponse([
                'success' => "1",
                'error'=> "0",
                'message' => 'data success',
                'data' => $data
                ]);
            */

              if (isset($_POST['user_id'])){
                  $user_id = $_POST['user_id'];
                 $invoice = ORM::for_table('sys_invoices')->where('userid', $user_id)->find_many();

                 if(count($invoice)){
                    $data = array();
                    $i = 1;
                    foreach ($invoice as $key => $value) {
                        $invoice_item = ORM::for_table('sys_invoiceitems')->where('invoiceid', $value->id)->find_many();
                        $qty = 0;
                        foreach($invoice_item as $k => $v){
                            $pps = array('product_id'=>$v->itemcode,'quantity'=>$v->qty);
        			    	$pro[]= $pps;
        			    	$qty += $v->qty;
                        }
                       $resproduct=array(
        				'user_id'=>$user_id,
        				'product'=>$pro
        				);
        				$ress['sr'] = $i++;

        				$ress['amount']   = $value->total;
        				$ress['quantity'] = $qty;
        				$ress['date']     = "2019-10-01T06:29:31Z";//$value->date;
        				$ress['image']    = "";
        				$ress['product_data']=$resproduct;

        				$data[]=$ress;
        				$pro=array();
                    }

                 jsonResponse([
                'success' => "1",
                'error'=> "0",
                'message' => 'data success',
                'data' => $data
                ]);

                 }
                 else{

                jsonResponse([
                'success' => "0",
                'error'=> "1",
                'message' => 'data not found'
                ]);
                 }





              }
              else{
                  jsonResponse([
                'success' => "0",
                'error'=> "1",
                'message' => 'user not found'

            ]);
              }

            break;

        /*shopping devlivery api end */


/*POS API */


case 'product_category': //product_category
//case 'post':
$request = new stdClass();
//$request->api_key = $_POST["api_key"];

$category = ORM::for_table('sys_category')->where('type', 1)->where('status', 1)->find_many();


$categorylist = array();
foreach ($category as $key => $value) {
  //print_r($value->title." -- ".$value->c2. "<br/>");
  $categoryDetail = array();
  $categoryDetail["id"] = $value->id;
  $categoryDetail["name"] = $value->name;
  $categoryDetail["description"] = $value->description;
  if($value->image != ""){
     $categoryDetail["image"] = APP_URL.'/ui/assets/shopping'.$value->image;

  }
  else{
  $categoryDetail["image"] = APP_URL.'/ui/assets/shopping/default.jpg';//$value->image;
  }
    // $categoryDetail["description"] = $value->description;


  array_push($categorylist, $categoryDetail);
}
if(count($category)!=0){
jsonResponse([
    "status" => "true",
    "message"=> "success",
    "response" => $categorylist
    //"request" => $request

]);
}
else {
    $mess = new StdClass();
    $mess->value =  "No product categories found";
jsonResponse([
   "status" => "true",
    "message"=> "error",
  'response' => $mess
  //  "request" => $request

]);
}

break;


case 'service_category': //service_category_list
//case 'post':
$request = new stdClass();
//$request->api_key = $_POST["api_key"];

$category = ORM::for_table('sys_category')->where('type', 2)->where('status', 1)->find_many();


$categorylist = array();
foreach ($category as $key => $value) {
  //print_r($value->title." -- ".$value->c2. "<br/>");
  $categoryDetail = array();
  $categoryDetail["id"] = $value->id;
  $categoryDetail["name"] = $value->name;
  $categoryDetail["description"] = $value->description;
  if($value->image != ""){
     $categoryDetail["image"] = APP_URL.'/ui/assets/shopping'.$value->image;

  }
  else{
  $categoryDetail["image"] = APP_URL.'/ui/assets/shopping/default.jpg';//$value->image;
  }
    // $categoryDetail["description"] = $value->description;


  array_push($categorylist, $categoryDetail);
}
if(count($category)!=0){
jsonResponse([
    "status" => "true",
    "message"=> "success",
    "response" => $categorylist
    //"request" => $request

]);
}
else {
    $mess = new StdClass();
    $mess->value =  "No service categories found";
jsonResponse([
   "status" => "true",
    "message"=> "error",
  'response' => $mess
  //  "request" => $request

]);
}

break;


case 'product_by_id': //service_category_list
//case 'post':
$request = new stdClass();
//$request->api_key = $_POST["api_key"];

if(!isset($_GET['id']) || $_GET['id']=="") {
 $product = ORM::for_table('sys_items')->where('status', 'Active')->find_many();

}
else if(isset($_GET['id'])){
$id = $_GET['id'];

$product = ORM::for_table('sys_items')->where('status', 'Active')->where('category_id', $id)->find_many();
}

$productslist = array();
foreach ($product as $key => $value) {

  $productDetail = array();
  $productDetail["id"] = $value->id;
  $productDetail["name"] = $value->name;
  $productDetail["cost_price"] = $value->cost_price;
  $productDetail["sales_price"] = $value->sales_price;
  $productDetail["unit"] = $value->unit;
  $productDetail["inventory"] = $value->inventory;
  $productDetail["weight"] = $value->weight;
  $productDetail["type"] = $value->type;
  $productDetail["created_at"] = $value->created_at;
  $productDetail["updated_at"] = $value->updated_at;
  $productDetail["item"] = $value->item_number;
  $productDetail["description"] = strip_tags($value->description);

   if($value->image != ""){
  $productDetail["image"] = APP_URL.'/storage/items/'.$value->image;

  }
  else{
  $productDetail["image"] = APP_URL.'/ui/assets/shopping/default.jpg';//$value->image;
  }

  $productDetail["category_id"] = $value->category_id;






  array_push($productslist, $productDetail);
}


if(count($productslist)!=0){
jsonResponse([
    "status" => "true",
    "message"=> "success",
    "response" => $productslist
    //"request" => $request

]);
}
else {
    $mess = new StdClass();
    $mess->value =  "No service categories found";
jsonResponse([
   "status" => "true",
    "message"=> "error",
  'response' => $mess
  //  "request" => $request

]);
}

break;



case 'queue':
$mess = new StdClass();
   // $mess->queueno =  "FA-104";//null;
     $mess->queueno =  null;
    $mess->type = "product_service"; //product, product_service

jsonResponse([
    "status" => "true",
    "message"=> "success",
    "response" => $mess
    //"request" => $request

]);
break;
case 'order_insert':
    case 'POST':

$rawPostData  = file_get_contents('php://input');

        $jsonData   = json_decode($rawPostData);

		if(isset($jsonData->user_id))
		{



		//$res=$this->Check_quantity($product);
		$cid=$jsonData->user_id;
		$tips=$jsonData->tips;
		$station=$jsonData->station;


//    $items = $_POST['items'];

    $i = 0;

    $description = [];
    $item_number = [];
    $qty = [];
    $amount = [];
    $tax_rate = [];

    foreach($jsonData->product as $value)
    {
      $product_id = $value->product_id;
      $product_detail = ORM::for_table('sys_items')->where('id', $product_id)->find_one();

        $description[$i] = $product_detail->name ;//$api_item['description'];
        $item_number[$i] = $product_detail->id;//$api_item['item_code'];
        $qty[$i] = $value->quantity;//$api_item['qty'];
        $amount[$i] = $product_detail->sales_price;// $api_item['amount'];
        //$taxed[$i] = $api_item['taxed'];

        $i++;
    }

  //  $cid = $user_id;//_post('cid');
    $admin_id = 0;//_post('admin_id');

  /*  if($admin_id == ''){
        $admin_id = 0;
    }
*/
    // find user with cid

    $u = ORM::for_table('crm_accounts')->find_one($cid);
    $errors = [];
    if ($cid == '') {
        $errors[] = $_L['select_a_contact'];
    }

    $notes = $tips;//_post('notes');

    //$show_quantity_as = _post('show_quantity_as');

    // find currency

  /*  $currency_id = _post('currency');
    $currency_find = Currency::where('iso_code',$currency_id)->first();
    if ($currency_find) {
        $currency = $currency_find->id;
        $currency_symbol = $currency_find->symbol;
        $currency_rate = $currency_find->rate;
    }
    else {
        $currency = 0;
        $currency_symbol = $config['currency_code'];
        $currency_rate = 1.0000;
    }

    if (empty($amount)) {
        $errors[] = $_L['at_least_one_item_required'];
    }

    $idate = _post('idate');
    $its = strtotime($idate);
    $duedate = _post('duedate');
    $dd = '';
    if ($duedate == 'due_on_receipt') {
        $dd = $idate;
    }
    elseif ($duedate == 'days3') {
        $dd = date('Y-m-d', strtotime('+3 days', $its));
    }
    elseif ($duedate == 'days5') {
        $dd = date('Y-m-d', strtotime('+5 days', $its));
    }
    elseif ($duedate == 'days7') {
        $dd = date('Y-m-d', strtotime('+7 days', $its));
    }
    elseif ($duedate == 'days10') {
        $dd = date('Y-m-d', strtotime('+10 days', $its));
    }
    elseif ($duedate == 'days15') {
        $dd = date('Y-m-d', strtotime('+15 days', $its));
    }
    elseif ($duedate == 'days30') {
        $dd = date('Y-m-d', strtotime('+30 days', $its));
    }
    elseif ($duedate == 'days45') {
        $dd = date('Y-m-d', strtotime('+45 days', $its));
    }
    elseif ($duedate == 'days60') {
        $dd = date('Y-m-d', strtotime('+60 days', $its));
    }
    else {
        $errors[] = 'Invalid Date';
    }

    if (!$dd) {
        $errors[] = 'Date Parsing Error';
    }

    $repeat = _post('repeat');
    $nd = $idate;
    if ($repeat == '0') {
        $r = '0';
    }
    elseif ($repeat == 'daily') {
        $r = '+1 day';
        $nd = date('Y-m-d', strtotime('+1 day', $its));
    }
    elseif ($repeat == 'week1') {
        $r = '+1 week';
        $nd = date('Y-m-d', strtotime('+1 week', $its));
    }
    elseif ($repeat == 'weeks2') {
        $r = '+2 weeks';
        $nd = date('Y-m-d', strtotime('+2 weeks', $its));
    }
    elseif ($repeat == 'weeks3') {
        $r = '+3 weeks';
        $nd = date('Y-m-d', strtotime('+3 weeks', $its));
    }
    elseif ($repeat == 'weeks4') {
        $r = '+4 weeks';
        $nd = date('Y-m-d', strtotime('+4 weeks', $its));
    }
    elseif ($repeat == 'month1') {
        $r = '+1 month';
        $nd = date('Y-m-d', strtotime('+1 month', $its));
    }
    elseif ($repeat == 'months2') {
        $r = '+2 months';
        $nd = date('Y-m-d', strtotime('+2 months', $its));
    }
    elseif ($repeat == 'months3') {
        $r = '+3 months';
        $nd = date('Y-m-d', strtotime('+3 months', $its));
    }
    elseif ($repeat == 'months6') {
        $r = '+6 months';
        $nd = date('Y-m-d', strtotime('+6 months', $its));
    }
    elseif ($repeat == 'year1') {
        $r = '+1 year';
        $nd = date('Y-m-d', strtotime('+1 year', $its));
    }
    elseif ($repeat == 'years2') {
        $r = '+2 years';
        $nd = date('Y-m-d', strtotime('+2 years', $its));
    }
    elseif ($repeat == 'years3') {
        $r = '+3 years';
        $nd = date('Y-m-d', strtotime('+3 years', $its));
    }
    else {
        $errors[] = 'Date Parsing Error';
    }*/

    if(empty($errors)){

       // $qty = $_POST['qty'];
      //  $item_number = $_POST['item_code'];

  //                            if (isset($_POST['taxed'])) {
  //                                $taxed = $_POST['taxed'];
  //                            }
  //                            else {
  //                                $taxed = false;
  //                            }

        $sTotal = '0';
        $taxTotal = '0';
        $i = '0';
        $a = array();


        $taxval = '0.00';
        $taxname = '';
        $taxrate = '0.00';


        $taxed_amount = 0.00;
        $lamount = 0.00;


        foreach($amount as $samount) {
            $samount = Finance::amount_fix($samount);
            $a[$i] = $samount;

            $sqty = $qty[$i];
            $sqty = Finance::amount_fix($sqty);

            $lTaxRate = 0;//$taxed[$i];
            $lTaxRate = Finance::amount_fix($lTaxRate);


            $sTotal+= $samount * ($sqty);
            $lamount = $samount * ($sqty);

            $lTaxVal = ($lamount*$lTaxRate)/100;

            $taxed_amount += $lTaxVal;

            $i++;
        }




      //  $invoicenum = _post('invoicenum');
      //  $cn = _post('cn');
        $fTotal = $sTotal;
      /*  $discount_amount = _post('discount_amount');
        $discount_type = _post('discount_type');
        $discount_value = '0.00';
        if ($discount_amount == '0' OR $discount_amount == '') {
            $actual_discount = '0.00';
        }
        else {
            if ($discount_type == 'f') {
                $actual_discount = $discount_amount;
                $discount_value = $discount_amount;
            }
            else {
                $discount_type = 'p';
                $actual_discount = ($sTotal * $discount_amount) / 100;
                $discount_value = $discount_amount;
            }
        }

        $actual_discount = number_format((float)$actual_discount, 2, '.', '');
        $fTotal = $fTotal + $taxed_amount - $actual_discount;



        $status = _post('status');
        if ($status != 'Draft') {
            $status = 'Unpaid';
        }

        $receipt_number = _post('receipt_number');
*/
        $datetime = date("Y-m-d H:i:s");
        $vtoken = _raid(10);
        $ptoken = _raid(10);
        $d = ORM::for_table('sys_invoices')->create();
        $d->userid = $cid;
        $d->account = $u['account'];
        $d->date = date("Y-m-d");
        $d->duedate = date("Y-m-d");
        $d->datepaid = $datetime;

        $d->subtotal = $sTotal;
        $d->discount_type = "p";//$discount_type;
        $d->discount_value = 0.0;//$discount_value;
        $d->discount = 0.0;//$actual_discount;
        $d->total = $fTotal;
        $d->tax = 0.0;//$taxed_amount;
        $d->taxname = '';
        $d->taxrate = 0.00;
        $d->vtoken = $vtoken;
        $d->ptoken = $ptoken;
        $d->status = "Paid";
        $d->notes = $notes;
        $d->r = 0;
        $d->nd = date('Y-m-d');

        $d->aid = $admin_id;

        $d->show_quantity_as = "Qty";


        $d->invoicenum = "INV-";
        $d->cn = "";//str_pad($config['invoice_code_current_number'], $config['number_pad'], '0', STR_PAD_LEFT);
        $d->tax2 = '0.00';
        $d->taxrate2 = '0.00';
        $d->paymentmethod = '';
        $d->shipping_street = $u['address']."\n".$u['city']."\n".$u['state']." ".$u['zip']."\n".$u['country'];

        $d->currency = 1;
        $d->currency_iso_code = "SGD";
        $d->currency_symbol = "$";
        $d->currency_rate = 1.0;


        $d->receipt_number = "";

        //for delivery Settings
         $d->c1 = 1;
         $delivery_date = new DateTime('tomorrow');
         $d->c2 = $delivery_date->format('Y-m-d H:i:s');
         $d->delivery_status = "Unassigned";
         $d->save();
        $invoiceid = $d->id();
      //  $description = $_POST['desc'];

        $i = '0';

        foreach($description as $item) {


            $samount = $a[$i];
            $samount = Finance::amount_fix($samount);
            if ($item == '' && $samount == '0.00') {
                $i++;
                continue;
            }

            $tax_rate = 0;//$taxed[$i];

            $sqty = $qty[$i];
            $sqty = Finance::amount_fix($sqty);
            $ltotal = ($samount) * ($sqty);
            $d = ORM::for_table('sys_invoiceitems')->create();
            $d->invoiceid = $invoiceid;
            $d->userid = $cid;
            $d->description = $item;
            $d->qty = $sqty;
            $d->amount = $samount;
            $d->total = $ltotal;
            $d->taxed = '0';





            $d->tax_rate = 0.00;

            $d->type = '';
            $d->relid = '0';
            $d->itemcode = $item_number[$i];
            $d->taxamount = '0.00';
            $d->duedate = date('Y-m-d');
            $d->paymentmethod = '';
            $d->notes = '';



            $d->save();

            Inventory::decreaseByItemID($item_number[$i], $sqty);



            $item_r = Item::where('name', $item)->first();
            if ($item_r) {
                $item_r->sold_count = $item_r->sold_count + $sqty;
                $item_r->total_amount = $item_r->total_amount + $samount;
                $item_r->save();
            }

            $i++;
        }
        $total_data=array(
        		'amount'=>$sTotal,
        		'order_id'=>$invoiceid,
        		);
        jsonResponse([
            'success' => "1",
            'error'=> "0",
            'message' => 'Invoice created successfully',
            'data'=>$total_data

        ]);


    }
    else {
      jsonResponse([
          'success' => "0",
          'error'=> "1",
          'message' => 'error Occured'

      ]);
    }



  }
        else {


    $i = 0;

    $description = [];
    $item_number = [];
    $qty = [];
    $amount = [];
    $tax_rate = [];

    foreach($jsonData->cart as $value)
    {
      $product_id = $value->product_id;
      $product_detail = ORM::for_table('sys_items')->where('id', $product_id)->find_one();

        $description[$i] = $product_detail->name ;//$api_item['description'];
        $item_number[$i] = $product_detail->id;//$api_item['item_code'];
        $qty[$i] = $value->quantity;//$api_item['qty'];
        $amount[$i] = $product_detail->sales_price;// $api_item['amount'];
        //$taxed[$i] = $api_item['taxed'];

        $i++;
    }


    $admin_id = 0;//_post('admin_id');

    if(empty($errors)){


        $sTotal = '0';
        $taxTotal = '0';
        $i = '0';
        $a = array();


        $taxval = '0.00';
        $taxname = '';
        $taxrate = '0.00';


        $taxed_amount = 0.00;
        $lamount = 0.00;


        foreach($amount as $samount) {
            $samount = Finance::amount_fix($samount);
            $a[$i] = $samount;

            $sqty = $qty[$i];
            $sqty = Finance::amount_fix($sqty);

            $lTaxRate = 0;//$taxed[$i];
            $lTaxRate = Finance::amount_fix($lTaxRate);


            $sTotal+= $samount * ($sqty);
            $lamount = $samount * ($sqty);

            $lTaxVal = ($lamount*$lTaxRate)/100;

            $taxed_amount += $lTaxVal;

            $i++;
        }





        $fTotal = $sTotal;

        $datetime = date("Y-m-d H:i:s");
        $vtoken = _raid(10);
        $ptoken = _raid(10);
        $d = ORM::for_table('sys_invoices')->create();
        $d->userid = 0;
        $d->account = $jsonData->name;
        $d->date = date("Y-m-d");
        $d->duedate = date("Y-m-d");
        $d->datepaid = $datetime;

        $d->subtotal = $sTotal;
        $d->discount_type = "p";//$discount_type;
        $d->discount_value = 0.0;//$discount_value;
        $d->discount = 0.0;//$actual_discount;
        $d->total = $fTotal;
        $d->tax = 0.0;//$taxed_amount;
        $d->taxname = '';
        $d->taxrate = 0.00;
        $d->vtoken = $vtoken;
        $d->ptoken = $ptoken;
        $d->status = "Paid";
        $d->notes = "";
        $d->r = 0;
        $d->nd = date('Y-m-d');

        $d->aid = $admin_id;

        $d->show_quantity_as = "Qty";


        $d->invoicenum = "INV-";
        $d->cn = "";//str_pad($config['invoice_code_current_number'], $config['number_pad'], '0', STR_PAD_LEFT);
        $d->tax2 = '0.00';
        $d->taxrate2 = '0.00';
        $d->paymentmethod = '';
        $d->shipping_street = $jsonData->address;

        $d->currency = 1;
        $d->currency_iso_code = "SGD";
        $d->currency_symbol = "$";
        $d->currency_rate = 1.0;


        $d->receipt_number = "";

        //for delivery Settings
         $d->c1 = 1;
         $delivery_date = new DateTime('tomorrow');
         $d->c2 = $delivery_date->format('Y-m-d H:i:s');
         $d->delivery_status = "Unassigned";
         $d->phone = $jsonData->mobile_num;
         $d->email = $jsonData->email;
         $d->save();
        $invoiceid = $d->id();
      //  $description = $_POST['desc'];

        $i = '0';

        foreach($description as $item) {


            $samount = $a[$i];
            $samount = Finance::amount_fix($samount);
            if ($item == '' && $samount == '0.00') {
                $i++;
                continue;
            }

            $tax_rate = 0;//$taxed[$i];

            $sqty = $qty[$i];
            $sqty = Finance::amount_fix($sqty);
            $ltotal = ($samount) * ($sqty);
            $d = ORM::for_table('sys_invoiceitems')->create();
            $d->invoiceid = $invoiceid;
            $d->userid = 0;
            $d->description = $item;
            $d->qty = $sqty;
            $d->amount = $samount;
            $d->total = $ltotal;
            $d->taxed = '0';





            $d->tax_rate = 0.00;

            $d->type = '';
            $d->relid = '0';
            $d->itemcode = $item_number[$i];
            $d->taxamount = '0.00';
            $d->duedate = date('Y-m-d');
            $d->paymentmethod = '';
            $d->notes = '';



            $d->save();


            Inventory::decreaseByItemID($item_number[$i], $sqty);



            $item_r = Item::where('name', $item)->first();
            if ($item_r) {
                $item_r->sold_count = $item_r->sold_count + $sqty;
                $item_r->total_amount = $item_r->total_amount + $samount;
                $item_r->save();
            }

            $i++;
        }
        $total_data=array(
        		'amount'=>$sTotal,
        		'order_id'=>$invoiceid,
        		);

        jsonResponse([
            'status' => "true",
            'message' => "sucessfully saved !!",
            'queNumber'=>""

        ]);


    }
    else {
      jsonResponse([
          'success' => "0",
          'error'=> "1",
          'message' => 'error Occured'

      ]);
    }

        }



break;

case 'banner_category':

$request = new stdClass();
//$request->api_key = $_POST["api_key"];

$category = ORM::for_table('sys_category')->where('featured', 1)->where('status', 1)->find_many();


$categorylist = array();
foreach ($category as $key => $value) {
  //print_r($value->title." -- ".$value->c2. "<br/>");
  $categoryDetail = array();
  $categoryDetail["id"] = $value->id;
  $categoryDetail["category_name"] = $value->name;
  $categoryDetail["description"] = $value->description;
  if($value->image != ""){
     $categoryDetail["image"] = APP_URL.'/ui/assets/shopping'.$value->image;

  }
  else{
  $categoryDetail["image"] = $value->image;
  }
    // $categoryDetail["description"] = $value->description;


  array_push($categorylist, $categoryDetail);
}
if(count($category)!=0){
jsonResponse([
    "status" => "true",
    "message"=> "success",
    "response" => $categorylist
    //"request" => $request

]);
}
else {
    $mess = new StdClass();
    $mess->value =  "No product categories found";
jsonResponse([
   "status" => "true",
    "message"=> "error",
  'response' => $mess
  //  "request" => $request

]);
}

break;


case 'client-signup':

case 'POST':

    $errors = [];
    $email = _post('email');
    $name = _post('name');
    $account = $name;

    $type_customer = _post('customer');
    $type_supplier = _post('supplier');

    $type = $type_customer.','.$type_supplier;
    $type = trim($type,',');

    if($type == ''){
        $type = 'Customer';
    }


    //  $company = _post('company');

    $company_id = _post('company_id');

    $company = '';
    $cid = 0;


    $username = $email;
    $phone = _post('phone');
    $currency = _post('currency');

    $address = _post('address');
    $city = _post('city');
    $state = _post('state');
    $zip = _post('zip');
    $country = _post('country');


    $owner_id = _post('owner_id');

    if($owner_id == '')
    {
        $owner_id = 0;
    }

    if($company_id != ''){

        if($company_id != '0'){
            $company_db = db_find_one('sys_companies',$company_id);

            if($company_db){
                $company = $company_db->company_name;
                $cid = $company_id;
            }
        }


    }


    elseif (_post('company') != ''){


        // create compnay
        $company = _post('company');
        $c = new Company;

        $c->company_name = $company;
        $c->email = $email;
        $c->phone = $phone;


        $c->address1 = $address;
        $c->city = $city;
        $c->state = $state;
        $c->zip = $zip;
        $c->country = $country;

        $c->save();

        $cid = $c->id;


    }



    if($currency == ''){
        $currency = '0';
    }

    if(isset($_POST['tags']) AND ($_POST['tags']) != ''){
        $tags = $_POST['tags'];
    }
    else{
        $tags = '';
    }



    if($account == ''){
        $errors[] = $_L['Account Name is required'];
    }


    if($email != ''){

        $f = ORM::for_table('crm_accounts')->where('email',$email)->find_one();

        if($f){
            $errors[] =  $_L['Email already exist'];
        }
    }


    if($phone != ''){

        $f = ORM::for_table('crm_accounts')->where('phone',$phone)->find_one();

        if($f){
            $errors[] =  $_L['Phone number already exist'];
        }
    }


    $gid = _post('group');

    if($gid != ''){
        $g = db_find_one('crm_groups',$gid);
        $gname = $g['gname'];
    }
    else{
        $gid = 0;
        $gname = '';
    }

    $password = _post('password');

    $u_password = '';


    if($password != ''){


        $u_password = $password;
        $password = Password::_crypt($password);


    }






    if(empty($errors)){

        Tags::save($tags,'Contacts');

        $data = array();

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        //  $type = _post('type');


        $d = ORM::for_table('crm_accounts')->create();

        $d->account = $account;
        $d->email = $email;
        $d->phone = $phone;
        $d->address = $address;
        $d->city = $city;
        $d->zip = $zip;
        $d->state = $state;
        $d->country = $country;
        $d->tags = Arr::arr_to_str($tags);

        //others
        $d->fname = '';
        $d->lname = '';
        $d->company = $company;
        $d->jobtitle = '';
        $d->cid = $cid;
        $d->o = $owner_id;
        $d->balance = '0.00';
        $d->status = 'Active';
        $d->notes = '';
        $d->password = $password;
        $d->token = '';
        $d->ts = '';
        $d->img = '';
        $d->web = '';
        $d->facebook = '';
        $d->google = '';
        $d->linkedin = '';

        // v 4.2

        $d->gname = $gname;
        $d->gid = $gid;

        // build 4550

        $d->currency = $currency;

        //

        $d->created_at = $data['created_at'];

        $d->type = $type;

        //

        $d->business_number = _post('business_number');

        $d->fax = _post('fax');


        //

        //

        $drive = time().Ib_Str::random_string(12);

        $d->drive = $drive;

        //
        $d->save();
        $cid = $d->id();
        _log($_L['New Contact Added'].' '.$account.' [CID: '.$cid.']','Admin',$owner_id);

        //now add custom fields
        $fs = ORM::for_table('crm_customfields')->where('ctype','crm')->order_by_asc('id')->find_array();
        foreach($fs as $f){
            $fvalue = _post('cf'.$f['id']);
            $fc = ORM::for_table('crm_customfieldsvalues')->create();
            $fc->fieldid = $f['id'];
            $fc->relid = $cid;
            $fc->fvalue = $fvalue;
            $fc->save();
        }
        //

        Event::trigger('contacts/add-post/_on_finished');

        // send welcome email if needed

        $send_client_signup_email = _post('send_client_signup_email');


        if(($email != '') && ($send_client_signup_email == 'Yes') && ($u_password != '')){

            $email_data = array();
            $email_data['account'] = $account;
            $email_data['company'] = $company;
            $email_data['password'] = $u_password;
            $email_data['email'] = $email;

            $send_email = Ib_Email::send_client_welcome_email($email_data);



        }



        // Create Drive if this feature is enabled


        if($config['client_drive'] == '1'){

            if (!file_exists('storage/drive/customers/'.$drive.'/storage')) {
                mkdir('storage/drive/customers/'.$drive.'/storage',0777,true);
            }

        }




        //

        jsonResponse([
            'status' => "success",
            'userId' => (int)$cid,
            'error' => false,
            'message' => [$_L['account_created_successfully']]
        ]);



    }
    else{
        jsonResponse([
            'status'=>"failed",
            'error' => true,
            'message' => $errors
        ]);
    }


    break;


    case 'client-signin':
    case 'POST':

        $validator = new Validator;
        $data = $request->all();
        $validation = $validator->validate($data, [
            'email' => 'required|email',
            'password' => 'required',
        ]);




        if ($validation->fails()) {
            jsonResponse([
                'status'=>"failed",
                'error' => true,
                'message' => $validation->errors()->all(),
                'user' => null
            ]);
        } else {
            $client = Contact::where('email',$data['email'])->first();
          //  print_r($client->id); die();
            if($client)
            {
                if(Password::_verify($data['password'],$client->password) == true){

                    if($client->autologin == '')
                    {
                        $token = Ib_Str::random_string(20).$client->id.time();
                        $client->autologin = $token;
                        $client->save();

                    }
                    else{
                        $token = $client->autologin;
                    }
                    jsonResponse([
                        'status'=>"success",
                        'error'=>false,
                        'message'=> ["Login Success"],
                        'user' => [
                            'name' => $client->account,
                            'email' => $client->email,
                            'image' => $client->img,
                            'token' => $token,
                            'id'=>$client->id
                        ]
                    ]);
                }
                else{
                    jsonResponse([
                        'status'=>"failed",
                        'error'=>true,
                        'message' => [
                            'Invalid Password'
                        ],
                        'user' => null
                    ]);
                }
            }

            else{
                jsonResponse([
                  'status'=>"failed",
                  'error'=>true,
                  'message' => [
                        'Invalid username or password'
                    ],
                    'user' => null
                ]);
            }

        }

        break;


        case 'getPackages':
        case 'post':
        $request = new stdClass();
        //$request->api_key = $_GET["api_key"];

        $product = ORM::for_table('sys_package')->where('status', 'Active')->find_many();

        $productslist = array();
        foreach ($product as $key => $value) {
          //print_r($value->title." -- ".$value->c2. "<br/>");
          //print_r($product); die();
          $productDetail = array();
          $productDetail["id"] = $value->id;
          $productDetail["name"] = $value->name;
          $productDetail["description"] = $value->description;
           $productDetail["amountInCents"] = $value->amountincent;

          if($value->image!=""){
          $productDetail["image"] = APP_URL.'/storage/items/'.$value->image;
        }
        else{
          $productDetail["image"] = "";//APP_URL.'/storage/items/'.$value->image;

        }
          array_push($productslist, $productDetail);
        }
        if(count($product)!=0){
        jsonResponse([
            'status' => "success",
            'error'=> false,
            'message' => [""],
            "packages" => $productslist
            //"request" => $request

        ]);
        }
        else {
        jsonResponse([
          'status' => "success",
          'error'=> true,
          'message' => ["No Package Available"],
          "package" => null

        ]);
        }

        break;

        case 'getUserProfile':

        $id = $_POST['userId'];
        $contact = Contact::find($id);

        if($contact)
        {
          $profile['userId'] = $contact->id;
          $profile['name'] = $contact->account;
          $profile['email'] = $contact->email;
          $profile['phone'] = $contact->phone;
          $profile['address'] = $contact->address;
          $profile['country'] = $contact->country;
          $profile['zip'] = $contact->zip;
          $profile['totalExpenditureInCents'] = 20000;
          $profile['accountBalanceInCents'] = $contact->balance * 100;
          $profile['amountRewardedInCents'] = 5000;
          jsonResponse([
            'status' => "success",
            'error'=> false,
            'message' => [""],
            "profile" => $profile

          ]);
        }
        else{
          jsonResponse([
            'status' => "failed",
            'error'=> true,
            'message' => ["Customer Not Found"],
            "profile" => null

          ]);
        }


        break;
      case 'getUserTransactions':

        $id = $_POST['userId'];
      $transactions = Transaction::where('payerid',$id)
        ->orWhere('payeeid',$id)
        ->limit(1000)
        ->get();

      $transactions_data = [];

      foreach ($transactions as $transaction)
      {
        $transactions_data[] = [
          'dateInMillis' => strtotime($transaction->date)*1000,
          'id' => $transaction->id,
          'date_formatted' => date( $config['df'], strtotime($transaction->date)),
          'account' => $transaction->account,
          'amountInCents' => $transaction->amount * 100,
          'amount_formatted' => formatCurrency($transaction->amount,$config['home_currency']),
          'description' => $transaction->description,
          'invoiceId' => $transaction->iid
        ];
      }
        if(!empty($transactions_data)){
      jsonResponse([
    //    'account' => $account,
    'status' => "success",
    'error'=> false,
    'message' => [""],

        'transactions' => $transactions_data
      ]);
    }
    else {
      jsonResponse([
    //    'account' => $account,
    'status' => "failed",
    'error'=> true,
    'message' => ["No Transaction Listed"],

        'transactions' => null
      ]);
    }

      break;
/*Pos api end */

        case 'test':
          echo "working";
        break;

          }
          break;




    default:

        jsonResponse([
            'error' => true,
            'message' => 'Unknown API version!'
        ]);

        break;

}
