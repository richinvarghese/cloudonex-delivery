<?php
/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
|
*/
require 'vendor/autoload.php';
use GuzzleHttp\Client;



_auth();
$ui->assign('_application_menu', 'driver');
$ui->assign('_st', 'Driver');
$ui->assign('_title', 'Driver');
$action = $routes['1'];
$user = User::_info();
$ui->assign('user', $user);
$ui->assign('_user', $user);
Event::trigger('driver');



switch ($action) {

  case 'driver-users':

  $ui->assign('content_inner',inner_contents($config['c_cache']));
  /*if($user['user_type'] != 'Admin'){
      r2(U."dashboard",'e',$_L['You do not have permission']);
  }*/
//        $ui->assign('xfooter', '
//<script type="text/javascript" src="ui/lib/c/users.js"></script>
//');

//  $d = ORM::for_table('sys_users')->find_many();
  $d = ORM::for_table('sys_users')->select('sys_users.img')->select('sys_users.username')->select('sys_users.fullname')->select('sys_users.user_type')->select('sys_users.id')->select('driver_location.latitude')->select('driver_location.longitude')->left_outer_join("driver_location", "driver_location.driver_id = sys_users.id")->order_by_desc('sys_users.id')->where_like('user_type','%Driver%')->where('status', 'Active');
  $d = $d->find_array();
  $ui->assign('d',$d);
  $css_arr = array(
      's2/css/select2.min',
      'modal',
      'dp/dist/datepicker.min',
      'redactor/redactor'
  );
  $js_arr = array(
      'redactor/redactor.min',
      's2/js/select2.min',
      's2/js/i18n/' . lan() ,
      'dp/dist/datepicker.min',
      'dp/i18n/' . $config['language'],
      'numeric',
      'filter.min',
      'modal'
  );

//  $pos = route(4);

//  Event::trigger('add_invoice_rendering_form');
  $ui->assign('xheader', Asset::css($css_arr));
  $ui->assign('xfooter', Asset::js($js_arr));

  view('driver_users');


  break;


  case 'driver-users-add':

      $ui->assign('xfooter',Asset::js('settings/staff'));
      $ui->assign('content_inner',inner_contents($config['c_cache']));
//        if($user['user_type'] != 'Admin'){
//            r2(U."dashboard",'e',$_L['You do not have permission']);
//        }


    $departments = TicketDepartment::all();

      $roles = M::factory('Models_Role')->find_array();
      $ui->assign('roles',$roles);



      view('driver-users-add',[
        'departments' => $departments,
        'employee' => false
      ]);

      break;

  case 'driver-users-edit':



    //  $ui->assign('content_inner',inner_contents($config['c_cache']));

      $ui->assign('_application_menu', 'driver');

      $ui->assign('languages',IBilling_I18n::get_languages());



      $id  = $routes['2'];
      $d = ORM::for_table('sys_users')->find_one($id);
      if($d){

          if($user->id != $d->id){

              if(!has_access($user->roleid,'settings','edit')){

                  permissionDenied();

              }

          }

          $ui->assign('xheader', Asset::css(array('imgcrop/assets/css/croppic')));


          $ui->assign('xfooter', Asset::js(array('imgcrop/croppic')));

          $ui->assign('d',$d);

          // find user language

          if($d->language == ''){
              $selected_language = $config['language'];
          }
          else{
              $selected_language = $d->language;
          }

          $roles = M::factory('Models_Role')->find_array();
          $ui->assign('roles',$roles);

          view('driver-users-edit',[
              'selected_language' => $selected_language
          ]);

      }
      else{
          r2(U . 'driver/driver-users', 'e', $_L['Account_Not_Found']);
      }

      break;

  case 'driver-users-delete':


      $id  = $routes['2'];
      //prevent self delete
      if(($user->id) == $id){
          r2(U . 'driver/driver-users', 'e', 'Sorry You can\'t delete yourself');
      }
      $d = ORM::for_table('sys_users')->find_one($id);
      if($d){

          if($user->id != $d->id){

      /*        if(!has_access($user->roleid,'settings','delete')){

                  permissionDenied();

              }*/

          }

          $d->delete();
          r2(U . 'driver/driver-users', 's', 'User deleted Successfully');
      }
      else{
          r2(U . 'driver/driver-users', 'e', $_L['Account_Not_Found']);
      }

      break;

  case 'driver-users-post':



      $username = _post('username');
      $fullname = _post('fullname');
      $password =  "123456789";//_post('password');
      $cpassword = "123456789";//_post('cpassword');
      $user_type = _post('user_type');
      $transport_type = _post('transport_type');
      $vehicle_number = _post('vehicle_number');


      /*if(!has_access($user->roleid,'settings','create')){

          permissionDenied();

      }*/


      $r = M::factory('Models_Role')->find_one($user_type);

      if($r){
          $role = $r->rname;
          $roleid = $user_type;
          $user_type = $r->rname;
      }
      else{
          $role = '';
          $roleid = 0;
          $user_type = 'Admin';
      }


      $msg = '';
      if(filter_var($username, FILTER_VALIDATE_EMAIL) == false){
          $msg .= $_L['notice_email_as_username']. '<br>';
      }

      if($password != $cpassword){
          $msg .= 'Passwords does not match'. '<br>';
      }
//check with same name account is exist
      $d = ORM::for_table('sys_users')->where('username',$username)->find_one();
      if($d){
          $msg .= $_L['account_already_exist']. '<br>';
      }


      // create Roles




      if($msg == ''){

          $password = Password::_crypt($password);
          // Add Account
          $d = ORM::for_table('sys_users')->create();
          $d->username = $username;
          $d->password = $password;
          $d->fullname = $fullname;
          $d->user_type = $user_type;

          //others
          $d->phonenumber = '';
          $d->last_login = date('Y-m-d H:i:s');
          $d->email = '';
          $d->creationdate = date('Y-m-d H:i:s');
          $d->pin = '';
          $d->img = '';
          $d->otp = 'No';
          $d->pin_enabled = 'No';
          $d->api = 'No';
          $d->pwresetkey = '';
          $d->keyexpire = '';
          $d->status = 'Active';
          $d->role = $role;
          $d->roleid = $roleid;
          $d->transport_type = $transport_type;
          $d->vehicle_number = $vehicle_number;


          //

          $d->save();
          r2(U . 'driver/driver-users', 's', $_L['account_created_successfully'].' and default password is "123456789"');
      }
      else{
          r2(U . 'driver/driver-users-add', 'e', $msg);
      }

      break;


  case 'driver-users-edit-post':


      $username = _post('username');
      $fullname = _post('fullname');
      $phonenumber = _post('phonenumber');
      $img = _post('picture');

      $img = str_replace(APP_URL.'/','',$img);
      $password = "123456789"; //_post('password');
      $cpassword = "123456789"; //_post('cpassword');

      $language = _post('user_language');

      $_SESSION['language'] = $language;


      $msg = '';

      if(filter_var($username, FILTER_VALIDATE_EMAIL) == false){
          $msg .= 'Please use a valid Email address as Username'. '<br>';
      }

      if($password != ''){

          if($password != $cpassword){
              $msg .= 'Passwords does not match'. '<br>';
          }
      }
      //find this user
      $id = _post('id');
      $d = ORM::for_table('sys_users')->find_one($id);
      if($d){

          if($user->id != $d->id){

              /*if(!has_access($user->roleid,'settings','edit')){

                  permissionDenied();

              }*/

          }

      }
      else{
          $msg .= 'Username Not Found'. '<br>';
      }
//check with same name account is exist
      if($d['username'] != $username){
          $c = ORM::for_table('sys_users')->where('username',$username)->find_one();
          if($c){
              $msg .= $_L['account_already_exist']. '<br>';
          }
      }



      if(APP_STAGE == 'Demo'){
          $msg .= 'Editing User is disabled in the Demo Mode!'. '<br>';
      }



      $user_type = _post('user_type');

      $r = M::factory('Models_Role')->find_one($user_type);

      if($r){
          $role = $r->rname;
          $roleid = $user_type;
          $user_type = $r->rname;
      }
      else{
          $role = '';
          $roleid = 0;
          $user_type = 'Admin';
      }


      if($msg == ''){




          $d->username = $username;

          $d->language = $language;
          if($password != ''){
              $password = Password::_crypt($password);
              $d->password = $password;
          }

          $d->fullname = $fullname;

          $d->fullname = $fullname;

          $d->phonenumber = $phonenumber;

          if(($user->id) != $id){

              $d->user_type = $user_type;
              $d->role = $role;
              $d->roleid = $roleid;
          }

          $d->img = $img;
          $d->isupdated = 1;



          $d->save();
          r2(U . 'driver/driver-users-edit/'.$id, 's', 'User Updated Successfully');
      }
      else{
          r2(U . 'driver/driver-users-edit/'.$id, 'e', $msg);
      }

      break;


          case 'settings':

              $ui->assign('content_inner',inner_contents($config['c_cache']));



          //

          $delivery_method = ORM::for_table('driver_settings')->where('id',2)->find_one();



              view('driver_settings',[
                'delivery_method' => $delivery_method
              ]);

              break;

case 'driver-assign';
$css_arr = array(
    's2/css/select2.min',
    'modal',
    'dp/dist/datepicker.min',
    'redactor/redactor'
);
$js_arr = array(
    'redactor/redactor.min',
    's2/js/select2.min',
    's2/js/i18n/' . lan() ,
    'dp/dist/datepicker.min',
    'dp/i18n/' . $config['language'],
    'numeric',
    'filter.min',
    'modal'
);

//$pos = route(4);

//Event::trigger('add_invoice_rendering_form');
$ui->assign('xheader', Asset::css($css_arr));
$ui->assign('xfooter', Asset::js($js_arr));

if(isset($_POST['current_date'])) {
$current_date = _post('current_date');
}
else if(isset($routes['2'])){
  $cd = $routes['2'];
  $current_date = $cd;
}


//print_r($current_date);

else if(!isset($_POST['current_date'])){
  $current_date = Date('Y-m-d');

}




  $ui->assign('current_date', $current_date);


  $delivery_notes = ORM::for_table('sys_invoices')->where_not_equal('c1', '')->where_like('c2', $current_date.'%')->where_in('delivery_status', array('Unassigned', 'Assigned'))->order_by_desc('id')->find_array();
  //$c = ORM::for_table('sys_users')->select('id')->select('fullname')->select('username')->select('email')->order_by_desc('id')->where_like('user_type','%Driver%')->where('status', 'Active');
  $c = ORM::for_table('sys_users')->select('sys_users.id')->select('fullname')->select('username')->select('email')->select('driver_availablity.date')->select("driver_availablity.status")->join('driver_availablity', "driver_availablity.driver_id = sys_users.id and driver_availablity.date='$current_date' and driver_availablity.status = 1 " )->where_like('user_type','%Driver%')->where('status', 'Active');


  $c = $c->find_array();
  if(!empty($c)){
    $driver_available = count($c);
  }
  else {
      $driver_available = 0;
  }
  if(!empty($delivery_notes)){
    $total_delivery = count($delivery_notes);
  }
  else {
      $total_delivery = 0;
  }
  $ui->assign('driver_available', $driver_available);
  $ui->assign('total_delivery', $total_delivery);

  $ui->assign('c', $c);
  //$delivery_notes = ORM::for_table('sys_invoices')->where_not_equal('c1' '1')->order_by_desc('id')->find_array();//Invoice //DeliveryNote::all();
    $ui->assign('d', $delivery_notes);

  view('driver_assign',[

  ]);
break;

case 'driver-availablity':
$css_arr = array(
    's2/css/select2.min',
    'modal',
    'dp/dist/datepicker.min',
    'redactor/redactor'
);
$js_arr = array(
    'redactor/redactor.min',
    's2/js/select2.min',
    's2/js/i18n/' . lan() ,
    'dp/dist/datepicker.min',
    'dp/i18n/' . $config['language'],
    'numeric',
    'filter.min',
    'modal'
);




$ui->assign('xheader', Asset::css($css_arr));
$ui->assign('xfooter', Asset::js($js_arr));
$current_date = _post('current_date');

if($current_date == ""){
  $current_date = Date('Y-m-d');

}

else if(isset($_SESSION['cdate']) || _post('current_date')==null) {
//  $current_date = $_SESSION['cdate'];
}



  $ui->assign('current_date', $current_date);


  $delivery_notes = ORM::for_table('sys_invoices')->where_not_equal('c1', '')->where_like('c2', $current_date.'%')->where_in('delivery_status', array('Unassigned', 'Assigned'))->order_by_desc('id')->find_array();
  $c = ORM::for_table('sys_users')->select('sys_users.id')->select('fullname')->select('username')->select('email')->select('driver_availablity.date')->select("driver_availablity.status")->left_outer_join('driver_availablity', "driver_availablity.driver_id = sys_users.id and driver_availablity.date='$current_date'")->where_like('user_type','%Driver%')->where('status', 'Active');

  $c = $c->find_array();
 //print_r($c); die();

  $ui->assign('c', $c);
  //$delivery_notes = ORM::for_table('sys_invoices')->where_not_equal('c1' '1')->order_by_desc('id')->find_array();//Invoice //DeliveryNote::all();
  $ui->assign('d', $delivery_notes);

  view('driver_manage_availablity',[

  ]);
break;

case 'driver-availablity-add':
  $current_date = $_POST["current_date"];
  $userid = $_POST['userid'];


  $_SESSION['cdate'] = $current_date;
  //$submit = $_POST['s'];
  //$c = ORM::for_table('sys_users')->select('sys_users.id')->select('fullname')->select('username')->select('email')->where_like('user_type','%Driver%')->where('status', 'Active');

  if(isset($_POST['status'])) {
      $status = $_POST['status'];


  }
  else{
    $status = [];
  }
  $x = ORM::for_table('driver_availablity')->where('date', $current_date)->delete_many();

  foreach ($userid as $key => $value) {
      if(in_array($value,$status)){
        $st = 1;
      }
      else{
        $st = 0;
      }

    $d = ORM::for_table('driver_availablity')->create();
    $d->driver_id = $value;
    $d->date = $current_date;
    $d->status = $st;
    $d->save();

  }
  r2(U . 'driver/driver-availablity/'.$current_date, 's', 'Updated successfully');
break;


case 'driver-smart-assign':
$url = 'https://api.routific.com/v1/';
$client = new Client([
  'base_uri' => $url,
]);

//$deliveryID = $_POST['deliveryid'];
//print_r($deliveryID); die();
$current_date = $_POST['current_date'];
$delivery_notes = ORM::for_table('sys_invoices')->where_not_equal('c1', '')->where_like('c2', $current_date.'%')->where_in('delivery_status', array('Unassigned', 'Assigned'))->order_by_desc('id')->find_array();
  $visits = array();
foreach ($delivery_notes as $key => $value) {
  //print_r($value); die();
    $order = array("location" => array(
              "name" =>$value["shipping_street"],
              "lat" => $value["c3"],
              "lng" => $value["c4"]
          ));
          $visits[$value['id']] = $order; //$value[id] => order_id
          //print_r($order);
          //echo "<br/><br/>";
}


//Step 3: Prepare vehicles
$c = ORM::for_table('sys_users')->select('sys_users.id')->select('fullname')->select('vehicle_number')->select('username')->select('email')->select('driver_availablity.date')->select("driver_availablity.status")->join('driver_availablity', "driver_availablity.driver_id = sys_users.id and driver_availablity.date='$current_date' and driver_availablity.status = 1 " )->where_like('user_type','%Driver%')->where('status', 'Active');


$c = $c->find_array();

$latitude = ORM::for_table('sys_appconfig')->where('setting','latitude')->find_one()->value;
$longitude = ORM::for_table('sys_appconfig')->where('setting','longitude')->find_one()->value;
//print_r($longitude); die();

$vechicle = array();
foreach ($c as $key => $value) {

  $vehicle1 = array(
    "start_location" => array(
      "id" => $value['id'], //driver id;
      "name" => "Company Office",
      "lat" => $latitude,
      "lng" => $longitude
  ));
  $vehicles["vehicle_".$value['vehicle_number']] = $vehicle1;
}
//print_r($vehicles); die();

//Step 4: Prepare data payload
$payload = array(
  "visits" => $visits,
  "fleet" => $vehicles
);

//Step 5: Send request
//$token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2U1MTdiYTg1ODVmMTFjOGFkZTk3ZjUiLCJpYXQiOjE1NTg1MTc2OTB9.9wxvuZypoSuRNAZ-mIbtDC0umWoKn4t1EmQLuDMmR0w';
$token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGE4MWQxMzFkYmIyYjE5Yjg4YTNhNGEiLCJpYXQiOjE1NzEyOTg1Nzl9.hRKuJgTufdz_LsjLxr7h2Gs7OyN80t5gAp4oid2pzbc';
try{
$response = $client->post('vrp', [
  'json' => $payload,
  'headers' => [
    "Content-type" => "application/json",
    "Authorization" => "bearer " . $token
  ]
]);
$result =  json_decode($response->getBody())->solution;
//print_r($result); echo "<br/><br/>"; die();
foreach($result as $vno => $r){
  //print_r($r); echo "<br/><br/>";
  $i=0;
  foreach ($r as $key => $value) {
    if($i==0){
      $i++;
      $driver_id = $value->location_id;
      continue;
    }
    //print_r($r); echo "<br/>";
    $do = $value->location_id;

//echo "delivery id - ".$driver_id ."  delivery order - ". $do. "<br/>";
$delivery_id = ORM::for_table('sys_invoices')->find_one($do);
$delivery_id->c5 = $driver_id;
$delivery_id->delivery_status = "Assigned";
$delivery_id->save();

  }
}
//print_r($result); die();
r2(U."driver/driver-assign/".$current_date,'s',"Smart Assign System assigned drivers successfully");

}
catch (Exception $e) {
  $error = json_decode($e->getResponse()->getBody()->getContents());
  //print_r($error->error);
  $time = strtotime($current_date);
  //print_r($time); die();
  r2(U."driver/driver-assign/".$current_date,'e',$error->error);
}

break;


case 'driver-manual-assign':

$css_arr = array(
    's2/css/select2.min',
    'modal',
    'dp/dist/datepicker.min',
    'redactor/redactor'
);
$js_arr = array(
    'redactor/redactor.min',
    's2/js/select2.min',
    's2/js/i18n/' . lan() ,
    'dp/dist/datepicker.min',
    'dp/i18n/' . $config['language'],
    'numeric',
    'filter.min',
    'modal'
);

//$pos = route(4);

//Event::trigger('add_invoice_rendering_form');
$ui->assign('xheader', Asset::css($css_arr));
$ui->assign('xfooter', Asset::js($js_arr));

if(isset($_POST['current_date'])) {
$current_date = _post('current_date');
}
else if(isset($routes['2'])){
  $cd = $routes['2'];
  $current_date = $cd;
}


//print_r($current_date);

else if(!isset($_POST['current_date'])){
  $current_date = Date('Y-m-d');

}




  $ui->assign('current_date', $current_date);


  $delivery_notes = ORM::for_table('sys_invoices')->where_not_equal('c1', '')->where_like('c2', $current_date.'%')->where_in('delivery_status', array('Unassigned', 'Assigned'))->order_by_desc('id')->find_array();
  //$c = ORM::for_table('sys_users')->select('id')->select('fullname')->select('username')->select('email')->order_by_desc('id')->where_like('user_type','%Driver%')->where('status', 'Active');
  $c = ORM::for_table('sys_users')->select('sys_users.id')->select('fullname')->select('username')->select('email')->select('driver_availablity.date')->select("driver_availablity.status")->join('driver_availablity', "driver_availablity.driver_id = sys_users.id and driver_availablity.date='$current_date' and driver_availablity.status = 1 " )->where_like('user_type','%Driver%')->where('status', 'Active');


  $c = $c->find_array();
  if(!empty($c)){
    $driver_available = count($c);
  }
  else {
      $driver_available = 0;
  }
  if(!empty($delivery_notes)){
    $total_delivery = count($delivery_notes);
  }
  else {
      $total_delivery = 0;
  }
  $ui->assign('driver_available', $driver_available);
  $ui->assign('total_delivery', $total_delivery);

  $ui->assign('c', $c);
  //$delivery_notes = ORM::for_table('sys_invoices')->where_not_equal('c1' '1')->order_by_desc('id')->find_array();//Invoice //DeliveryNote::all();
    $ui->assign('d', $delivery_notes);

  view('driver_manual-assign',[

  ]);
break;


case 'driver-map':
if (!has_access($user->roleid, 'products_n_services', 'view')) {
    permissionDenied();
}


$d = ORM::for_table('sys_items')->order_by_asc('name')->find_array();

echo '
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h3> Current Location</h3>
</div>
<div class="modal-body">
  <div id="map"></div>

</div>
<div class="modal-footer">

<button type="button" data-dismiss="modal" class="btn btn-primary update">'.$_L['Close'].'</button>
</div>
<style type="text/css">

</style>
<script>
// Initialize and add the map
function initMap() {

// The location of Uluru
var uluru = {lat: -25.344, lng: 131.036};
// The map, centered at Uluru

var map = new google.maps.Map(
  document.getElementById("map"), {zoom: 4, center: uluru});
// The marker, positioned at Uluru
var marker = new google.maps.Marker({position: uluru, map: map});

}
</script>
<!--Load the API from the specified URL
* The async attribute allows the browser to render the page while the API loads
* The key parameter will contain your own API key (which is not needed for this tutorial)
* The callback parameter executes the initMap() function
-->
<script async defer
src=https://maps.googleapis.com/maps/api/js?key=AIzaSyDK9Wyy81NpY3wkqekhOF4EBFHt5Tc-Yyw&callback=initMap>
</script>
';

break;
  /*  case 'add':
    case 'edit':



        $customers_all_data = has_access($user->roleid,'customers','all_data');

        $extraHtml = '';

        $app->emit('invoices_add');

        $extra_fields = '';
        $extra_jq = '';

        Event::trigger('add_invoice');

        $ui->assign('extra_fields', $extra_fields);

    $recurring = false;

    $p_cid = '';

    $invoice = false;
    $contact = false;
    $items = false;

        if($action == 'add')
        {
            if (isset($routes['2']) AND ($routes['2'] == 'recurring')) {
                $recurring = true;
            }

            if (isset($routes['3']) && ($routes['3'] != '') && ($routes['3'] != '0')) {
                $p_cid = $routes['3'];
                $p_d = ORM::for_table('crm_accounts')->find_one($p_cid);
                if (!$p_d) {
                   $p_cid = '';
                }
            }

            $ui->assign('_st', $_L['Add Invoice']);
        }

        elseif ($action == 'edit')
        {
            $id = route(2);
            $invoice = Invoice::find($id);

            $p_cid = $invoice->userid;


        }

        if($invoice)
        {
            $contact = Contact::find($invoice->userid);
            $items = InvoiceItem::where('invoiceid',$invoice->id)->get();
        }
    $ui->assign('p_cid', $p_cid);



    $ui->assign('recurring', $recurring);

        $c = ORM::for_table('crm_accounts')->select('id')->select('account')->select('company')->select('email')->order_by_desc('id')->where_like('type','%Customer%');



        if(!$customers_all_data)
        {
            $c->where('o',$user->id);
        }


        $c = $c->find_array();

        $ui->assign('c', $c);
        $t = ORM::for_table('sys_tax')->find_array();
        $ui->assign('t', $t);
        $ui->assign('idate', date('Y-m-d'));

        $tax_default = ORM::for_table('sys_tax')->where('is_default',1)->find_one();


        $tax_system = $config['tax_system'];

        switch ($tax_system){

            case 'India':


                $states = Tax::indianStates();


                break;

            default:

                $states = [];

        }
        $driver = ORM::for_table('sys_users')->select('id')->select('fullname')->select('username')->select('email')->order_by_desc('id')->where_like('user_type','%Driver%')->where('status', 'Active');
        $driver = $driver->find_array();

        $ui->assign('driver', $driver);


        $googleapi = ORM::for_table('googleapi')->where('id',1)->find_one();
      //  print_r($googleapi->apikey); die();


        $css_arr = array(
            's2/css/select2.min',
            'modal',
            'dp/dist/datepicker.min',
            'redactor/redactor'
        );
        $js_arr = array(
            'redactor/redactor.min',
            's2/js/select2.min',
            's2/js/i18n/' . lan() ,
            'dp/dist/datepicker.min',
            'dp/i18n/' . $config['language'],
            'numeric',
            'filter.min',
            'modal'
        );

        $pos = route(4);

        Event::trigger('add_invoice_rendering_form');
        $ui->assign('xheader', Asset::css($css_arr));
        $ui->assign('xfooter', Asset::js($js_arr));



        view('invoice',[
            'pos' => $pos,
            'tax_default' => $tax_default,
            'states' => $states,
            'extraHtml' => $extraHtml,
            'currencies' => getActiveCurrencies(),
            'invoice' => $invoice,
            'contact' => $contact,
            'items' => $items,
            'googleapi' =>$googleapi
        ]);


        break;


*/


    default:
        echo 'action not defined';
}
