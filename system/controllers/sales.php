<?php
/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
|
*/

_auth();
$ui->assign('_application_menu', 'invoices');
$ui->assign('_st', $_L['Delivery Challans']);
$ui->assign('_title', $_L['Sales'] . '- ' . $config['CompanyName']);
$user = User::_info();
$ui->assign('user', $user);
Event::trigger('invoices');

if(has_access($user->roleid,'sales','all_data')){

    $sales_all_data = true;

}
else{

    $sales_all_data = false;

}

$action = route(1);

switch ($action) {


    case 'delivery_challans':

    $ui->assign('_application_menu', 'driver');

    if(!db_table_exist('delivery_notes'))
    {
        r2(U.'sales/update');
    }
    $delivery_notes = ORM::for_table('sys_invoices')->select('sys_invoices.*')->select('sys_users.fullname')->left_outer_join('sys_users', "sys_invoices.c5 = sys_users.id")->where_not_equal('c1', '')->order_by_desc('id')->find_array();
    $c = ORM::for_table('sys_users')->select('id')->select('fullname')->select('username')->select('email')->order_by_desc('id')->where_like('user_type','%Driver%')->where('status', 'Active');
    $c = $c->find_array();

    $ui->assign('c', $c);
    //$delivery_notes = ORM::for_table('sys_invoices')->where_not_equal('c1' '1')->order_by_desc('id')->find_array();//Invoice //DeliveryNote::all();
      $ui->assign('d', $delivery_notes);
    //print_r($delivery_notes); die();
  /*  view('sales_delivery_notes',[
        'delivery_notes' => $delivery_notes
    ]);*/
    view('sales_delivery_notes');


        break;


    case 'delivery_challan':

    $customers_all_data = has_access($user->roleid,'customers','all_data');

$extraHtml = '';

$app->emit('invoices_add');

$extra_fields = '';
$extra_jq = '';

Event::trigger('add_invoice');

$ui->assign('extra_fields', $extra_fields);

$recurring = false;

$p_cid = '';

$invoice = false;
$contact = false;
$items = false;

if($action == 'add')
{
  if (isset($routes['2']) AND ($routes['2'] == 'recurring')) {
      $recurring = true;
  }

  if (isset($routes['3']) && ($routes['3'] != '') && ($routes['3'] != '0')) {
      $p_cid = $routes['3'];
      $p_d = ORM::for_table('crm_accounts')->find_one($p_cid);
      if (!$p_d) {
         $p_cid = '';
      }
  }

  $ui->assign('_st', $_L['Add Invoice']);
}

elseif ($action == 'edit')
{
  $id = route(2);
  $invoice = Invoice::find($id);

  $p_cid = $invoice->userid;


}

if($invoice)
{
  $contact = Contact::find($invoice->userid);
  $items = InvoiceItem::where('invoiceid',$invoice->id)->get();
}
$ui->assign('p_cid', $p_cid);



$ui->assign('recurring', $recurring);

$c = ORM::for_table('crm_accounts')->select('id')->select('account')->select('company')->select('email')->order_by_desc('id')->where_like('type','%Customer%');



if(!$customers_all_data)
{
  $c->where('o',$user->id);
}


$c = $c->find_array();

$ui->assign('c', $c);
$t = ORM::for_table('sys_tax')->find_array();
$ui->assign('t', $t);
$ui->assign('idate', date('Y-m-d'));

$tax_default = ORM::for_table('sys_tax')->where('is_default',1)->find_one();


$tax_system = $config['tax_system'];

switch ($tax_system){

  case 'India':


      $states = Tax::indianStates();


      break;

  default:

      $states = [];

}

//$googleapi = ORM::for_table('driver_settings')->where('id',1)->find_one();
//  print_r($googleapi->apikey); die();
$c = ORM::for_table('sys_users')->select('id')->select('fullname')->select('username')->select('email')->order_by_desc('id')->where_like('user_type','%Driver%')->where('status', 'Active');
$c = $c->find_array();

$ui->assign('driver', $c);

$css_arr = array(
  's2/css/select2.min',
  'modal',
  'dp/dist/datepicker.min',
  'redactor/redactor'
);
$js_arr = array(
  'redactor/redactor.min',
  's2/js/select2.min',
  's2/js/i18n/' . lan() ,
  'dp/dist/datepicker.min',
  'dp/i18n/' . $config['language'],
  'numeric',
  'filter.min',
  'modal'
);

$pos = route(4);

Event::trigger('add_invoice_rendering_form');
$ui->assign('xheader', Asset::css($css_arr));
$ui->assign('xfooter', Asset::js($js_arr));



view('delivery_chellan_cust',[
  'pos' => $pos,
  'tax_default' => $tax_default,
  'states' => $states,
  'extraHtml' => $extraHtml,
  'currencies' => getActiveCurrencies(),
  'invoice' => $invoice,
  'contact' => $contact,
  'items' => $items
]);



      break;



    case 'update':

        $script = '<script>
    $(function() {
        var delay = 10000;
        var $serverResponse = $("#serverResponse");
        var interval = setInterval(function(){
   $serverResponse.append(\'.\');
}, 500);

        setTimeout(function(){ window.location = \''.U.'sales/delivery_challans\'; }, delay);
    });
</script>';

        if(db_table_exist('delivery_notes')){
            HtmlCanvas::createTerminal('Already updated!',$script);
            exit;
        }

        $message = 'Updating scehma to support Delivery Challans... '.PHP_EOL;


        ORM::execute('CREATE TABLE `delivery_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contact_id` int(10) unsigned DEFAULT NULL,
  `currency` char(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_total` decimal(16,8) NOT NULL DEFAULT \'0.00000000\',
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

        ORM::execute('CREATE TABLE `delivery_note_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned DEFAULT NULL,
  `quantity` decimal(8,2) unsigned NOT NULL DEFAULT \'0.00\',
  `rate` decimal(16,8) unsigned NOT NULL DEFAULT \'0.00000000\',
  `tax_id` int(10) unsigned DEFAULT NULL,
  `tax_rate` decimal(16,8) unsigned NOT NULL DEFAULT \'0.00000000\',
  `total` decimal(16,8) unsigned NOT NULL DEFAULT \'0.00000000\',
  `discount_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` decimal(16,8) unsigned NOT NULL DEFAULT \'0.00000000\',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

        $message .= 'Tables were created...'.PHP_EOL;

        $message .= '---------------------------'.PHP_EOL;
        $message .= 'Redirecting, please wait...';





        HtmlCanvas::createTerminal($message,$script);


        break;


        case 'tasks':

$ui->assign('_application_menu', 'driver');
  if(!isset($_POST['selected_date'])){
    $mdate = date('Y-m-d');
  }
  else{
    $mdate = $_POST['selected_date'];
  }

  $unassigned = ORM::for_table('sys_invoices')->select('sys_invoices.*')->select('sys_users.fullname')->left_outer_join('sys_users', "sys_invoices.c5 = sys_users.id")->where_not_equal('c1', '')->where_like('c2', $mdate.'%')->where("delivery_status", "Unassigned")->order_by_desc('id')->find_array();
  $assigned = ORM::for_table('sys_invoices')->select('sys_invoices.*')->select('sys_users.fullname')->left_outer_join('sys_users', "sys_invoices.c5 = sys_users.id")->where_not_equal('c1', '')->where_like('c2', $mdate.'%')->where_not_equal("delivery_status", "Unassigned")->where_not_equal("delivery_status", "Successful")->order_by_desc('id')->find_array();
  $completed = ORM::for_table('sys_invoices')->select('sys_invoices.*')->select('sys_users.fullname')->left_outer_join('sys_users', "sys_invoices.c5 = sys_users.id")->where_not_equal('c1', '')->where_like('c2', $mdate.'%')->where("delivery_status", "Successful")->order_by_desc('id')->find_array();
  $all =   ORM::for_table('sys_invoices')->select('sys_invoices.*')->select('sys_users.fullname')->left_outer_join('sys_users', "sys_invoices.c5 = sys_users.id")->where_not_equal('c1', '')->where_like('c2', $mdate.'%')->order_by_desc('id')->find_array();

  $c = ORM::for_table('sys_users')->select('id')->select('fullname')->select('username')->select('email')->order_by_desc('id')->where_like('user_type','%Driver%')->where('status', 'Active');
  $c = $c->find_array();

  $ui->assign('c', $c);
  //$delivery_notes = ORM::for_table('sys_invoices')->where_not_equal('c1' '1')->order_by_desc('id')->find_array();//Invoice //DeliveryNote::all();
    $ui->assign('un', $unassigned);
    $ui->assign("as", $assigned);
    $ui->assign("co", $completed);
    $ui->assign("all", $all);

  //print_r($delivery_notes); die();
/*  view('sales_delivery_notes',[
      'delivery_notes' => $delivery_notes
  ]);*/

  $ui->assign('mdate', $mdate);
  $ui->assign('xheader', Asset::css(array(
      'dropzone/dropzone',
      'modal',
      's2/css/select2.min',
      'dp/dist/datepicker.min'
  )));
  $ui->assign('xfooter', Asset::js(array(
      'modal',
      'dropzone/dropzone',
      's2/js/select2.min',
      's2/js/i18n/' . lan() ,
      'dp/dist/datepicker.min',
      'dp/i18n/' . $config['language'],
      'numeric'
  )));

  view('view_task');

break;

case 'import_csv':

$ui->assign('_application_menu', 'driver');

    $ui->assign('xheader', Asset::css(array('dropzone/dropzone')));


    $ui->assign('xfooter', Asset::js(array('dropzone/dropzone','task/import')));



    view('task_import');



    break;

case 'csv_upload':

    $uploader   =   new Uploader();
    $uploader->setDir('storage/temp/');
   // $uploader->sameName(true);
    $uploader->setExtensions(array('csv'));  //allowed extensions list//
    if($uploader->uploadFile('file')){   //txtFile is the filebrowse element name //
        $uploaded  =   $uploader->getUploadName(); //get uploaded file name, renames on upload//

        $_SESSION['uploaded'] = $uploaded;

    }else{//upload failed
        _msglog('e',$uploader->getMessage()); //get upload error message
    }


    break;

case 'csv_uploaded':


    if(isset($_SESSION['uploaded'])){

        $uploaded = $_SESSION['uploaded'];


        $csv = new parseCSV();
        $csv->auto('storage/temp/'.$uploaded);

        $contacts = $csv->data;



        $cn = 0;

        foreach($contacts as $contact){
           $cont_description = $csv->data;
            $data = array();
            //$data['id'] = $contact['id'];
            $data['account'] = $contact['name'];
            $data['c2'] = $contact['delivery_date'];
            $data['subtotal'] = $contact['subtotal'];
            $data['total'] = $contact['total'];
            $data['shipping_street'] = $contact['address'];
            $data['description'] = $contact['product_detail'];

            if($data['c2']==''){
              continue;
            }
            if(isset($data['account']))
            {

              $cid=0;
              $i = 0;
              $description = [];
              $item_number = [];
              $qty = [];
              $amount = [];
              $tax_rate = [];

              $description[0] = $data['description'] ;//$api_item['description'];
              $item_number[0] = ''; //$api_item['item_code'];
              $qty[0] = 1;//$api_item['qty'];
              $amount[$i] = $data['total'];// $api_item['amount'];

              $admin_id = 0;//_post('admin_id');

              $errors = [];

              if(empty($errors)){


                  $sTotal = '0';
                  $taxTotal = '0';
                  $i = '0';
                  $a = array();
                  $taxval = '0.00';
                  $taxname = '';
                  $taxrate = '0.00';


                  $taxed_amount = 0.00;
                  $lamount = 0.00;
                  foreach($amount as $samount) {
                      $samount = Finance::amount_fix($samount);
                      $a[$i] = $samount;

                      $sqty = $qty[$i];
                      $sqty = Finance::amount_fix($sqty);

                      $lTaxRate = 0;//$taxed[$i];
                      $lTaxRate = Finance::amount_fix($lTaxRate);


                      $sTotal+= $samount * ($sqty);
                      $lamount = $samount * ($sqty);

                      $lTaxVal = ($lamount*$lTaxRate)/100;

                      $taxed_amount += $lTaxVal;

                      $i++;

                    }



                    $fTotal = $sTotal;

                    $datetime = date("Y-m-d H:i:s");
                    $vtoken = _raid(10);
                    $ptoken = _raid(10);
                    $d = ORM::for_table('sys_invoices')->create();
                    $d->userid = 0;
                    $d->account = $data['account'];
                    $d->date = date("Y-m-d");
                    $d->duedate = date("Y-m-d");
                    $d->datepaid = $datetime;

                    $d->subtotal = $sTotal;
                    $d->discount_type = "p";//$discount_type;
                    $d->discount_value = 0.0;//$discount_value;
                    $d->discount = 0.0;//$actual_discount;
                    $d->total = $fTotal;
                    $d->tax = 0.0;//$taxed_amount;
                    $d->taxname = '';
                    $d->taxrate = 0.00;
                    $d->vtoken = $vtoken;
                    $d->ptoken = $ptoken;
                    $d->status = "Unpaid";
                    $d->notes = "";
                    $d->r = 0;
                    $d->nd = date('Y-m-d');

                    $d->aid = $admin_id;

                    $d->show_quantity_as = "Qty";


                    $d->invoicenum = "INV-";
                    $d->cn = "";//str_pad($config['invoice_code_current_number'], $config['number_pad'], '0', STR_PAD_LEFT);
                    $d->tax2 = '0.00';
                    $d->taxrate2 = '0.00';
                    $d->paymentmethod = '';
                    $d->shipping_street =  $data['shipping_street'];
                    $d->currency = 1;
                    $d->currency_iso_code = "SGD";
                    $d->currency_symbol = "$";
                    $d->currency_rate = 1.0;


                    $d->receipt_number = "";

                    //for delivery Settings
                     $d->c1 = 1;
                     $delivery_date = new DateTime('tomorrow');
                     $d->c2 =   $data['c2'];//$delivery_date->format('Y-m-d H:i:s');
                     $d->delivery_status = "Unassigned";
                     $string = str_replace(' ', '-', $d->shipping_street); // Replaces all spaces with hyphens.

                     $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                     $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$string.'&key=AIzaSyDK9Wyy81NpY3wkqekhOF4EBFHt5Tc-Yyw'; // path to your JSON file
                     $data = file_get_contents($url); // put the contents of the file into a variable
                     $location_detail = json_decode($data); // decode the JSON feed
                     if(count($location_detail->results))
                     {
                     $d->c3 = $location_detail->results[0]->geometry->location->lat;
                     $d->c4 = $location_detail->results[0]->geometry->location->lng;

                    }
                    $d->save();
                   $invoiceid = $d->id();

                   $i = '0';

                   foreach($cont_description as $item) {

                     if($item['product_detail']!= "" && $item['id'] ==  $contact['id'])
                     {
                       $samount = $item['price'];
                       $samount = Finance::amount_fix($samount);
                       if ($item == '' && $samount == '0.00') {
                           $i++;
                           continue;
                       }

                       $tax_rate = 0;//$taxed[$i];

                       $sqty = $item['qty'];
                       $sqty = Finance::amount_fix($sqty);
                       $ltotal = ($samount) * ($sqty);
                       $d = ORM::for_table('sys_invoiceitems')->create();
                       $d->invoiceid = $invoiceid;
                       $d->userid = 0;
                       $d->description = $item['product_detail'];
                       $d->qty = $sqty;
                       $d->amount = $samount;
                       $d->total = $ltotal;
                       $d->taxed = '0';





                       $d->tax_rate = 0.00;

                       $d->type = '';
                       $d->relid = '0';
                       $d->itemcode = $item['item_number'];
                       $d->taxamount = '0.00';
                       $d->duedate = date('Y-m-d');
                       $d->paymentmethod = '';
                       $d->notes = '';



                       $d->save();

                       Inventory::decreaseByItemNumber($item_number[$i], $sqty);



                       $item_r = Item::where('name', $item['product_detail'])->first();
                       if ($item_r) {
                           $item_r->sold_count = $item_r->sold_count + $sqty;
                           $item_r->total_amount = $item_r->total_amount + $samount;
                           $item_r->save();
                       }

                       $i++;
                     }

                   }



                  }

            }

          /*  if(isset($data['account']))
            {











                                    }







}
else{


}




            }
            else {

            }



          }
          */













                $cn++;




        }


        _msglog('s',$cn.' Task Imported');

//


    }
    else{

        _msglog('e','An Error Occurred while uploading the files');

    }


    break;


  //  case 'add':
    case 'edit':


    $ui->assign('_application_menu', 'driver');

        $customers_all_data = has_access($user->roleid,'customers','all_data');

        $extraHtml = '';

        $app->emit('invoices_add');

        $extra_fields = '';
        $extra_jq = '';

        Event::trigger('add_invoice');

        $ui->assign('extra_fields', $extra_fields);

    $recurring = false;

    $p_cid = '';

    $invoice = false;
    $contact = false;
    $items = false;

        if($action == 'add')
        {
            if (isset($routes['2']) AND ($routes['2'] == 'recurring')) {
                $recurring = true;
            }

            if (isset($routes['3']) && ($routes['3'] != '') && ($routes['3'] != '0')) {
                $p_cid = $routes['3'];
                $p_d = ORM::for_table('crm_accounts')->find_one($p_cid);
                if (!$p_d) {
                   $p_cid = '';
                }
            }

            $ui->assign('_st', $_L['Add Invoice']);
        }

        elseif ($action == 'edit')
        {
            $id = route(2);
            $invoice = Invoice::find($id);

            $p_cid = $invoice->userid;

            if($invoice->r != 0)
            {
                $recurring = true;
            }


        }

        if($invoice)
        {
            $contact = Contact::find($invoice->userid);
            $items = InvoiceItem::where('invoiceid',$invoice->id)->orderBy('id','asc')->get();
        }
    $ui->assign('p_cid', $p_cid);



    $ui->assign('recurring', $recurring);

        $c = ORM::for_table('crm_accounts')->select('id')->select('account')->select('company')->select('email')->order_by_desc('id')->where_like('type','%Customer%');



        if(!$customers_all_data)
        {
            $c->where('o',$user->id);
        }


        $c = $c->find_array();

        $ui->assign('c', $c);
        $t = ORM::for_table('sys_tax')->find_array();
        $ui->assign('t', $t);
        $ui->assign('idate', date('Y-m-d'));

        $tax_default = ORM::for_table('sys_tax')->where('is_default',1)->find_one();


        $tax_system = $config['tax_system'];

        switch ($tax_system){

            case 'India':


                $states = Tax::indianStates();


                break;

            default:

                $states = [];

        }

        /*For retrive driver - Delivery Managment */
        $driver = ORM::for_table('sys_users')->select('id')->select('fullname')->select('username')->select('email')->order_by_desc('id')->where_like('user_type','%Driver%')->where('status', 'Active');
        $driver = $driver->find_array();

        $ui->assign('driver', $driver);


        $css_arr = array(
            's2/css/select2.min',
            'modal',
            'dp/dist/datepicker.min',
            'redactor/redactor'
        );
        $js_arr = array(
            'redactor/redactor.min',
            's2/js/select2.min',
            's2/js/i18n/' . lan() ,
            'dp/dist/datepicker.min',
            'dp/i18n/' . $config['language'],
            'numeric',
            'filter.min',
            'modal'
        );

        $pos = route(4);

        Event::trigger('add_invoice_rendering_form');
        $ui->assign('xheader', Asset::css($css_arr));
        $ui->assign('xfooter', Asset::js($js_arr));



        view('delivery_chellan_cust',[
            'pos' => $pos,
            'tax_default' => $tax_default,
            'states' => $states,
            'extraHtml' => $extraHtml,
            'currencies' => getActiveCurrencies(),
            'invoice' => $invoice,
            'contact' => $contact,
            'items' => $items,
        ]);


        break;


    default:
        echo 'action not defined';

}
