<?php
/* Smarty version 3.1.33, created on 2019-10-08 18:23:36
  from '/Applications/MAMP/htdocs/apstrix/cloudonex/ui/theme/default/driver_users.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d9d0c68350217_16510392',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cbf1b107960be6e4785586e7829117170ee54a93' => 
    array (
      0 => '/Applications/MAMP/htdocs/apstrix/cloudonex/ui/theme/default/driver_users.tpl',
      1 => 1570573351,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d9d0c68350217_16510392 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9518964875d9d0c682e7856_36451465', "style");
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12131768045d9d0c682ef236_32880458', "content");
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12254447885d9d0c6834a2c7_30695880', "script");
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['layouts_admin']->value));
}
/* {block "style"} */
class Block_9518964875d9d0c682e7856_36451465 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'style' => 
  array (
    0 => 'Block_9518964875d9d0c682e7856_36451465',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
ui/lib/footable/css/footable.core.min.css" />
    <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
<?php
}
}
/* {/block "style"} */
/* {block "content"} */
class Block_12131768045d9d0c682ef236_32880458 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_12131768045d9d0c682ef236_32880458',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Manage Driver</h5>

                </div>
                <div class="ibox-content">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
driver/driver-users-add" class="btn btn-primary"><i class="fa fa-plus"></i>Add Driver</a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
driver/driver-availablity" class="btn btn-inverse "><i class="fa fa-tv"></i>Manage Driver Availablity</a>

                    <br>
                    <br>

                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="fa fa-search"></span>
                                    </div>
                                    <input type="text" name="name" id="foo_filter" class="form-control" placeholder="<?php echo $_smarty_tpl->tpl_vars['_L']->value['Search'];?>
..."/>

                                </div>
                            </div>

                        </div>
                    </form>

                    <table class="table table-bordered table-hover sys_table footable" data-filter="#foo_filter" data-page-size="50">
                        <thead>
                        <tr>
                            <th style="width: 60px;"><?php echo $_smarty_tpl->tpl_vars['_L']->value['Avatar'];?>
</th>
                            <th><?php echo $_smarty_tpl->tpl_vars['_L']->value['Username'];?>
</th>
                            <th><?php echo $_smarty_tpl->tpl_vars['_L']->value['Full_Name'];?>
</th>
                            <th><?php echo $_smarty_tpl->tpl_vars['_L']->value['Type'];?>
</th>
                            <th><?php echo $_smarty_tpl->tpl_vars['_L']->value['Manage'];?>
</th>
                        </tr>
                        </thead>

                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['d']->value, 'ds');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ds']->value) {
?>
                            <tr>
                                <td><?php if ($_smarty_tpl->tpl_vars['ds']->value['img'] == 'gravatar') {?>
                                        <img src="http://www.gravatar.com/avatar/<?php echo md5(($_smarty_tpl->tpl_vars['ds']->value['username']));?>
?s=60" class="img-circle" alt="<?php echo $_smarty_tpl->tpl_vars['user']->value['fullname'];?>
">
                                    <?php } elseif ($_smarty_tpl->tpl_vars['ds']->value['img'] == '') {?>
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
ui/lib/imgs/default-user-avatar.png" style="max-height: 60px;" alt="">
                                    <?php } else { ?>
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['ds']->value['img'];?>
" class="img-circle" style="max-height: 60px;" alt="<?php echo $_smarty_tpl->tpl_vars['ds']->value['fullname'];?>
">
                                    <?php }?></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['ds']->value['username'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['ds']->value['fullname'];?>
</td>
                                <td><?php echo ib_lan_get_line($_smarty_tpl->tpl_vars['ds']->value['user_type']);?>
</td>
                                <td>
                                
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
driver/driver-users-edit/<?php echo $_smarty_tpl->tpl_vars['ds']->value['id'];?>
" class="btn btn-inverse"><i class="fa fa-pencil"></i> </a>
                                    <?php if (($_smarty_tpl->tpl_vars['_user']->value['username']) != ($_smarty_tpl->tpl_vars['ds']->value['username'])) {?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
driver/driver-users-delete/<?php echo $_smarty_tpl->tpl_vars['ds']->value['id'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['ds']->value['id'];?>
" class="btn btn-danger cdelete"><i class="fa fa-trash"></i> </a>
                                    <?php }?>
                                    <button class="btn btn-primary mapdata" data-toggle="modal" data-latitude="<?php echo $_smarty_tpl->tpl_vars['ds']->value['latitude'];?>
" data-longitude="<?php echo $_smarty_tpl->tpl_vars['ds']->value['longitude'];?>
" data-target="#myModal"><i class="fa fa-map"></i> View On Map</button>
                                </td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                        <tfoot>
                        <tr>
                            <td colspan="5">
                                <ul class="pagination">
                                </ul>
                            </td>
                        </tr>
                        </tfoot>

                    </table>

                </div>
            </div>



        </div>



    </div>
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-header">
    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    	<h3>Current Location</h3>
    </div>
    <div class="modal-body">
          <div id="map"></div>
    </div>
    <div class="modal-footer">

    	<button class="btn btn-primary update" data-dismiss="modal">Close</button>
    </div>
  </div>
    <style type="text/css">

    </style>


<?php
}
}
/* {/block "content"} */
/* {block "script"} */
class Block_12254447885d9d0c6834a2c7_30695880 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_12254447885d9d0c6834a2c7_30695880',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
ui/lib/footable/js/footable.all.min.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
>
        $(function () {
            $('.footable').footable();
        });

 

 var latitude = 1.3521;
 var longitude = 103.8198;
 $(".mapdata").click(function(){
  latitude =  parseFloat($(this).data("latitude"));
  longitude = parseFloat($(this).data("longitude"));

  initMap();
 });

        function initMap() {
          //var latitude = parseFloat($("#latitude").val());
          //var longitude = parseFloat($("#longitude").val());
          //alert(latitude);
          var uluru = {lat: latitude, lng: longitude};
          // The map, centered at Uluru
          var map = new google.maps.Map(
              document.getElementById('map'), {zoom: 15, center: uluru});
          // The marker, positioned at Uluru
          var marker = new google.maps.Marker({position: uluru, map: map});

        }

    
    <?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK9Wyy81NpY3wkqekhOF4EBFHt5Tc-Yyw">
    <?php echo '</script'; ?>
>

<?php
}
}
/* {/block "script"} */
}
