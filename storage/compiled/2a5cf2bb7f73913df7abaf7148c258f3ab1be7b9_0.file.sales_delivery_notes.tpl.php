<?php
/* Smarty version 3.1.33, created on 2019-10-10 02:45:45
  from '/Applications/MAMP/htdocs/apstrix/cloudonex-delivery/ui/theme/default/sales_delivery_notes.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d9ed399671156_48046488',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2a5cf2bb7f73913df7abaf7148c258f3ab1be7b9' => 
    array (
      0 => '/Applications/MAMP/htdocs/apstrix/cloudonex-delivery/ui/theme/default/sales_delivery_notes.tpl',
      1 => 1570689941,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d9ed399671156_48046488 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20937097265d9ed399582e29_54913306', "style");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4463816055d9ed39958a7f9_05479737', "content");
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6406538615d9ed399618c84_74358672', 'script');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['layouts_admin']->value));
}
/* {block "style"} */
class Block_20937097265d9ed399582e29_54913306 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'style' => 
  array (
    0 => 'Block_20937097265d9ed399582e29_54913306',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
ui/lib/footable/css/footable.core.min.css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
ui/lib/mselect/multiple-select.css" rel="stylesheet">

<?php
}
}
/* {/block "style"} */
/* {block "content"} */
class Block_4463816055d9ed39958a7f9_05479737 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_4463816055d9ed39958a7f9_05479737',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/Applications/MAMP/htdocs/apstrix/cloudonex-delivery/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>


    <div class="row">
        <div class="col-md-12">
            <h3 class="ibilling-page-header"><?php echo $_smarty_tpl->tpl_vars['_L']->value['Delivery Challans'];?>
</h3>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">

                  <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
sales/delivery_challan" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $_smarty_tpl->tpl_vars['_L']->value['New'];?>
</a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
driver/driver-assign"  class="btn btn-inverse  btn-sm"><i class="fa fa-car"></i> Smart Assign Driver </a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
driver/driver-manual-assign"  class="btn btn-inverse  btn-sm"><i class="fa fa-car"></i> Manual Assign  </a>

                  <div class="hr-line-dashed"></div>

                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="fa fa-search"></span>
                                    </div>
                                    <input type="text" name="name" id="foo_filter" class="form-control" placeholder="<?php echo $_smarty_tpl->tpl_vars['_L']->value['Search'];?>
..."/>

                                </div>
                            </div>

                        </div>
                    </form>

                    <table class="table table-bordered table-hover sys_table footable" data-filter="#foo_filter" data-page-size="50">
                        <thead>
                        <tr>
                            <th>Delivery Chellan</th>
                            <th><?php echo $_smarty_tpl->tpl_vars['_L']->value['Customer'];?>
</th>
                            <th>Delivery Date</th>
                            <th>Assigned Driver</th>
                            <th>Status</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['d']->value, 'ds');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ds']->value) {
?>
                            <tr>
                              <td><a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
invoices/deliveryview/<?php echo $_smarty_tpl->tpl_vars['ds']->value['id'];?>
">DO-000<?php echo $_smarty_tpl->tpl_vars['ds']->value['id'];?>
</a></td>
                              <td><a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
contacts/view/<?php echo $_smarty_tpl->tpl_vars['ds']->value['userid'];?>
"><?php echo $_smarty_tpl->tpl_vars['ds']->value['account'];?>
</a> </td>
                                <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ds']->value['c2'],"%D %I:%M %p");?>


 </td>
                              <!--  <td class="amount"><?php echo $_smarty_tpl->tpl_vars['ds']->value['total'];?>
</td>-->
                              <td>

                                                              <?php echo $_smarty_tpl->tpl_vars['ds']->value['fullname'];?>


                </td>
                <td>
                  <?php if ($_smarty_tpl->tpl_vars['ds']->value['delivery_status'] == mb_strtolower('Unassigned', 'UTF-8')) {?>
                  <span class="label label-danger"><?php echo $_smarty_tpl->tpl_vars['ds']->value['delivery_status'];?>
</span>
                  <?php } elseif ($_smarty_tpl->tpl_vars['ds']->value['delivery_status'] == mb_strtolower('Assigned', 'UTF-8')) {?>
                      <span class="label label-info"><?php echo $_smarty_tpl->tpl_vars['ds']->value['delivery_status'];?>
</span>
                  <?php } elseif ($_smarty_tpl->tpl_vars['ds']->value['delivery_status'] == mb_strtolower('Acknowledged', 'UTF-8')) {?>
                      <span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['ds']->value['delivery_status'];?>
</span>
                  <?php } elseif ($_smarty_tpl->tpl_vars['ds']->value['delivery_status'] == mb_strtolower('Started', 'UTF-8')) {?>
                      <span class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['ds']->value['delivery_status'];?>
</span>
                 <?php } elseif ($_smarty_tpl->tpl_vars['ds']->value['delivery_status'] == mb_strtolower('Inprogress', 'UTF-8')) {?>
                      <span class="label label-default"><?php echo $_smarty_tpl->tpl_vars['ds']->value['delivery_status'];?>
</span>
                 <?php } elseif ($_smarty_tpl->tpl_vars['ds']->value['delivery_status'] == mb_strtolower('Successful', 'UTF-8')) {?>
                      <span class="label label-success"><?php echo $_smarty_tpl->tpl_vars['ds']->value['delivery_status'];?>
</span>
                  <?php } else { ?>
                      <span class="label label-danger"><?php echo $_smarty_tpl->tpl_vars['ds']->value['delivery_status'];?>
</span>
                  <?php }?>
                </td>

                <td>
                  <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
invoices/view/<?php echo $_smarty_tpl->tpl_vars['ds']->value['id'];?>
/" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['_L']->value['View'];?>
"><i class="fa fa-file-text-o"></i></a>


                  <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
invoices/edit/<?php echo $_smarty_tpl->tpl_vars['ds']->value['id'];?>
/" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['_L']->value['Edit'];?>
"><i class="fa fa-pencil"></i></a>



                  <a href="#" class="btn btn-danger btn-xs cdelete" id="iid<?php echo $_smarty_tpl->tpl_vars['ds']->value['id'];?>
" data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['_L']->value['Delete'];?>
"><i class="fa fa-trash"></i></a>

                </td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                        </tbody>

                        <tfoot>
                        <tr>
                            <td colspan="5">
                                <ul class="pagination">
                                </ul>
                            </td>
                        </tr>
                        </tfoot>

                    </table>

                </div>
            </div>
        </div>
    </div>



<?php
}
}
/* {/block "content"} */
/* {block 'script'} */
class Block_6406538615d9ed399618c84_74358672 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_6406538615d9ed399618c84_74358672',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
ui/lib/footable/js/footable.all.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
ui/lib/numeric.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
ui/lib/mselect/multiple-select.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
>


        $(function() {

            $('.footable').footable();

            $('.amount').autoNumeric('init', {

                aSign: '<?php echo $_smarty_tpl->tpl_vars['config']->value['currency_code'];?>
 ',
                dGroup: <?php echo $_smarty_tpl->tpl_vars['config']->value['thousand_separator_placement'];?>
,
                aPad: <?php echo $_smarty_tpl->tpl_vars['config']->value['currency_decimal_digits'];?>
,
                pSign: '<?php echo $_smarty_tpl->tpl_vars['config']->value['currency_symbol_position'];?>
',
                aDec: '<?php echo $_smarty_tpl->tpl_vars['config']->value['dec_point'];?>
',
                aSep: '<?php echo $_smarty_tpl->tpl_vars['config']->value['thousands_sep'];?>
',
                vMax: '9999999999999999.00',
                vMin: '-9999999999999999.00'

            });



        });


        function assign_driver(cid, invoiceid){
          var _url = $("#_url").val();


          // alert(invoiceid);

              $.post(_url + 'invoices/assign_driver/', {
                  cid: cid,
                  invoice: invoiceid


              })
                  .done(function (data) {
                      alert(data);

                  });
        }
        $('#driver1').select2({
            theme: "bootstrap",
            language: {
                noResults: function () {
                    return $("#_lan_no_results_found").val();
                }
            }
        })
        .on("change", function (e) {
            // mostly used event, fired to the original element when the value changes
            // log("change val=" + e.val);
            //  alert(e.val);

          //  update_address();
        });


    <?php echo '</script'; ?>
>



<?php
}
}
/* {/block 'script'} */
}
